#!/usr/bin/python
import socket, select
import struct
import sys,os
import time

import argparse

parser = argparse.ArgumentParser(description="Location discovery bug reproducer")
parser.add_argument('--ip', 
                    dest="ip",
                    action="store",
                    help="IP address to send UDP packets to",
                    default="10.79.1.29")

parser.add_argument('--port', 
                    dest="port",
                    action="store",
                    type=int,
                    help="Port no. to send UDP packets to",
                    default=10001)

parser.add_argument('--interval', '-i',
                    dest="interval",
                    action="store",
                    type=float,
                    help="Interval between successive UDP packets",
                    default=1.0)

args = parser.parse_args()

def send_udp_probe(dst_ipaddr, dst_port):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    data = "X"
    try:
        s.sendto(data, (dst_ipaddr, dst_port))
        print '.',
        sys.stdout.flush()
    except:
        print "Failed to send UDP probe"

while True:
    try:
        time.sleep(args.interval)
        send_udp_probe(args.ip, args.port)
    except (KeyboardInterrupt):
        print "Goodbye"
        sys.exit(0)
