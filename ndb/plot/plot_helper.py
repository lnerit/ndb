import matplotlib.pyplot as plt
import os

if os.uname()[0] == "Darwin":
    m.use("MacOSX")
#else:
#    m.use("Agg")

def colorGenerator():
    "Return cycling list of colors"
    colors = [ 'red', 'green', 'blue', 'purple', 'orange', 'cyan']
    index = 0
    while True:
        yield colors[ index ]
        index = ( index + 1 ) % len( colors )

def linestyleGenerator():
    "Return cycling list of linestyles"
    linestyles = ['solid', 'dashed', 'dashdot', 'dotted']
    index = 0
    while True:
        yield linestyles[index]
        index = (index + 1) % len(linestyles)


def markerGenerator():
    "Return cycling list of colors"
    markers = [ 'o', '^', 'v', '*', 'x', 'd']
    index = 0
    while True:
        yield markers [ index ]
        index = ( index + 1 ) % len(markers)

def cdf(values):
    values.sort()
    prob = 0
    l = len(values)
    x, y = [], []

    for v in values:
        prob += 1.0 / l
        x.append(v)
        y.append(prob)

    return (x, y)

def convertToStep(x, y):
    """Convert to a "stepped" data format by duplicating all but the last elt."""
    newx = []
    newy = []
    for i, d in enumerate(x):
        newx.append(d)
        newy.append(y[i])
        if i != len(x) - 1:
            newx.append(x[i + 1])
            newy.append(y[i])
    return newx, newy

def convertToStepUpCDF(x, y):
    """Convert to a "stepped" data format by duplicating all but the last elt.

    Step goes up, rather than to the right.
    """
    newx = []
    newy = []
    for i, d in enumerate(x):
        newx.append(d)
        newy.append(y[i])
        if i != len(x) - 1:
            newx.append(x[i])
            newy.append(y[i + 1])
    newx.append(newx[-1])
    newy.append(1.0)
    return newx, newy

def plotTimeSeries(data, title, xlabel, ylabel, step, xscale = 'linear',
                   yscale = 'linear', legend_loc = 'best'):
    """Plot a time series.

    Input: data, a list of dicts.
    Each inner-most dict includes the following fields:
        x: array
        y: array
        label: string

    step: if True, display data in stepped form.
    xscale: linear or log
    """
    fig = plt.figure()
    ax = plt.subplot(111)
    cgen = colorGenerator()

    fig.canvas.set_window_title( title )

    for d in data:
        x = d['x']
        y = d['y']
        opts = {}
        if 'opts' in d:
            opts = d['opts']
        if step:
            x, y = convertToStep(x, y)
        if 'color' in opts:
            plt.plot( x, y, label=d['label'], **opts)
        else:
            plt.plot( x, y, label=d['label'], color=cgen.next(), **opts)

    plt.xlabel( xlabel, fontsize=20 )
    plt.ylabel( ylabel, fontsize=20 )
    plt.xscale( xscale )
    plt.yscale( yscale )
    plt.grid( True )
    plt.legend(loc = legend_loc)
    for tick in ax.xaxis.get_major_ticks():
        tick.label1.set_fontsize(18)
    for tick in ax.yaxis.get_major_ticks():
        tick.label1.set_fontsize(18)

    return fig

def plotCDF(data, title, xlabel, ylabel, step, xscale = 'linear'):
    data_mod = []
    for index, line in enumerate(data):
        vals = sorted(line['y'])
        x = sorted(vals)
        y = [(float(i) / len(x)) for i in range(len(x))]
        x, y = convertToStepUpCDF(x, y)
        entry = {}
        entry['x'] = x
        entry['y'] = y
        entry['label'] = line['label']
        if 'opts' in line:
            entry['opts'] = line['opts']
        data_mod.append(entry)

    return plotTimeSeries(data_mod, title, xlabel, ylabel, step = False, xscale = xscale)
