#!/usr/bin/python

import inspect
import datetime
import time

def tstamp(data_str=None, logger=None):
    '''Stamp current time along with the caller function name in the logger'''
    f_name = inspect.stack()[1][3]
    t_str = datetime.datetime.now().strftime('%Y%m%d%H%M%S%f')
    ret_str = '%s %s %s' % (t_str, f_name, data_str)
    if logger:
        logger.info(ret_str)
    return ret_str

def parse_tstamp(t_str):
    '''Return datetime object from timestamp string'''
    return datetime.datetime.strptime(t_str, '%Y%m%d%H%M%S%f')
