#!/bin/bash

alg=diff_field_with_prev-
file=${1:-1G-analysis/core-capture.cap.1G.pcap-stats-}
folder=$(dirname $file)/
key=${2:-5}

(grep '^alg:.*diff_field_with_prev-' `ls $file* | sort -k$key -t- -n`) | sed "s|$folder||g"

