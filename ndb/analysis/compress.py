#!/usr/bin/env python
"""Run a bunch of compression algorithms on PCAP files.

Pre-req: on Ubuntu:
    sudo apt-get install python-snappy

"""
import os
import subprocess
import argparse

# Disable for now due to missing native python-snappy support.
# Possibly use this library instead to use PyPY, since it uses CTYPES:
# http://pypi.python.org/pypi/ctypes-snappy/1.03
USE_SNAPPY = False

if USE_SNAPPY:
    import snappy as snappy_lib

FOLDER = 'pcaps/clemson/'
SIZES = ['100K', '1M', '10M', '100M']
FILES = ['core-capture.cap.%s.pcap' % s for s in SIZES]
FILES = [os.path.join(FOLDER, filename) for filename in FILES]

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--files", "-f",
                        nargs="+",
                        default=[])
    args = parser.parse_args()

    if len(args.files) == 0:
        args.files = FILES

def run(cmd):
    return subprocess.Popen(cmd, shell=True)


def tar(filepath, opts, ext):
    output_filepath = filepath + '.' + ext
    if type(opts) is not list:
        opts = [opts]
    args = ['tar'] + opts + [output_filepath, filepath]
    output = subprocess.check_output(args)
    return output, output_filepath

def tgz(filepath):
    return tar(filepath, 'czf', 'tgz')

def bz2(filepath):
    return tar(filepath, 'cjf', 'bz2')

def lzma(filepath):
    return tar(filepath, ['-c', '--lzma', '-f'], 'lzma')

def snappy(filepath):
    file_as_str = open(filepath, 'r').read()
    compressed = snappy_lib.compress(file_as_str)
    ext = 'snappy'
    output_filepath = filepath + '.' + ext
    open(output_filepath, 'w').write(compressed)
    return "", output_filepath

# In order of increasing compression levels
COMPRESSION_FCNS = [tgz, bz2, lzma]
if USE_SNAPPY:
    COMPRESSION_FCNS = [snappy] + COMPRESSION_FCNS

def main():
    for filepath in args.files:
        file = os.path.basename(filepath)
        print "Compressing file: %s" % file
        if not os.path.exists(filepath):
            raise Exception("could not find file: %s" % filepath)
        orig_size = os.path.getsize(filepath)
        print "\torig: %i" % orig_size
        for fcn in COMPRESSION_FCNS:
            print "\t%s:" % fcn.__name__,
            output, output_filepath = fcn(filepath)
            if not os.path.exists(filepath):
                size = -1
            else:
                size = os.path.getsize(output_filepath)
            ratio = float(orig_size) / float(size)
            print "%i (%0.2f)" % (size, ratio)

if __name__ == '__main__':
    main()
