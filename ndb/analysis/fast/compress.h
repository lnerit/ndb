#include <zlib.h>
#define BUFSIZE (10 << 20)
#define FIRST_PACKET_ENCODE (0xf)

static inline int varint_encode(u32 value, u8 *target) {
	u8 *src = (u8 *)&value;

	*target++ = *src++;
	if (likely(value <= 0xff))
		return 1;

	*target++ = *src++;
	if (value <= 0xffff)
		return 2;

	*target++ = *src++;
	if (value <= 0xffffff)
		return 3;

	*target++ = *src++;
	return 4;
}

static inline u32 varint_decode(int len, u8 *src) {
	u32 ret = 0;
	u8 *target = (u8 *)&ret;

	do {
		*target++ = *src++;
	} while (--len);

	return ret;
}


struct FieldRecord {
#if defined (FIXED_LENGTH_VALUES)
	u8 field_nr;
#else
	/* TODO: ensure endian-ness is correct. */
	u8 field_nr : 6;
	u8 value_len : 2;
#endif
	u8 field_value[0];
} __attribute__((packed));

struct DiffRecord {
	u32 packet_ref : 28;
	u32 num_changes : 4;
	FieldRecord records[0];

	void print(u32 seq) {
		int offset = 0;
		printf("%u: DiffRecord <ref: %d, nchg: %d> {\n",
		       seq, packet_ref, num_changes);

		if (num_changes == FIRST_PACKET_ENCODE)
			goto done;

		for (int i = 0; i < num_changes; i++) {
			FieldRecord *field = (FieldRecord*)(((u8 *)this->records) + offset);
			int field_nr = field->field_nr;
			Header key = static_cast<Header>(field_nr);
			u32 value = 0;
			int len;

#if not defined (FIXED_LENGTH_VALUES)
			len = field->value_len + 1;
			value = varint_decode(len, field->field_value);
			offset += 1 + len;
#else
			len = HEADER_WRITE_BITS[key]/8;
			memcpy(&value, field->field_value, len);
			offset += 1 + len;
#endif
			/* Finish decoding */
			printf("\t%s: 0x%llx,\n", HEADER_NAMES[key].c_str(), (u64)value);
		}

	done:
		printf("};\n");
	}

} __attribute__((packed));
