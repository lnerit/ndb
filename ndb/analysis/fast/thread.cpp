
typedef pair<FlowKey, Packet> QueueItem;
struct ThreadData {
	int id;
	bool done;

	pthread_t thread;
	queue<QueueItem> q;
	u32 qsize;
	pthread_spinlock_t qmutex;

	pthread_cond_t data_present;
	pthread_mutex_t dmutex;

	FlowHashTable flows;
	u32 num_flows;

	ThreadData() {
		id = 0;
		done = false;
		num_flows = 0;
		pthread_cond_init(&data_present, NULL);
		pthread_mutex_init(&dmutex, NULL);
		pthread_spin_init(&qmutex, 0);
		flows.rehash(1 << 20);
	}

	/* This function waits for data on the queue.  Signals can be
	 * lost, so we wait until timeout. */
	void wait() {
		struct timespec ts;
		ts.tv_sec = 0;
		ts.tv_nsec = 0;
		pthread_mutex_lock(&dmutex);
		pthread_cond_timedwait(&data_present, &dmutex, &ts);
		pthread_mutex_unlock(&dmutex);
	}

	/* Signal the waiter that there is some data (or that it
	 * needs to exit) */
	void signal() {
		pthread_mutex_lock(&dmutex);
		pthread_cond_signal(&data_present);
		pthread_mutex_unlock(&dmutex);
	}
};

ThreadData threads[8];

void stick_this_thread_to_core(int core_id) {
	int num_cores = sysconf(_SC_NPROCESSORS_ONLN);
	if (core_id >= num_cores)
		return;

	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(core_id, &cpuset);

	pthread_t current_thread = pthread_self();
	int return_val = pthread_setaffinity_np(current_thread, sizeof(cpu_set_t), &cpuset);
}

void *consumer_thread(void *ptr) {
	ThreadData *td = (ThreadData *)ptr;
	queue<QueueItem> *q = &td->q;

	printf("Consumer %d binding to cpu %d\n", td->id, td->id);
	stick_this_thread_to_core(td->id);

	while (1) {
		QueueItem item;
		// td->wait();

		if (unlikely(td->done)) {
			break;
		}

		pthread_spin_lock(&td->qmutex);
		if (likely(q->size())) {
			item = q->front();
			q->pop();
			td->qsize--;
		} else {
			pthread_spin_unlock(&td->qmutex);
			continue;
		}
		pthread_spin_unlock(&td->qmutex);

		FlowKey &key = item.first;
		Packet &pkt = item.second;
		td->num_flows += td->flows[key].add_packet(pkt, global_flow_stats[td->id]);
	}

	printf("Consumer %d finishing\n", td->id);
	pthread_exit(NULL);
}


void thread_init() {
	for (int i = 0; i < NR_THREADS; i++) {
		ThreadData *td = &threads[i];
		td->id = i;
		int ret = pthread_create(&td->thread, NULL, consumer_thread, (void *)td);

		if (ret) {
			printf("Thread %d failed to create\n", i);
			exit(-1);
		}
	}

	stick_this_thread_to_core(NR_THREADS);
	printf("Main thread binding to cpu %d\n", NR_THREADS);
}

void thread_aggregate(FlowStats *stats, JSON &json) {
	u64 max_duration_sec = 0;
	FlowHashTable &table = threads[0].flows;
	EACH(it, table) {
		Flow &flow = it->second;
		stats->PacketsPerFlow[flow.packets]++;
		stats->num_packets += flow.packets;
	}

	print_table_int(stats->PacketsPerFlow, "Packets per flow", 0.01, json);

#if 0
	print_table(stats->FieldsChanged, HEADER_NAMES, "Fields that changed");
	print_table_int(stats->NumCompressedFields, "Number of changes per packet", 0.0);
	print_table_int(stats->TCPSeqDeltas, "TCP Seq deltas", 0.01);
	print_table_int(stats->TCPAckDeltas, "TCP Ack deltas", 0.01);

	REP(i, NR_THREADS) {
		FlowHashTable &table = threads[i].flows;
		EACH(it, table) {
			Flow &flow = it->second;
			stats->PacketsPerFlow[flow.packets]++;
			stats->total_compressed_bits += flow.compressed_size_bits + flow.first_packet_size * 8;
			stats->total_bytes += flow.bytes;
			stats->num_packets += flow.packets;
			max_duration_sec = max(max_duration_sec, (u64)(flow.prev.ts.tv_sec - flow.first.ts.tv_sec));
		}
	}

	stats->max_duration_sec = max_duration_sec;
	stats->total_compressed_bytes = stats->total_compressed_bits / 8;

	print_table_int(stats->PacketsPerFlow, "Packets per flow", 0.01);

	printf("Total compressed size: %llu bits / %llu bytes,  pcap ratio: %.3lf%%,  ratio: %.3lf%%\n",
	       stats->total_compressed_bits, stats->total_compressed_bytes,
	       pcap_read_bytes * 100.0 / stats->total_bytes,
	       stats->total_compressed_bits * 100.0 / 8.0 / stats->total_bytes);

	printf("\tbytes-per-packet: %.3lf\n",
	       stats->total_compressed_bytes * 1.0 / stats->num_packets);

	printf("Max duration of flow: %llus\n", stats->max_duration_sec);
#endif
}

void thread_finish(JSON &json) {

	for (int i = 0; i < NR_THREADS; i++) {
		ThreadData *td = &threads[i];
		td->done = true;
		pthread_join(td->thread, NULL);

		if (i > 0) {
			global_flow_stats[0]->update(global_flow_stats[i]);
		}

/*
		EACH(it, threads[i].flows) {
			flows[it->first] = it->second;
		}
*/
	}

	thread_aggregate(global_flow_stats[0], json);
}

