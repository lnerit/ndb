
import os
import sys
import argparse
import matplotlib
import matplotlib.cm
import matplotlib as mp
import matplotlib.pyplot as plt
#from colour import Color # sigh...
from ndb.plot import plot_defaults_bpp_uspp

import numpy as np
from collections import defaultdict

parser = argparse.ArgumentParser()
parser.add_argument("--csv",
                    default="-")

parser.add_argument('--out', '-o')
args = parser.parse_args()

"""
Read in trace, method, bpp, uspp from input/csv file and scatter plot.
These values can be obtained by running

    analysis/fast/parse.py --dir dir1 [dir2...]

It will output a csv which can be piped to this plotter.
"""

LOCATIONS = ["dc", "campus", "wan"]

SHAPES = {
    'pcap': 's',  # square
    'gz': '^',  # triangle
    'ns': 'o', # circle,
    'ns+gz': '*', # star
    }

#import matplotlib.pyplot as plt
#fig, ax = plt.subplots()
#color_cycle = ax._get_lines.color_cycle
# the default colour cycle in matplotlib
COLOURS = 'bgrcmyk'

# More colours...
NUM_COLOURS = 15
cmap = mp.cm.get_cmap(name='spectral')
MORECOLOURS = [cmap(i) for i in np.linspace(0, 0.9, NUM_COLOURS)]
COLOUR_BY_TRACE = {
    'wan': 'cyan',
    'dc': 'orange',
    'campus': 'green'
    }
file = sys.stdin
if args.csv != "-":
    file = open(args.csv)

mp.rc("figure", figsize=(8, 5.5))
data = defaultdict(list)
for line in file.xreadlines():
    if line.startswith('#'):
        continue

    trace, method, bpp, uspp = line.strip().split(',')
    bpp, uspp = map(float, [bpp, uspp])
    data[trace].append((method, bpp, uspp))

style = {}
debug = {}
for i, trace in enumerate(data.keys()):
    #col = MORECOLOURS[i]
    col = 'red'
    type = 'wan'
    if "equinix" in trace:
        type = 'wan'
    elif "univ" in trace:
        type = 'dc'
    elif "clemson" in trace:
        type = 'campus'
    col = COLOUR_BY_TRACE[type]
    #col = Color(col)
    #col.saturation = 0.8
    #col.luminance = 0.5
    for method, x, y in data[trace]:
        method = method.replace('vj', 'ns')
        if method == 'nsgz':
            method = 'ns+gz'
        shape = SHAPES[method]
        p = plt.plot(x, y, ls='', color=col, marker=shape, markersize=15)
        style[shape] = (method, p[0])


# Manually create the method legends
def add_method_legends():
    lines = []
    text = []
    for name in ['pcap', 'gz', 'ns', 'ns+gz']:
        shape = SHAPES[name]
        line = plt.Line2D(range(1), range(1), color="white",
                          marker=shape, markersize=15,
                          markerfacecolor="grey")
        lines.append(line)
        text.append(name.upper())
    L = plt.legend(lines, text, numpoints=1)
    plt.gca().add_artist(L)

def add_location_legends():
    lines = []
    text = []
    for loc in LOCATIONS:
        line = plt.Rectangle((0, 0), 10, 10, color=COLOUR_BY_TRACE[loc])
        lines.append(line)
        text.append(loc)
    L = plt.legend(lines, text, loc="lower right")
    plt.gca().add_artist(L)

add_location_legends()
add_method_legends()

plt.grid(True)
plt.xlim((0, None))
plt.ylim((0, 6))

plt.xlabel("Storage cost (bytes per pkt)")
plt.ylabel("CPU cost (usec per pkt)")
if args.out:
    plt.savefig(args.out)
    print 'saved to', args.out
else:
    plt.show()
