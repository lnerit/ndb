#ifndef __TYPES_H__
#define __TYPES_H__

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;

struct FlowKey;
struct Flow;
struct HashFlowKey;
typedef unordered_map<FlowKey, Flow, HashFlowKey> FlowHashTable;
struct Compressor;

typedef u32 *HVArray;
struct proto_stats {
	u32 count;
	u64 bytes;
};

#endif /* __TYPES_H__ */
