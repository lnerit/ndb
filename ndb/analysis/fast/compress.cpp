#include <cassert>
using namespace std;

#if not defined(SINGLE_THREADED)
#error "Beware: Compressor (when writing to disk) is probably not thread safe."
#endif
#include "compress.h"

void print_timestamp(struct timeval ts) {
	printf("val: %ld.%06ld\n", ts.tv_sec, ts.tv_usec);
}

FILE *dieopenw(string filename, int buffersize) {
	FILE *fp;
	char buff[128];
	static int count = 0;

	if (!MEMORY_OPEN) {
		fp = fopen(filename.c_str(), "wb");
	} else {
		sprintf(buff, "/tmp/fast-pcap-%d", count++);
		fp = fopen(buff, "w");
	}

	if (!fp) {
		fprintf(stderr, "Cannot open file %s for writing\n", filename.c_str());
		exit(-1);
	}

	setvbuf(fp, NULL, _IOFBF, buffersize);

	return fp;
}

gzFile compressed_stream(FILE *fp) {
	gzFile ret = gzdopen(fileno(fp), "w");

	if (ret == NULL) {
		fprintf(stderr, "Couldn't convert file %p to gzip stream.\n", fp);
		exit(-1);
	}

#ifdef gzbuffer
	gzbuffer(ret, BUFSIZE);
#endif
	/* Turns out higher compression levels don't work that well */
	gzsetparams(ret, COMPRESSION_LEVEL, Z_DEFAULT_STRATEGY);
	return ret;
}

/*
 * Use this: www.stanford.edu/class/cs276/Jeff-Dean-compression-slides.pdf
 * Group Varint Encoding snippet: http://www.oschina.net/code/snippet_12_5083
 * http://videolectures.net/wsdm09_dean_cblirs/
 */
struct Compressor {
	string ts_filename;
	string firstpkt_filename;
	string diff_filename;

	FILE *fp_ts;
	FILE *fp_firstpkt;
	FILE *fp_diff;

	gzFile fp_ts_comp;
	gzFile fp_firstpkt_comp;
	gzFile fp_diff_comp;

	struct timeval ts_prev;
	u32 first_packet_id;
	bool write_to_disk;

	u64 diff_size, diff_csize;
	u64 firstpkt_size, firstpkt_csize;
	u64 ts_delta_size, ts_delta_csize;
	u64 num_packets;
	u64 desc_size;

	u32 NumFieldChanged[NUM_FIELDS];
	u32 NumChangePerPacket[20];
	u64 TotalFieldBytes[NUM_FIELDS];
	u64 NumNonOneIPID;

	Compressor(char *pcap_filename, bool write_disk = false, int buffersize = BUFSIZE) {
		ts_filename = string(pcap_filename);
		ts_filename += ".ts.delta";
		fp_ts = dieopenw(ts_filename, buffersize);
		fp_ts_comp = compressed_stream(fp_ts);

		firstpkt_filename = string(pcap_filename);
		firstpkt_filename += ".firstpackets";
		fp_firstpkt = dieopenw(firstpkt_filename, buffersize);
		fp_firstpkt_comp = compressed_stream(fp_firstpkt);

		diff_filename = string(pcap_filename);
		diff_filename += ".diffs";
		fp_diff = dieopenw(diff_filename, buffersize);
		fp_diff_comp = compressed_stream(fp_diff);

		ts_prev.tv_sec = ~0;
		first_packet_id = 0;
		write_to_disk = write_disk;

		diff_size = 0, diff_csize = 0;
		firstpkt_size = 0, firstpkt_csize = 0;
		ts_delta_size = 0, ts_delta_csize = 0;
		desc_size = 0;
		num_packets = 0;

		bzero(NumFieldChanged, sizeof NumFieldChanged);
		bzero(NumChangePerPacket, sizeof NumChangePerPacket);
		bzero(TotalFieldBytes, sizeof TotalFieldBytes);
		NumNonOneIPID = 0;
	}

	void seek_end() {
		fflush(fp_ts);
		fflush(fp_firstpkt);
		fflush(fp_diff);

		fseek(fp_ts, 0, SEEK_END);
		fseek(fp_firstpkt, 0, SEEK_END);
		fseek(fp_diff, 0, SEEK_END);

		/* Note down compressed file sizes */
		ts_delta_csize = ftell(fp_ts);
		firstpkt_csize = ftell(fp_firstpkt);
		diff_csize = ftell(fp_diff);
	}

	void flush_compress() {
		gzflush(fp_ts_comp, Z_FINISH);
		gzflush(fp_firstpkt_comp, Z_FINISH);
		gzflush(fp_diff_comp, Z_FINISH);
	}

	void close() {
		flush_compress();
		if (!fp_ts) return;

		seek_end();
		fclose(fp_ts);
		fclose(fp_firstpkt);
		fclose(fp_diff);

		fp_ts = NULL;
		fp_firstpkt = NULL;
		fp_diff = NULL;
	}

	double bpp_normal() {
		seek_end();
		u64 total_size = diff_size + firstpkt_size + ts_delta_size;
		return total_size * 1.0 / num_packets;
	}

	double bpp_compress() {
		seek_end();
		u64 total_csize = diff_csize + firstpkt_csize + ts_delta_csize;
		return total_csize * 1.0 / num_packets;
	}

	void stats(JSON &j) {
		u64 total_size = diff_size + firstpkt_size + ts_delta_size;
		u64 total_csize = diff_csize + firstpkt_csize + ts_delta_csize;
		float avg, total;
		JSON jstat;

		flush_compress();

		printf("Compressor: diff %llu, first %llu, ts %llu, desc %llu, total %llu\n",
		       diff_size, firstpkt_size, ts_delta_size, desc_size,
		       total_size);

		j["num_packets"] = V(num_packets);
		j["diff_size"] = V(diff_size);
		j["firstpkt_size"] = V(firstpkt_size);
		j["ts_delta_size"] = V(ts_delta_size);
		j["desc_size"] = V(desc_size);
		j["total_size"] = V(total_size);
		j["bpp"] = V(bpp_normal());

		printf("bpp: diff %.3lf, first %.3f, ts %.3f, desc %.3f, total %.3f\n",
		       diff_size * 1.0 / num_packets,
		       firstpkt_size * 1.0 / num_packets,
		       ts_delta_size * 1.0 / num_packets,
		       desc_size * 1.0 / num_packets,
		       total_size * 1.0 / num_packets);

		printf("bpp: diff %.3lf, first %.3f, ts %.3f, total %.3f\n",
		       diff_csize * 1.0 / num_packets,
		       firstpkt_csize * 1.0 / num_packets,
		       ts_delta_csize * 1.0 / num_packets,
		       total_csize * 1.0 / num_packets);

		j["diff_csize"] = V(diff_csize);
		j["firstpkt_csize"] = V(firstpkt_csize);
		j["ts_delta_csize"] = V(ts_delta_csize);
		j["total_csize"] = V(total_csize);
		j["gzbpp"] = V(bpp_compress());

		j["non_one_ipid_changes"] = V(NumNonOneIPID);

		printf("\nNumber of times a particular field changed per packet\n");
		total = 0;
		REP(i, NUM_FIELDS) total += NumFieldChanged[i];

		jstat = JSON();
		REP(i, NUM_FIELDS) {
			if (!NumFieldChanged[i]) continue;
			float bpp = TotalFieldBytes[i] * 1.0 / num_packets;
			string header = HEADER_NAMES[static_cast<Header>(i)];
			JSON ele;
			ele["count"] = V((u64)NumFieldChanged[i]);
			ele["bytes"] = V((u64)TotalFieldBytes[i]);

			printf("%s: %u, %.3f%%, %.3f bpp\n",
			       header.c_str(),
			       NumFieldChanged[i],
			       NumFieldChanged[i] * 100.0 / total,
			       bpp);

			jstat[header.c_str()] = picojson::value(ele);
		}

		j["NumFieldChangePerPacket"] = picojson::value(jstat);
		printf("*******\n");

		printf("Number of field changes per packet\n");
		avg = 0, total = 0;
		jstat = JSON();
		REP(i, 20) {
			if (!NumChangePerPacket[i]) break;
			printf("%d: %u\n",
			       i, NumChangePerPacket[i]);
			avg += i * NumChangePerPacket[i];
			total += NumChangePerPacket[i];
			jstat[ntos(i)] = V((u64)NumChangePerPacket[i]);
		}
		printf("Average: %.3f fchgs/packet\n", avg/total);
		j["FieldChangePerPacket"] = picojson::value(jstat);
	}

	~Compressor() {
		// stats();
		close();
	}

	/* These are for non-compressed diff records */
	template<class T>
	int EmitTimestamp(T *obj) {
		if (write_to_disk) {
			// fwrite((void *)obj, sizeof(T), 1, fp_ts);
			gzwrite(fp_ts_comp, (void *)obj, sizeof(T));
		}
		return sizeof(T);
	}

	int EmitFirstpacket(const u8 *payload, u8 caplen) {
		if (write_to_disk) {
			//fwrite((void *)&caplen, sizeof(caplen), 1, fp_firstpkt);
			//fwrite((void *)payload, caplen, 1, fp_firstpkt);
			gzwrite(fp_firstpkt_comp, (void *)&caplen, sizeof(caplen));
			gzwrite(fp_firstpkt_comp, (void *)payload, caplen);
		}
		return caplen + sizeof(caplen);
	}

	int EmitDiffRecord(u8 *buff, int diffsize) {
		int sz = sizeof(struct DiffRecord) + diffsize;
		if (write_to_disk) {
			//fwrite((void *)buff, sz, 1, fp_diff);
			gzwrite(fp_diff_comp, (void *)buff, sz);
		}
		return sz;
	}

	void write_time_stamp(struct timeval &ts) {
		/* First timestamp */
		if (unlikely(ts_prev.tv_sec == ~0)) {
			ts_delta_size += EmitTimestamp(&ts);
			printf("first timestamp ");
			print_timestamp(ts);
		} else {
			u64 usec_delta = (ts.tv_sec - ts_prev.tv_sec) * int(1e6);
			usec_delta += (ts.tv_usec - ts_prev.tv_usec);

			if (usec_delta >= UINT_MAX) {
				fprintf(stderr, "Timestamp is wrapping\n");
			}

			u32 val = usec_delta;
			ts_delta_size += EmitTimestamp(&val);
		}

		ts_prev = ts;
	}

	u32 write_first_header(Packet &pkt) {
		firstpkt_size += EmitFirstpacket(pkt.payload, pkt.caplen);
		return first_packet_id++;
	}

	bool ignore_header(Header key) {
		return false; //key == IP_CSUM or key == TCP_CSUM;
	}

	/* TODO: Switch to using "Emit()" functions as a narrow waist for marshalling data */
	void write_diff_packet(Flow &flow, Packet &curr, int first_packet_id) {
		u8 buff[100];
		DiffRecord *diff = (DiffRecord *)(&buff[0]);
		FieldRecord *field = diff->records;
		int diffsize = 0;
		u32 *hv_prev = NULL;
		u32 hv_curr[NUM_FIELDS];

		hv_prev = flow.get_prev_harray();
		memset(hv_curr, -1, sizeof (hv_curr));
		curr.get_headers_opt(hv_curr);

		desc_size += 1;

		if (first_packet_id >= 0) {
			diff->packet_ref = first_packet_id;
			diff->num_changes = FIRST_PACKET_ENCODE;
			goto write;
		} else {
			diff->packet_ref = flow.prev.seq;
			diff->num_changes = 0;
		}

		for (int i = IP_TOS_F; i < NUM_FIELDS; i++) {
			Header key = static_cast<Header>(i);
			auto value = hv_curr[i];

			if (key != IP_ID and (value == hv_prev[key] or value == ~0))
				continue;

			if (ignore_header(key))
				continue;

			if (key == TCP_SEQ or key == TCP_ACK or key == IP_ID) {
				value = hv_curr[i] - hv_prev[i];
			}

			if (key == IP_ID) {
				if (value == 1) {
					continue;
				} else {
					NumNonOneIPID++;
				}
			}

			if (HEADER_WRITE_BITS[key] == 16)
				value = value & 0xffff;

			NumFieldChanged[key]++;
			diff->num_changes++;
			field = encode(field, key, value, diffsize);
		}

	write:
		if (VERBOSE)
			diff->print(curr.seq);

		diff_size += EmitDiffRecord(buff, diffsize);
		NumChangePerPacket[diff->num_changes]++;
	}

	void write(Flow &flow, Packet &curr, int first_packet_id=-1) {
		write_time_stamp(curr.ts);
		write_diff_packet(flow, curr, first_packet_id);
		num_packets++;
	}

#if not defined(FIXED_LENGTH_VALUES)
	/* Encodes key, value into curr and returns the next pointer */
	FieldRecord *encode(FieldRecord *curr, Header key, u32 value, int &diffsize) {
		int size;
		curr->field_nr = static_cast<u8>(key);
		size = varint_encode(value, curr->field_value);
		TotalFieldBytes[key] += 1 + size;
		curr->value_len = size - 1;
		assert (1 <= size and size <= 4);
		diffsize += 1 + size;
		desc_size += 1;
		/*
		if (VERBOSE)
			printf("field %d/%d, len %d/%d, value %d\n",
			       static_cast<u8>(key), curr->field_nr,
			       size, curr->value_len, value);
		*/
		return (FieldRecord *)(((u8 *) curr) + 1 + size);
	}
#endif

#if defined(FIXED_LENGTH_VALUES)
	FieldRecord *encode(FieldRecord *curr, Header key, u32 value, int &diffsize) {
		int size = HEADER_WRITE_BITS[key] / 8;
		curr->field_nr = static_cast<u8>(key);
		TotalFieldBytes[key] += size;
		memcpy(curr->field_value, &value, size);
		/* Advance to the next field */
		diffsize += 1 + size;
		desc_size += 1;
		return (FieldRecord *)(((u8 *) curr) + 1 + size);
#undef CASE
	}
#endif
};
