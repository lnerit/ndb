#include <map>
#include <unordered_map>
#include <functional>
#include <vector>
#include <array>
#include <algorithm>
#include <string>
#include <queue>
using namespace std;
#include <pcap.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <unistd.h>
#include <limits.h>

#include <zlib.h>
#include <pcap.h>

/* Simple json: just a header file
 * Github: https://github.com/kazuho/picojson
 * Examples: http://mbed.org/users/mimil/code/PicoJSONSample/docs/81c978de0e2b/main_8cpp_source.html
 */
#include "picojson.h"
#define V(x) picojson::value(x)

#include "types.h"

const int FLOW_EXP_SEC = 10;

// TODO: remove these and put it in ONE place; shared with fast.cpp!
#define OUT_SNAPLEN (64)
#define USEC_PER_SEC (1e6)
// TODO: these also belong in a more sensible, shared place.
#define MAX_CAPLEN (128)
#define MAX_DIFF_SIZE (100)

int NR_THREADS = 1;

char *JSON_OUTPUT = NULL;

const int LOOP_STATS_INTERVAL = 1;
bool VERBOSE = false;
bool DUMP_READ_PCAP = false;
u64 MAX_PACKETS = ~0;
int SKIP_ETHERNET = 0;

/* The final stats; use in slowpath only. */
typedef picojson::object JSON;
JSON json;


#define SINGLE_THREADED 1
#define LET(var,x) auto var(x)
#define EACH(it, cont) for (LET(it, (cont).begin()); it != (cont).end(); ++it)
#define REP(i, n) for (int i = 0; i < (n); ++i)

#define likely(x)       __builtin_expect((x),1)
#define unlikely(x)     __builtin_expect((x),0)

struct proto_stats proto_count[1 << 16];
struct proto_stats ip_proto_count[1 << 8];

/*
map<u16, struct proto_stats> ip_proto_count;
map<u16, struct proto_stats> proto_count;
*/
unordered_map<u16, u32> packet_sizes;
unordered_map<u32, u32> ip_count;

time_t loop_start_time;
time_t loop_finish_time;
u64 pcap_filesize;
u64 pcap_read_bytes;
u64 pcap_bytes_on_wire;
u64 pcap_num_packets;
u64 num_flows;

char *pcap_filename;

#include "packet.cpp"
#include "flow.cpp"
#include "thread.cpp"
#include "helper.cpp"
#include "compress.h"


void print_timestamp(struct timeval ts) {
	printf("timestamp val: %ld.%06ld\n", ts.tv_sec, ts.tv_usec);
}

FILE *dieopenr(string filename, int buffersize) {
	FILE *fp = fopen(filename.c_str(), "r");
	if (!fp) {
		fprintf(stderr, "Cannot open file %s for reading\n", filename.c_str());
		exit(-1);
	}

	// Specify full file buffering.
	setvbuf(fp, NULL, _IOFBF, buffersize);

	return fp;
}

FILE *dieopenw(string filename, int buffersize) {
    FILE *fp = fopen(filename.c_str(), "w");
    if (!fp) {
        fprintf(stderr, "Cannot open file %s for writing\n", filename.c_str());
        exit(-1);
    }

    // Specify full file buffering.
    setvbuf(fp, NULL, _IOFBF, buffersize);

    return fp;
}

gzFile compressed_read_stream(FILE *fp) {
	return gzdopen(fileno(fp), "r");
}

gzFile compressed_write_stream(FILE *fp) {
    return gzdopen(fileno(fp), "w");
}

string time_string(struct timeval tv) {
	time_t nowtime;
	struct tm *nowtm;
	char tmbuf[64], buf[64];

	nowtime = tv.tv_sec;
	nowtm = localtime(&nowtime);
	strftime(tmbuf, sizeof tmbuf, "%Y-%m-%d %H:%M:%S", nowtm);
	snprintf(buf, sizeof buf, "%s.%06u", tmbuf, (u32)tv.tv_usec);
	return string(buf);
}


inline void update_stats(const struct pcap_pkthdr *hdr) {
//!
    struct proto_stats &stat = proto_count[ETHERTYPE_IP]; //pkt.eth.proto];
//    if (pkt.eth.proto == ETHERTYPE_IP) {
//        struct proto_stats &ipstat = ip_proto_count[pkt.ip.proto];
//        ipstat.count++;
//        ipstat.bytes += hdr->len;
//    }

    stat.count++;
    stat.bytes += hdr->len;
    pcap_bytes_on_wire += hdr->len;

    packet_sizes[hdr->len] += 1;
    pcap_num_packets += 1;

//    ip_count[pkt.nw_src()]++;
//    ip_count[pkt.nw_dst()]++;
}


void loop_start() {
    loop_start_time = time(NULL);
}

float loop_stats(gzFile pcap_file_comp, struct pcap_pkthdr *hdr) {
    static time_t prev_time = 0;
    static u64 prev_read = 0, prev_bytes_on_wire = 0;
    static bool first = true;
    static struct timeval pcap_begin;
    static struct timeval start, curr;

    u64 pcap_sec;
    time_t now = time(NULL);
    u32 delta = now - prev_time;
    float ret = 0;

    if (delta > LOOP_STATS_INTERVAL) {
        if (first) {
            pcap_begin = hdr->ts;
            gettimeofday(&start, NULL);
            first = false;
        }
        JSON jstat;

        gettimeofday(&curr, NULL);
        double dt_usec = (curr.tv_sec - start.tv_sec) * USEC_PER_SEC;
        dt_usec += (curr.tv_usec - start.tv_usec);

        pcap_sec = hdr->ts.tv_sec - pcap_begin.tv_sec;
        pcap_read_bytes = gztell(pcap_file_comp);
        printf("pcap_read_bytes: %u\n", (uint) pcap_read_bytes);

        float speed = (pcap_read_bytes - prev_read) * 8.0 / delta / 1e6;
        float wire_speed = (pcap_bytes_on_wire - prev_bytes_on_wire) * 8.0 / delta / 1e6;
        prev_bytes_on_wire = pcap_bytes_on_wire;
        if (speed < 1e-3) speed = 1;

        float percent = pcap_read_bytes * 100.0 / pcap_filesize;
        float est = (pcap_filesize - pcap_read_bytes) * 8.0 / 1e6 / speed;
        u64 mem_kb = memory_usage_kb();

        printf("read %llu/%llu bytes, num-flows %llu, keys %llu "
               "percent %.2f%%, pcap-speed %.3f Mb/s, est-rem %.1fs, "
               "wire-speed %.3f Mb/s, mem %.3fGB\n ",
               pcap_read_bytes, pcap_filesize, num_flows, (u64)threads[0].flows.size(),
               percent, speed, est,
               wire_speed, mem_kb/1e6);

        jstat["read"] = V(pcap_read_bytes);
        jstat["total"] = V(pcap_filesize);
        jstat["num_flows"] = V(num_flows);
        jstat["keys"] = V((u64)(threads[0].flows.size()));
        jstat["percent"] = V(percent);
        jstat["pcap-speed"] = V(speed);
        jstat["wire-speed"] = V(wire_speed);
        jstat["memgb"] = V(mem_kb/1e6);

        printf("uspp %.3lf, fpp %.3lf, mem-bpp %.3lf ",
               dt_usec / pcap_num_packets,
               num_flows * 1.0 / pcap_num_packets,
               mem_kb * 1e3 / pcap_num_packets);

        jstat["uspp"] = V(dt_usec / pcap_num_packets);
        jstat["fpp"] = V(num_flows * 1.0 / pcap_num_packets);

        picojson::array &arr = json["running-stats"].get<picojson::array>();
        arr.push_back(picojson::value(jstat));

#if not defined(SINGLE_THREADED)
        printf(" qsizes:");
        for (int i = 0; i < NR_THREADS; i++) {
            printf("%llu,", (u64) threads[i].q.size());
        }
#endif

        printf("\n");

        prev_time = now;
        prev_read = pcap_read_bytes;
        ret = percent;
    }

    return ret;
}

void scan_stats() {
    num_flows = 0;
#if defined SINGLE_THREADED
    num_flows = threads[0].num_flows;
#endif

    REP(i, NR_THREADS) {
        ThreadData *td = &threads[i];
        num_flows += td->num_flows;
    }
}

static inline u64 ts_to_usec(struct timeval &ts) {
    return ts.tv_sec * USEC_PER_SEC + ts.tv_usec;
}

static inline u64 ts_diff_usec(struct timeval &end, struct timeval &start) {
    u64 us = 0;
    us += (end.tv_sec - start.tv_sec) * USEC_PER_SEC;
    us += end.tv_usec - start.tv_usec;
    return us;
}




struct Decompressor {
	string prefix;
	string ts_filename;
	string firstpkt_filename;
	string diff_filename;
	string out_filename;

	FILE *fp_ts;
	FILE *fp_firstpkt;
	FILE *fp_diff;
	FILE *fp_out;

	gzFile fp_ts_comp;
	gzFile fp_firstpkt_comp;
	gzFile fp_diff_comp;
	gzFile fp_out_comp;

    pcap_t *out_pcap;
    pcap_dumper_t *out_dumper;

    // Data initialized from the files.
    vector<struct timeval> ts;
    //!vector<Packet> first_packets;
    vector<FastPacket> first_packets_fast;
    struct timeval ts_first;

    // Intermediate data structures used during decompression:
	struct timeval ts_prev;
	// recent_packets stores the most recently-seen packet for each flow.
	// Required because the compressor output format gives a reference to
	// either:
	//    (1) the first packet of a flow, if nchanges is zero
	// or (2) the last-seen packet of a flow
	//unordered_map<uint, Packet> recent_packets;
    unordered_map<uint, FastPacket> recent_packets_fast;

	// TODO: document the meaning of these things.
	int num_packets;
	u32 seq;

	Decompressor(char *prefix, int buffersize = BUFSIZE) {

	    ts_filename = string(prefix);
		ts_filename += ".ts.delta";
		fp_ts = dieopenr(ts_filename, buffersize);
		fp_ts_comp = compressed_read_stream(fp_ts);

		firstpkt_filename = string(prefix);
		firstpkt_filename += ".firstpackets";
		fp_firstpkt = dieopenr(firstpkt_filename, buffersize);
		fp_firstpkt_comp = compressed_read_stream(fp_firstpkt);

		diff_filename = string(prefix);
		diff_filename += ".diffs";
		fp_diff = dieopenr(diff_filename, buffersize);
		fp_diff_comp = compressed_read_stream(fp_diff);

		out_filename = string(prefix);
		out_filename += ".out.pcap";
        fp_out = dieopenw(out_filename, buffersize);
        fp_out_comp = compressed_write_stream(fp_out);

		seq = 0;

	    pcap_filesize = get_file_size(diff_filename.c_str());

		printf("Size of DiffRecord: %dB + var\n", (int)sizeof (struct DiffRecord));
		printf("Size of FieldRecord: %dB + var\n", (int)sizeof (struct FieldRecord));

	}

	void close() {
	    scan_stats();
        loop_finish();
        stats_finish(json);
        dump_json();

	    pcap_dump_close(out_dumper);
	    if (fp_ts) fclose(fp_ts);
		if (fp_firstpkt) fclose(fp_firstpkt);
		if (fp_diff) fclose(fp_diff);
        if (fp_out) fclose(fp_out);
		fp_ts = NULL;
		fp_firstpkt = NULL;
		fp_diff = NULL;
		fp_out = NULL;

	}

	~Decompressor() {
		close();
	}

	void setup() {
        out_pcap = pcap_open_dead(DLT_EN10MB, OUT_SNAPLEN);
        if (out_pcap == NULL) {
            printf("Couldn't open out_pcap for pcap output\n");
            exit(EXIT_FAILURE);
        }

        out_dumper = pcap_dump_open(out_pcap, out_filename.c_str());
        if (out_dumper == NULL) {
            printf("Error in opening %s\n", out_filename.c_str());
            exit (EXIT_FAILURE);
        }
	}

	void decompress() {
	    setup();
	    loop_start();
	    json["running-stats"] = picojson::value(picojson::array());
	    read_first_timestamp();
	    read_all_ts();
	    // c.print_all_ts();
        read_all_first_packets();
        //print_all_first_packets();
        int i;
        float percent;
        struct pcap_pkthdr hdr;
        //Packet p;

        //!printf("num first packets: %i\n", (int) first_packets.size());
        printf("num first packets (fast): %i\n", (int) first_packets_fast.size());
        int packets_to_read = ts.size();
        if (MAX_PACKETS < packets_to_read)
            packets_to_read = MAX_PACKETS;
        for (i = 0; i < packets_to_read; i++) {
            if (VERBOSE)
                printf("\n-------------------------\n");
            FastPacket p_fast;
            read_one_diff(&hdr, p_fast);
            update_stats(&hdr);
            percent = loop_stats(fp_diff_comp, &hdr);
        }
        printf("done with diffs\n");
        if (DUMP_READ_PCAP) {
            printf("wrote pcap output to %s\n", out_filename.c_str());
        }
	}

	void read_first_timestamp() {
		//int nread;
		//nread = fread((void *)&ts_first, sizeof ts_first, 1, fp_ts);
		//assert (nread == 1);
		int bytes_read;
		bytes_read = gzread (fp_ts_comp, &ts_first, sizeof ts_first);
		ts_prev = ts_first;
		assert (bytes_read == sizeof ts_first);
		ts.push_back(ts_first);
		//printf("first_ts bytes_read: %i\n", bytes_read);
		printf("first_ts: ");
		print_timestamp(ts_first);
	}

    u32 read_ts_deltas() {
        u32 ret;
        int nread;
        // Was:
        //nread = fread((void *)&ret, sizeof ret, 1, fp_ts);

        // Code derives from:
        //http://www.lemoda.net/c/gzfile-read/index.html
        if (!fp_ts_comp) {
            fprintf(stderr, "gzopen of '%s' failed: %s.\n", ts_filename.c_str(),
                    strerror(errno));
            exit(EXIT_FAILURE);
        }

        int counter = 0;
        while (1) {
            int err;
            int bytes_read;
            u32 delta;
            bytes_read = gzread(fp_ts_comp, &delta, sizeof delta);
            counter++;
            //printf("bytes_read: %i\n", bytes_read);
            //printf("counter: %i\n", counter);
            if (bytes_read < (int) (sizeof delta)) {
                if (gzeof(fp_ts_comp)) {
                    break;
                } else {
                    const char * error_string;
                    error_string = gzerror(fp_ts_comp, &err);
                    if (err) {
                        fprintf(stderr, "Error: %s.\n", error_string);
                        exit (EXIT_FAILURE);
                    }
                }
            }
            assert(bytes_read == sizeof delta);
            ts_prev.tv_usec += delta;
            while (ts_prev.tv_usec >= USEC_PER_SEC) {
                ts_prev.tv_usec -= USEC_PER_SEC;
                ts_prev.tv_sec += 1;
            }
            ts.push_back(ts_prev);
            //printf("read ");
            //print_timestamp(ts_prev);
        }
        return ret;
    }

    void read_all_ts() {
        read_ts_deltas();
        printf("%u timestamps read\n", (u32) ts.size());
    }

    void print_all_ts() {
        EACH(it, ts) {
            print_timestamp((struct timeval) *it);
        }
    }

	inline u8 read_caplen() {
		u8 caplen;
		//int nread = fread((void *)&caplen, sizeof caplen, 1, fp_firstpkt);
		int bytes_read;
        bytes_read = gzread(fp_firstpkt_comp, &caplen, sizeof caplen);

		if (!gzeof(fp_firstpkt_comp)) {
			//assert(nread == 1);
		    assert(bytes_read == sizeof caplen);
			return caplen;
		} else {
			return -1;
		}
	}

	void read_one_packet() {
		static int nr = 0;
		int caplen = read_caplen();
		if (caplen == 255)
			return;

		u8 buf[MAX_CAPLEN];
		int bytes_read;
		bytes_read = gzread(fp_firstpkt_comp, buf, caplen);
		assert(bytes_read == caplen);
		Packet pkt(buf, caplen, SKIP_ETHERNET, nr++, caplen);
        FastPacket pkt2(buf, caplen);

		//!first_packets.push_back(pkt);
        first_packets_fast.push_back(pkt2);
		// Reenable these prints to test the copy constructor
		//first_packets.back().print();
		//!first_packets.back().update();
		//first_packets.back().print();

	}

	void read_all_first_packets() {
	    int i = 0;
	    while (!gzeof(fp_firstpkt_comp)) {
			read_one_packet();
			if (0) {
                if (VERBOSE) {
                    //!printf("%i: %i\n", i, first_packets[i].caplen);
                }
			}
			i++;
	    }
		//!printf("%u first packets\n", (u32)first_packets.size());
        printf("%u first packets\n", (u32)first_packets_fast.size());
	}

    void print_all_first_packets() {
        int i;
        if (0) {
            //!for (i = 0; i < first_packets.size(); i++) {
            //!    first_packets[i].print();
            //!}
        }
    }

	u64 read_nbytes(int len) {
		u64 ret = 0;
		if (len == 0)
			return 0;
        int bytes_read;
        bytes_read = gzread(fp_diff_comp, &ret, len);

		if (bytes_read != len) {
            printf("Failed after reading %d (bits=%d) field records\n",
			       seq, len * 8);
		}
		assert(bytes_read == len);
		return ret;
	}

	void read_one_diff(struct pcap_pkthdr* hdr, FastPacket p_fast) {
		char buf[MAX_DIFF_SIZE];
		DiffRecord *diff = (DiffRecord *)(&buf[0]);
		FieldRecord *field = diff->records;
		int bytes_read;
        bytes_read = gzread(fp_diff_comp, buf, sizeof(struct DiffRecord));
		int offset = 0;

		if (gzeof(fp_diff_comp))
			return;

		assert(bytes_read == sizeof(struct DiffRecord));

		/* First packet of flow */
		if (diff->num_changes == FIRST_PACKET_ENCODE)
			goto done;

		for(int i = 0; i < diff->num_changes; i++) {
			FieldRecord *field = (FieldRecord *)((u8 *)diff->records + offset);
			u8 *byte = (u8 *)field;
			*byte = read_nbytes(1);
			int len;
			u32 value;

#if not defined (FIXED_LENGTH_VALUES)
			/* We use varint encoding with length packed
			 * in field->value_len
			 * len 1 -> 00 - binary
			 * len 2 -> 01
			 * len 3 -> 10
			 * len 4 -> 11
			 */
			len = (field->value_len + 1);
			Header key = static_cast<Header>(field->field_nr);
			value = read_nbytes(len);
			if (VERBOSE)
				printf("---> %d field %d/%d, len %d/%d, value %d\n",
				       *byte, static_cast<u8>(key), field->field_nr,
				       field->value_len, field->value_len, value);
			offset += 1 + len;
			assert (0 <= field->value_len and field->value_len <= 3);
			memcpy(field->field_value, &value, len);
#else
			Header key = static_cast<Header>(*byte);
			len = HEADER_WRITE_BITS[key]/8;
			value = read_nbytes(len);
			/* Let us not worry about 64 bit yet */
			assert (len <= 4);
			memcpy(field->field_value, &value, len);
			offset += 1 + len;
#endif
		}

	done:
	    if (VERBOSE)
			diff->print(seq);

		reconstruct_pcap(diff, seq, hdr, p_fast);
        seq++;
	}

	void reconstruct_pcap(DiffRecord* diff, uint packet_index, struct pcap_pkthdr* hdr, FastPacket p_fast) {
	    u8 buf[128]; // store packet buf to actually write to pcap
	    timeval t;
	    uint16_t packet_len; // original len - make this up?

	    assert(diff->packet_ref < ts.size());

        t = ts[packet_index];

	    if (diff->num_changes == FIRST_PACKET_ENCODE) {

            // If there are no diffs, the packet_ref refers to a first_pkt.
	        //!assert(diff->packet_ref < first_packets.size());
	        //! p = first_packets[diff->packet_ref];
            p_fast = first_packets_fast[diff->packet_ref];

            if (VERBOSE) {
                printf("FIRST PACKET (no mods for this record):\n");
                //!p.print_hex();
            }
	    }
	    else {
	        // iterate through each change, modifying packet.
	        int offset = 0;

	        // If there are diffs, they are relative to a previously seen
	        // (and stored) packet in the recent_packets table.
	        //!p = recent_packets[diff->packet_ref];
            p_fast = recent_packets_fast[diff->packet_ref];
	        //printf("PREVIOUS PACKET: \n");
	        //p.print_hex();

	        u8 saw_ip_diff = 0;

	        //printf("num changes: %i\n", diff->num_changes);
	        for (int i = 0; i < diff->num_changes; i++) {
	            FieldRecord *field = (FieldRecord*)(((u8 *)diff->records) + offset);
	            int field_nr = field->field_nr;
	            Header key = static_cast<Header>(field_nr);
	            u32 value = 0;
	            int len;

#if not defined (FIXED_LENGTH_VALUES)
	            len = field->value_len + 1;
	            value = varint_decode(len, field->field_value);
	            offset += 1 + len;
#else
	            len = HEADER_WRITE_BITS[key]/8;
	            memcpy(&value, field->field_value, len);
	            offset += 1 + len;
#endif
	            /* Finish decoding */
	            //printf("\t%s: 0x%llx,\n", HEADER_NAMES[key].c_str(), (u64)value);

	            // Apply change
//	            if (0) {
//                    p.apply_diff(key, value);
//                    if (field_nr == IP_ID) {
//                        saw_ip_diff = 1;
//                    }
//	            }

	            p_fast.apply_diff(key, value);
                if (field_nr == IP_ID) {
                    //printf("processing IP ID key\n");
                    saw_ip_diff = 1;
                }
	        }

	        // Assume increment by one for each flow.
	        if (!saw_ip_diff) {
	            //!p.ip.id++;
	            p_fast.inc_ip_id();
	        }

	        if (VERBOSE) {
	            printf("THIS PACKET (without mods applied, yet): \n");
                //p.print_hex();
	        }
	    }

	    //uint size = p.pack_buf(buf);

	    // With p_fast, no need to pack!  Stored in network format.
	    //if (VERBOSE)
	    //    printf("length of packed string: %i\n", size);
	    //packet_len = size;

        // Add this packet to recent_packets, as a future packet may
        // reference it.
        //!recent_packets.insert(make_pair(seq, p));
        //!recent_packets[seq].update();
        //printf("SIZE of inserted packet: %i", recent_packets[seq].size);
        recent_packets_fast.insert(make_pair(seq, p_fast));

        // Remove previous packet in this flow, which should never be
        // referenced again if the compressor is correctly implemented
        if (diff->num_changes != FIRST_PACKET_ENCODE) {
            //!recent_packets.erase(diff->packet_ref);
            recent_packets_fast.erase(diff->packet_ref);
        }

        //struct pcap_pkthdr hdr;
        hdr->ts = t;
        //hdr->caplen = p.caplen;
        //hdr->caplen = size;
        hdr->caplen = p_fast.hdr_size();
        //hdr->len = p.infer_len();
        hdr->len = p_fast.infer_len();
	    if (DUMP_READ_PCAP) {
	        pcap_dump((u_char *)out_dumper, hdr, p_fast.buff);
            //pcap_dump((u_char *)out_dumper, hdr, buf);
	    }
	}

	void loop_finish() {
	    loop_finish_time = time(NULL);
	    u32 delta_sec = loop_finish_time - loop_start_time;
	    if (delta_sec == 0)
	        delta_sec = 1;

	    pcap_read_bytes = gztell(fp_diff_comp);
	    float speed = pcap_read_bytes * 8.0 / delta_sec / 1e6;
	    printf("Parsed %llu bytes in %d seconds, speed %.3f Mb/s\n",
	           pcap_read_bytes, delta_sec, speed);

	    json["exec_time_sec"] = picojson::value((u64)delta_sec);
	}

	void stats_finish(JSON &json) {
	    printf("Found %llu flow keys in %llu packets\n", num_flows, pcap_num_packets);
	    printf("Unique IPs (source, dest): %llu\n", (u64)ip_count.size());
	    printf("    Unique IPs per packet: %.3f\n", ip_count.size() * 1.0 / pcap_num_packets);

	    print_proto_stats("Ethernet", proto_count, 1 << 16, ETHERTYPE_TO_STRING, json);
	    print_proto_stats("IP", ip_proto_count, 1 << 8, IPPROTO_TO_STRING, json);

	    json["pcap_bytes"] = picojson::value(pcap_read_bytes);
	    json["pcap_file"] = picojson::value(pcap_filename);
	    json["num_flows"] = picojson::value(num_flows);
	    json["num_packets"] = picojson::value(pcap_num_packets);
	    json["unique_ips"] = picojson::value((u64)ip_count.size());
	}

	void dump_json() {
	    string filename;

	    if (JSON_OUTPUT == NULL) {
	        filename = pcap_filename;
	        filename += "---stats.json";
	    } else {
	        filename = JSON_OUTPUT;
	    }

	    FILE *fp = fopen(filename.c_str(), "w");
	    if (!fp) {
	        printf("Counldn't open file %s\n", filename.c_str());
	        return;
	    }

	    printf("Dumping stats to %s\n", filename.c_str());
	    fprintf(fp, "%s", picojson::value(json).serialize().c_str());
	    fclose(fp);
	}

};

void parse_args(int argc, char *argv[]) {
	int i = 0;
	while (i < argc) {
		if (string(argv[i]) == "--verbose") {
			VERBOSE = true;
			i += 1;
			continue;
		}

        if (string(argv[i]) == "-D") {
            DUMP_READ_PCAP = true;
            i += 1;
            continue;
        }

        if (string(argv[i]) == "-P" or string(argv[i]) == "--packets") {
            MAX_PACKETS = atoi(argv[i+1]);
            i += 2;
            continue;
        }

        if (string(argv[i]) == "-s" or string(argv[i]) == "--skip-ethernet") {
            SKIP_ETHERNET = true;
            i += 1;
            continue;
        }

        if (string(argv[i]) == "-j" or string(argv[i]) == "--json") {
            JSON_OUTPUT = argv[i+1];
            i += 2;
            continue;
        }

		i++;
	}
}

void varint_test() {
	u8 encode[4] = {0};
	u32 num = 0x3ff;
	int len = varint_encode(num, encode);
	printf("Encoded to len %d %d %d %d %d\n", len, encode[0], encode[1], encode[2], encode[3]);
	printf("Decoded as %d\n", varint_decode(2, encode));
}

int main(int argc, char *argv[]) {
	// varint_test();
    pcap_filename = argv[1];
    Decompressor c(argv[1]);
	packet_init();
	parse_args(argc, argv);
	c.decompress();
	return 0;
}
