#!/bin/bash

set -e
set -o nounset

pcap=${1:-}
times=1
exptid=`date +%b%d--%H-%M-%S`
fast=~/ndb/ndb/analysis/fast/fast

if [ ! -f "$fast" ]; then
    fast=~/ndb/fast/fast
    if [ ! -f "$fast" ]; then
	echo couldnt find $fast compressor
	exit
    fi
fi

opts=" -i 10000000 "

if [ -z "$pcap" ]; then
    echo usage: $0 pcap-file
    exit
fi

function finish {
    echo "********************************"
    killall -9 fast gzip tar
    echo results in $exptid
    exit
}

trap finish SIGINT

function vj {
    (time $fast $pcap $opts -j vj.json) 2>&1
}

function vjgz {
    (time $fast $pcap $opts -d -j vjgz.json) 2>&1
}

function gz {
    (time tar czf /tmp/tmp.tar.gz $pcap;
    ls -l /tmp/tmp.tar.gz;
    rm -f /tmp/tmp.tar.gz;) 2>&1
}

function bz2 {
    (time tar cjf /tmp/tmp.tar.bz2 $pcap;
    ls -l /tmp/tmp.tar.bz2;
    rm -f /tmp/tmp.tar.bz2;) 2>&1
}

function lzma {
    (time tar -c --lzma -f /tmp/tmp.tar.lzma $pcap;
    ls -l /tmp/tmp.tar.lzma;
    rm -f /tmp/tmp.tar.lzma;) 2>&1
}

mkdir -p $exptid

pushd $exptid

echo $pcap | tee -a README
ls -l $pcap | tee -a README
echo start... `date` | tee -a README
echo $opts | tee -a README

for i in `seq 1 $times`; do
    echo $i $pcap
    mkdir $i
    pushd $i

    vj   | tee vj.log
    vjgz | tee vjgz.log
    gz   | tee gz.log

    # Expensive ones.
    #bz2   | tee bz2.log
    #lzma   | tee lzma.log

    popd
done

echo finish... `date` | tee -a README
popd

finish
