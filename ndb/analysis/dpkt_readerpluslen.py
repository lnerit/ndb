"""Modified dpkt.pcap.Reader object

The PCAP Reader object in dpkt does not return the actual length of the
captured packet, even though that information is stored in each entry in the
libpcap file.

While ideally we could just subclass the thing, change __iter__(), and call
it a day, that doesn't work because dpkt hides access to the file object
(self.__f) with double underscores.

Hence, the ugly code duplication here, with a single-line change to the yield
statement: return the length as part of the iterator tuple.

Alternately, one could modify self.__f in Reader() to be just self._f, which
would make it subclass'able, or add an option at class creation time to choose
the kind of iterator used.  Those would require keeping the dpkt version
though.
"""

from dpkt.pcap import *

class ReaderPlusLen(object):
    """Simple pypcap-compatible pcap file reader."""
    
    def __init__(self, fileobj):
        self.name = fileobj.name
        self.fd = fileobj.fileno()
        self.__f = fileobj
        buf = self.__f.read(FileHdr.__hdr_len__)
        self.__fh = FileHdr(buf)
        self.__ph = PktHdr
        if self.__fh.magic == PMUDPCT_MAGIC:
            self.__fh = LEFileHdr(buf)
            self.__ph = LEPktHdr
        elif self.__fh.magic != TCPDUMP_MAGIC:
            raise ValueError, 'invalid tcpdump header'
        if self.__fh.linktype in dltoff:
            self.dloff = dltoff[self.__fh.linktype]
        else:
            self.dloff = 0
        self.snaplen = self.__fh.snaplen
        self.filter = ''

    def fileno(self):
        return self.fd
    
    def datalink(self):
        return self.__fh.linktype
    
    def setfilter(self, value, optimize=1):
        return NotImplementedError

    def readpkts(self):
        return list(self)
    
    def dispatch(self, cnt, callback, *args):
        if cnt > 0:
            for i in range(cnt):
                ts, pkt = self.next()
                callback(ts, pkt, *args)
        else:
            for ts, pkt in self:
                callback(ts, pkt, *args)

    def loop(self, callback, *args):
        self.dispatch(0, callback, *args)
    
    def __iter__(self):
        self.__f.seek(FileHdr.__hdr_len__)
        while 1:
            buf = self.__f.read(PktHdr.__hdr_len__)
            if not buf: break
            hdr = self.__ph(buf)
            buf = self.__f.read(hdr.caplen)
            # NOTE: the mod is to the line below, to add hdr.len.
            yield (hdr.tv_sec + (hdr.tv_usec / 1000000.0), buf, hdr.len)