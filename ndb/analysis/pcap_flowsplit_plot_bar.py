#!/usr/bin/env python
"""
Program to print out compression values as a function of input size.

Each series is a different compression method.

Maybe compare the best methods for each trace size and source?
"""
import pickle
import os
from collections import OrderedDict

import matplotlib.pyplot as plt
from ndb.plot.plot_helper import colorGenerator
from plot_bar_graph import plot_data_bar

from ndb.analysis import plot_defaults
import compress

import numpy as np


PLOT_FOLDER = 'pcaps/plots'

CLEMSON_NAME = 'clemson'
CLEMSON_FOLDER = 'pcaps/' + CLEMSON_NAME
CLEMSON_SIZES = ['100K', '1M', '10M', '100M']
CLEMSON_FILENAME = 'core-capture.cap'
def CLEMSON_SIZE_TO_NAME(size):
    return '%s.%s.pcap' % (CLEMSON_FILENAME, size)
CLEMSON_FILES = [CLEMSON_SIZE_TO_NAME(s) for s in CLEMSON_SIZES]
CLEMSON_FILEPATHS = [os.path.join(CLEMSON_FOLDER, f) for f in CLEMSON_FILES]

STANFORD_NAME = 'vlan74'
STANFORD_FOLDER = 'pcaps/' + STANFORD_NAME
STANFORD_FILES = ['dump.2012-09-06-14.log.small']
STANFORD_FILEPATHS = [os.path.join(STANFORD_FOLDER, f) for f in STANFORD_FILES]

FILES = CLEMSON_FILES + STANFORD_FILES
FILEPATHS = CLEMSON_FILEPATHS + STANFORD_FILEPATHS

NO_LEGEND = False

STATS_DATA_EXT = 'stats'  # dict of stats key/val pairs

ALGORITHMS = ["known_len_bytes",
              "unpacked_hdrs", "packed_hdrs", "pcap_flowkeys",
              "unpacked_hdr_flowkeys", "packed_hdr_flowkeys",
              "used_packed_hdr_flowkeys"]
for fcn in compress.COMPRESSION_FCNS:
    ratio_print_name = "Original / PackedHdrs+%s" % fcn.__name__
    ratio_size_name = "packedhdrs+%s" % fcn.__name__ 
    ALGORITHMS.append(ratio_size_name)


GROUP_NAME_MAP = {}
for a in ALGORITHMS:
    GROUP_NAME_MAP[a] = a
SUBGROUP_NAME_MAP = {}

def simplify_name(name):
    name = name.replace('dump.2012-09-06-14.log' , 'Stanford VLAN74 ')
    name = name.replace('core-capture.cap.', 'Clemson ')
    name = name.replace('.pcap', "")
    return name

for i, s in enumerate(FILEPATHS):
    name = FILES[i]
    SUBGROUP_NAME_MAP[s] = simplify_name(name)



def ensure_path(filepath):
    """Check that path exists and return it."""
    if not os.path.exists(filepath):
        raise Exception("could not find file: %s" % filepath)
    return filepath


def read_stats_file(filepath):
    ensure_path(filepath)
    f = open(filepath, 'r')
    return pickle.load(f)


def invert_dict_of_dicts(data):
    """Flip primary and secondary key order within a dict
    
    data[x][y] = val -> data[y][x], for all x, y
    """    
    data_flipped = OrderedDict()
    for filepath, stats in data.iteritems():
        for metric_name, metric_val in stats.iteritems():
            if metric_name not in data_flipped:
                data_flipped[metric_name] = OrderedDict()
            data_flipped[metric_name][filepath] = metric_val
    return data_flipped

def main():
    # Save data to flip the order; need to add one series at a time.
    ratios = OrderedDict()  # data[file] = stats ({} of key/val stat pairs)
    bytes_per_pkt = OrderedDict()
    for filepath in FILEPATHS:
        print "Grabbing data in file: %s" % filepath
        stats = read_stats_file(filepath + '.' + STATS_DATA_EXT)

        orig_size = stats["known_len_bytes"]
        num_pkts = stats["known_len_pkts"]

        ratios[filepath] = OrderedDict()
        bytes_per_pkt[filepath] = OrderedDict()

        for a in ALGORITHMS:
            ratios[filepath][a] = float(orig_size) / stats[a]
            bytes_per_pkt[filepath][a] = float(stats[a]) / num_pkts

    # Flip data order; we want:
    # group to be algorithms
    # subgroup to be filename
    ratios = invert_dict_of_dicts(ratios)
    bytes_per_pkt = invert_dict_of_dicts(bytes_per_pkt)

    plot_dir = PLOT_FOLDER
    show_plot = False
    save_plot = True
    plot_ext = 'pdf'
    group_order = ALGORITHMS
    subgroup_order = FILEPATHS
    analysis_name = 'compression_ratios_bar'
    yscale = 'log'
    legend_title = 'filenames'
    print "Plotting graphs to %s" % plot_dir
    ylabel_fcn = lambda y_unit: 'compression ratio (%s)' % y_unit
    plot_data_bar(plot_dir, ratios, show_plot, save_plot, group_order,
                  subgroup_order, analysis_name, yscale = yscale,
                  legend_title = legend_title, ylabel_fcn = ylabel_fcn,
                  axis_left = 0.05, axis_right = 0.85, axis_bottom = 0.35,
                  group_name_map = GROUP_NAME_MAP,
                  subgroup_name_map = SUBGROUP_NAME_MAP)

    analysis_name = 'bytes_per_pkt_bar'
    ylabel_fcn = lambda y_unit: 'bytes per packet (%s)' % y_unit
    plot_data_bar(plot_dir, bytes_per_pkt, show_plot, save_plot, group_order,
                  subgroup_order, analysis_name, yscale = yscale,
                  legend_title = legend_title, ylabel_fcn = ylabel_fcn,
                  axis_left = 0.05, axis_right = 0.85, axis_bottom = 0.35,
                  group_name_map = GROUP_NAME_MAP,
                  subgroup_name_map = SUBGROUP_NAME_MAP,
                  ymin = 0.01, ymax = 1000)


if __name__ == '__main__':
    main()
