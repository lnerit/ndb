"""Postcard functions, put here to reduce dependencies."""
import dpkt

# If True, generate postcards with ingress inport set to -1.
NO_INGRESS_INPORT = True

def postcards_on_path(t, node_path, no_ingress_inport, pkt, sw_to_dpid):
    """Return the list of postcards on the path.

    t: Topo object
    node_path: path of dpids
    no_ingress_inport: if True, set in_port to -1.
    pkt: dpkt Packet object
    sw_to_dpid: map of sw names to dpids
    """
    path = []  # Each element is a postcard dict.
    for i, node in enumerate(node_path):
        # If first node, skip sending host
        if i == 0:
            pass
        # If middle node:
        elif i < len(node_path) - 1:
            this_in_port, prev_out_port = t.port(node, node_path[i - 1])
            this_out_port, next_in_port = t.port(node, node_path[i + 1])
            if no_ingress_inport:
                in_port = -1
            version = 1
            dpid = sw_to_dpid[node]
            postcard = {
                'pkt': pkt,
                'pkt_str': pkt.pack(),
                'dpid': dpid,
                'inport': this_in_port,
                'outport': this_out_port,
                'version': version,
                'tag': (version, this_out_port, dpid),
            }
            path.append(postcard)
        # If last node, skip receiving host
        else:
            pass
    return path


def clean_dstmac(pp):
    for pcard in pp:
        pkt = pcard['pkt']
        if isinstance(pkt, dpkt.ethernet.Ethernet):
            pkt.dst = '\x00\x00\x00\x00\x00\x00'
