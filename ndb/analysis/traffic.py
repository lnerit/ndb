"""
Functions to define traffic matrices for postcard analyses.
"""
import random

import dpkt
from dpkt.tcp import TCP
from dpkt.ip import IP, IP_PROTO_TCP
from dpkt.ethernet import Ethernet, ETH_TYPE_IP

from analysis_lib import autodict


def buildIPPacket(src, dst):
    """Create a TCP/IP packet.

    src: 4B string
    dst: 4B string

    returns completed dpkt.Packet object.
    """
    tcp = TCP()
    ip = IP(src=src, dst=dst, p=IP_PROTO_TCP, data=tcp)
    eth = Ethernet(src='\x00'*6, dst='\x00'*6, type=ETH_TYPE_IP, data=ip)
    return eth


class Flow(object):
    pass


class DpktFlow(Flow):
    """Represent a flow generated from a dpkt packet object."""

    def __init__(self, rate, pkt):
        """
        rate: unitless quantity
        pkt: dpkt pkt obj
        """
        self.rate = rate
        self.pkt = pkt


class IPFlow(Flow):
    """Represent a simple IP flow over unspecified Ethernet fields."""

    def __init__(self, rate, src, dst):
        """
        rate: unitless quantity
        src: 4B string
        dst: 4B string
        """
        self.rate = rate
        self.pkt = buildIPPacket(src, dst)


def all_to_all_tm(t, host_to_ip, traffic):
    """Return all-to-all traffic matrix for use in PostcardAnalysis.

    t: Topo object
    traffic: unitless integer
    """
    # tm[src][dst] = list of Flow objects.
    # Each Flow object describes a rate and headerspace of a flow.
    tm = autodict()
    traffic = 1
    for src in t.hosts():
        for dst in t.hosts():
            if src != dst:
                src_ip = host_to_ip[src]
                dst_ip = host_to_ip[dst]
                tm[src][dst] = [IPFlow(traffic, src_ip, dst_ip)]
    return tm


def random_tm(t, host_to_ip, traffic):
    """Return all-to-all traffic matrix for use in PostcardAnalysis.

    t: Topo object
    traffic: unitless integer
    """
    # tm[src][dst] = list of Flow objects.
    # Each Flow object describes a rate and headerspace of a flow.
    tm = autodict()
    traffic = 1
    hosts = t.hosts()
    for src in t.hosts():
        # Pick a random host. Send 'traffic' units there.
        other_hosts = [h for h in t.hosts() if h != src]
        dst = random.choice(other_hosts)
        src_ip = host_to_ip[src]
        dst_ip = host_to_ip[dst]
        tm[src][dst] = [IPFlow(traffic, src_ip, dst_ip)]
    return tm