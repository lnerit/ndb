# $Id: ethernet.py 65 2010-03-26 02:53:51Z dugsong $

"""ofp_match"""

"""
From openflow/include/openflow/openflow.h:

/* Fields to match against flows */
struct ofp_match {
    uint32_t wildcards;        /* Wildcard fields. */
    uint16_t in_port;          /* Input switch port. */
    uint8_t dl_src[OFP_ETH_ALEN]; /* Ethernet source address. */
    uint8_t dl_dst[OFP_ETH_ALEN]; /* Ethernet destination address. */
    uint16_t dl_vlan;          /* Input VLAN id. */
    uint8_t dl_vlan_pcp;       /* Input VLAN priority. */
    uint8_t pad1[1];           /* Align to 64-bits */
    uint16_t dl_type;          /* Ethernet frame type. */
    uint8_t nw_tos;            /* IP ToS (actually DSCP field, 6 bits). */
    uint8_t nw_proto;          /* IP protocol or lower 8 bits of
                                * ARP opcode. */
    uint8_t pad2[2];           /* Align to 64-bits */
    uint32_t nw_src;           /* IP source address. */
    uint32_t nw_dst;           /* IP destination address. */
    uint16_t tp_src;           /* TCP/UDP source port. */
    uint16_t tp_dst;           /* TCP/UDP destination port. */
};
OFP_ASSERT(sizeof(struct ofp_match) == 40);

/* Flow wildcards. */
enum ofp_flow_wildcards {
    OFPFW_IN_PORT  = 1 << 0,  /* Switch input port. */
    OFPFW_DL_VLAN  = 1 << 1,  /* VLAN id. */
    OFPFW_DL_SRC   = 1 << 2,  /* Ethernet source address. */
    OFPFW_DL_DST   = 1 << 3,  /* Ethernet destination address. */
    OFPFW_DL_TYPE  = 1 << 4,  /* Ethernet frame type. */
    OFPFW_NW_PROTO = 1 << 5,  /* IP protocol. */
    OFPFW_TP_SRC   = 1 << 6,  /* TCP/UDP source port. */
    OFPFW_TP_DST   = 1 << 7,  /* TCP/UDP destination port. */

    /* IP source address wildcard bit count.  0 is exact match, 1 ignores the
     * LSB, 2 ignores the 2 least-significant bits, ..., 32 and higher wildcard
     * the entire field.  This is the *opposite* of the usual convention where
     * e.g. /24 indicates that 8 bits (not 24 bits) are wildcarded. */
    OFPFW_NW_SRC_SHIFT = 8,
    OFPFW_NW_SRC_BITS = 6,
    OFPFW_NW_SRC_MASK = ((1 << OFPFW_NW_SRC_BITS) - 1) << OFPFW_NW_SRC_SHIFT,
    OFPFW_NW_SRC_ALL = 32 << OFPFW_NW_SRC_SHIFT,

    /* IP destination address wildcard bit count.  Same format as source. */
    OFPFW_NW_DST_SHIFT = 14,
    OFPFW_NW_DST_BITS = 6,
    OFPFW_NW_DST_MASK = ((1 << OFPFW_NW_DST_BITS) - 1) << OFPFW_NW_DST_SHIFT,
    OFPFW_NW_DST_ALL = 32 << OFPFW_NW_DST_SHIFT,

    OFPFW_DL_VLAN_PCP = 1 << 20,  /* VLAN priority. */
    OFPFW_NW_TOS = 1 << 21,  /* IP ToS (DSCP field, 6 bits). */

    /* Wildcard all fields. */
    OFPFW_ALL = ((1 << 22) - 1)
};
"""

import struct
import dpkt

OFPM_HDR_LEN    = 40

OFPM_LEN_MIN    = 40
OFPM_LEN_MAX    = 40


VLAN_UNKNOWN = 65535

OFPFW_IN_PORT  = 1 << 0  #/* Switch input port. */
OFPFW_DL_VLAN  = 1 << 1
  #/* VLAN id. */
OFPFW_DL_SRC   = 1 << 2  #/* Ethernet source address. */
OFPFW_DL_DST   = 1 << 3  #/* Ethernet destination address. */
OFPFW_DL_TYPE  = 1 << 4  #/* Ethernet frame type. */
OFPFW_NW_PROTO = 1 << 5  #/* IP protocol. */
OFPFW_TP_SRC   = 1 << 6  #/* TCP/UDP source port. */
OFPFW_TP_DST   = 1 << 7  #/* TCP/UDP destination port. */

"""
/* IP source address wildcard bit count.  0 is exact match, 1 ignores the
 * LSB, 2 ignores the 2 least-significant bits, ..., 32 and higher wildcard
 * the entire field.  This is the *opposite* of the usual convention where
 * e.g. /24 indicates that 8 bits (not 24 bits) are wildcarded. */
"""
OFPFW_NW_SRC_SHIFT = 8
OFPFW_NW_SRC_BITS = 6
OFPFW_NW_SRC_MASK = ((1 << OFPFW_NW_SRC_BITS) - 1) << OFPFW_NW_SRC_SHIFT
OFPFW_NW_SRC_ALL = 32 << OFPFW_NW_SRC_SHIFT

"""
/* IP destination address wildcard bit count.  Same format as source. */
"""
OFPFW_NW_DST_SHIFT = 14
OFPFW_NW_DST_BITS = 6
OFPFW_NW_DST_MASK = ((1 << OFPFW_NW_DST_BITS) - 1) << OFPFW_NW_DST_SHIFT
OFPFW_NW_DST_ALL = 32 << OFPFW_NW_DST_SHIFT

OFPFW_DL_VLAN_PCP = 1 << 20  #/* VLAN priority. */
OFPFW_NW_TOS = 1 << 21  #/* IP ToS (DSCP field, 6 bits). */

#/* Wildcard all fields. */
OFPFW_ALL = ((1 << 22) - 1)


class OFPMatch(dpkt.Packet):
    __hdr__ = (
        ('wildcards', 'I', 1),  # wildcard in_port by default, to match pox
        ('in_port', 'H', 0),
        ('dl_src', '6s', ''),
        ('dl_dst', '6s', ''),
        ('dl_vlan', 'H', VLAN_UNKNOWN),
        ('dl_vlan_pcp', 'B', 0),
        ('pad1', 's', ''),
        ('dl_type', 'H', 0),
        ('nw_tos', 'B', 0),
        ('nw_proto', 'B', 0),
        ('pad2', '2s', ''),        
        ('nw_src', '4s', ''),
        ('nw_dst', '4s', ''),
        ('tp_src', 'H', 0),
        ('tp_dst', 'H', 0)
        )

def ofpm_from_packet(eth):
    match = OFPMatch()
    match.wildcards = OFPFW_ALL
    match.dl_src = eth.src
    match.dl_dst = eth.dst
    match.dl_type = eth.type
    # Assume no VLAN wildcarding/PCP bit, also that this is an ethernet packet.
    match.wildcards = match.wildcards & ~OFPFW_DL_SRC & ~OFPFW_DL_DST & ~OFPFW_DL_VLAN & ~OFPFW_DL_TYPE & ~OFPFW_DL_VLAN_PCP
    if eth.type == 0x8100:  # VLAN
        raise UnimplementedError("missing VLAN code")
    elif eth.type == 0x0806:  # ARP
        arp = eth.data
        match.wildcards = match.wildcards & ~OFPFW_NW_SRC_MASK & ~OFPFW_NW_DST_MASK
        match.nw_src = arp.spa
        match.nw_dst = arp.dpa
    elif eth.type == 0x0800:
        ip = eth.data
        match.wildcards = match.wildcards & ~OFPFW_NW_SRC_MASK & ~OFPFW_NW_DST_MASK & ~OFPFW_NW_TOS & ~OFPFW_NW_PROTO
        match.nw_src = ip.src
        match.nw_dst = ip.dst
        match.nw_tos = ip.tos
        match.nw_proto = ip.p
        # See OF spec v1.0, p9:
        # "For IP packets with nonzero fragment offset or More Fragments bit 
        # set, the transport ports are set to zero for the lookup."
        fragment = ((ip.off & (dpkt.ip.IP_MF | dpkt.ip.IP_OFFMASK)) != 0)
        if not fragment:
            if ip.p == 6:
                tcp = ip.data
                match.wildcards = match.wildcards & ~OFPFW_TP_SRC & ~OFPFW_TP_DST
                match.tp_src = tcp.sport
                match.tp_dst = tcp.dport
            elif ip.p == 17:
                udp = ip.data
                match.wildcards = match.wildcards & ~OFPFW_TP_SRC & ~OFPFW_TP_DST
                match.tp_src = udp.sport
                match.tp_dst = udp.dport
            elif ip.p == 1:
                icmp = ip.data
                match.wildcards = match.wildcards & ~OFPFW_TP_SRC & ~OFPFW_TP_DST
                match.tp_src = icmp.type
                match.tp_dst = icmp.code
        else:
            match.wildcards = match.wildcards & ~OFPFW_TP_SRC & ~OFPFW_TP_DST
            match.tp_src = 0
            match.tp_dst = 0
            

    return match