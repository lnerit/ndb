#!/usr/bin/env python
import sys
from collections import defaultdict 
import dpkt

if len(sys.argv) == 1:
    filename = 'pcaps/http/http.cap'
elif len(sys.argv) == 2:
    filename = sys.argv[1]
else:
    raise Exception("usage: %s [filename]" % sys.argv[0])

f = open(filename)
pcap = dpkt.pcap.Reader(f)

PRINT_INTERVAL = 10000

valid = 0
pkts = 0
errors = defaultdict(int)
for ts, buf in pcap:
    try:
        eth = dpkt.ethernet.Ethernet(buf)
        valid += 1
    except ValueError as ve:
        errors['ValueError'] += 1
    except IndexError as ie:
        errors['IndexError'] += 1

    pkts += 1
    if pkts % PRINT_INTERVAL == 0:
        print '.',
        # Force screen output; otherwise, no output.
        sys.stdout.flush()

if pkts >= PRINT_INTERVAL:
    print

print "total pkts: %s" % pkts
print "valid pkts: %s" % valid
for err, freq in errors.iteritems():
    print "%s pkts: %s" % (err, freq)
f.close()