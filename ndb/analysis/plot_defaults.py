import matplotlib as plt

def apply(axis_left = 0.04,
          axis_right = 0.95,
          axis_bottom = 0.5,  # was 0.45 for benchmark
          axis_top = 0.97):
    # http://stackoverflow.com/questions/3899980/how-to-change-the-font-size-on-a-matplotlib-plot
    # Reduce font size to not overlap
    #plt.rc('xtick', labelsize=12)
    # TODO: figure out why this only seems to work on PDF & screen output, but
    # not with png output.
    plt.rcParams.update({'font.size': 12})  # was 14 for benchmark
    
    axis_width = axis_right - axis_left
    axis_height = axis_top - axis_bottom
    # add_axes takes [left, bottom, width, height]
    DEF_AXES = [axis_left, axis_bottom, axis_width, axis_height]
    COLOR_LIGHTGRAY = '#cccccc'
    
    plt.rcParams['figure.subplot.top'] = axis_top
    plt.rcParams['figure.subplot.bottom'] = axis_bottom
    plt.rcParams['figure.subplot.left'] = axis_left
    plt.rcParams['figure.subplot.right'] = axis_right
    plt.rcParams['lines.linewidth'] = 2
    plt.rcParams['grid.color'] = COLOR_LIGHTGRAY
    plt.rcParams['grid.linewidth'] = 0.6
    
    # Generate figures with Type 1 fonts for camera-ready copies:
    # from http://nerdjusttyped.blogspot.com/2010/07/type-1-fonts-and-matplotlib-figures.html
    plt.rcParams['ps.useafm'] = True
    plt.rcParams['pdf.use14corefonts'] = True
    #rcParams['text.usetex'] = True


def apply_cdf_plot_defaults():
    apply_plot_defaults()


def apply_time_series_plot_defaults_with_legend():
    apply_time_series_plot_defaults()

    DEF_AXIS_LEFT = 0.15
    DEF_AXIS_RIGHT = 0.75
    DEF_AXIS_BOTTOM = 0.15  # was 0.45 for benchmark
    DEF_AXIS_TOP = 0.95
    DEF_AXIS_WIDTH = DEF_AXIS_RIGHT - DEF_AXIS_LEFT
    DEF_AXIS_HEIGHT = DEF_AXIS_TOP - DEF_AXIS_BOTTOM
    # add_axes takes [left, bottom, width, height]
    DEF_AXES = [DEF_AXIS_LEFT, DEF_AXIS_BOTTOM, DEF_AXIS_WIDTH, DEF_AXIS_HEIGHT]

    plt.rcParams['figure.subplot.top'] = DEF_AXIS_TOP
    plt.rcParams['figure.subplot.bottom'] = DEF_AXIS_BOTTOM
    plt.rcParams['figure.subplot.left'] = DEF_AXIS_LEFT
    plt.rcParams['figure.subplot.right'] = DEF_AXIS_RIGHT


def apply_time_series_plot_defaults():
    apply_plot_defaults()


def apply_plot_defaults():
    # http://stackoverflow.com/questions/3899980/how-to-change-the-font-size-on-a-matplotlib-plot
    # Reduce font size to not overlap
    #plt.rc('xtick', labelsize=12)
    # TODO: figure out why this only seems to work on PDF & screen output, but
    # not with png output.
    plt.rcParams.update({'font.size': 12})  # was 14 for benchmark
    
    DEF_AXIS_LEFT = 0.15
    DEF_AXIS_RIGHT = 0.95
    DEF_AXIS_BOTTOM = 0.15  # was 0.45 for benchmark
    DEF_AXIS_TOP = 0.95
    DEF_AXIS_WIDTH = DEF_AXIS_RIGHT - DEF_AXIS_LEFT
    DEF_AXIS_HEIGHT = DEF_AXIS_TOP - DEF_AXIS_BOTTOM
    # add_axes takes [left, bottom, width, height]
    DEF_AXES = [DEF_AXIS_LEFT, DEF_AXIS_BOTTOM, DEF_AXIS_WIDTH, DEF_AXIS_HEIGHT]
    COLOR_LIGHTGRAY = '#cccccc'

    plt.rcParams['figure.subplot.top'] = DEF_AXIS_TOP
    plt.rcParams['figure.subplot.bottom'] = DEF_AXIS_BOTTOM
    plt.rcParams['figure.subplot.left'] = DEF_AXIS_LEFT
    plt.rcParams['figure.subplot.right'] = DEF_AXIS_RIGHT
    plt.rcParams['lines.linewidth'] = 2
    plt.rcParams['grid.color'] = COLOR_LIGHTGRAY
    plt.rcParams['grid.linewidth'] = 0.6

    # Generate figures with Type 1 fonts for camera-ready copies:
    # from http://nerdjusttyped.blogspot.com/2010/07/type-1-fonts-and-matplotlib-figures.html
    plt.rcParams['ps.useafm'] = True
    plt.rcParams['pdf.use14corefonts'] = True
    #rcParams['text.usetex'] = True

