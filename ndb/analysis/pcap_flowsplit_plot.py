#!/usr/bin/env python
"""
Program to print out compression values as a function of input size.

Each series is a different compression method.

Maybe compare the best methods for each trace size and source?
"""
import pickle
import os
from collections import OrderedDict

import matplotlib.pyplot as plt
from ndb.plot.plot_helper import colorGenerator

from ndb.analysis import plot_defaults
import compress


FOLDER = 'pcaps/clemson/'
SIZES = ['100K', '1M', '10M', '100M']
SIZES = ['100K', '1M', '10M', '100M']
FILENAME = 'core-capture.cap'
def SIZE_TO_NAME(size):
    return '%s.%s.pcap' % (FILENAME, size)
FILES = [SIZE_TO_NAME(s) for s in SIZES]

DPI = 300
NO_LEGEND = False

STATS_DATA_EXT = 'stats'  # dict of stats key/val pairs

METRICS = ["known_len_bytes",
           "unpacked_hdrs", "packed_hdrs", "pcap_flowkeys",
           "unpacked_hdr_flowkeys", "packed_hdr_flowkeys",
           "used_packed_hdr_flowkeys"]
for fcn in compress.COMPRESSION_FCNS:
    ratio_print_name = "Original / PackedHdrs+%s" % fcn.__name__
    ratio_size_name = "packedhdrs+%s" % fcn.__name__ 
    METRICS.append(ratio_size_name)


def ensure_path(filepath):
    """Check that path exists and return it."""
    if not os.path.exists(filepath):
        raise Exception("could not find file: %s" % filepath)
    return filepath


def read_stats_file(filepath):
    ensure_path(filepath)
    f = open(filepath, 'r')
    return pickle.load(f)


def plotCustomTimeSeries(data, title, xlabel, ylabel, step, xscale = 'linear',
                   yscale = 'linear', legend_loc = 'best'):
    """Plot a time series.

    Input: data, a list of dicts.
    Each inner-most dict includes the following fields:
        x: array
        y: array
        label: string

    step: if True, display data in stepped form.
    xscale: linear or log
    """
    fig = plt.figure()
    ax = plt.subplot(111)
    cgen = colorGenerator()
    colors = {}

    fig.canvas.set_window_title( title )

    for d in data:
        x = d['x']
        y = d['y']
        opts = {}
        if 'opts' in d:
            opts = d['opts']
        if step:
            x, y = convertToStep(x, y)
        ax.plot( x, y, label=d['label'], color=cgen.next(), **opts)

    plt.xlabel( xlabel, fontsize=20 )
    plt.ylabel( ylabel, fontsize=20 )
    plt.xscale( xscale )
    plt.yscale( yscale )
    plt.grid( True )
    #plt.legend(loc = legend_loc)
    for tick in ax.xaxis.get_major_ticks():
        tick.label1.set_fontsize(18)
    for tick in ax.yaxis.get_major_ticks():
        tick.label1.set_fontsize(18)

    # MPL: push legend off to the side.
    # from http://stackoverflow.com/questions/4700614/how-to-put-the-legend-out-of-the-plot
    # Shrink current axis by 10%
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.6, box.height])

    # Put a legend to the right of the current axis
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

    return fig


def plot_data_line(data, show_plot, save_plot, plot_dir, plot_ext, analysis_name):
    plot_defaults.apply_time_series_plot_defaults_with_legend()

    plot_data_line_series = []  # see plot_helper for format.

    #print "data: %s" % data

    orig_sizes = data.keys()
    for m in METRICS:
        xvals = []
        yvals = []
        for orig_size in orig_sizes:
            xvals.append(orig_size)
            yvals.append(data[orig_size][m])
        plot_data_line_series.append({
            'label': m,
            'x': xvals,
            'y': yvals,
            'opts': {'marker': 'o'},
        })

    #print "plot_data_line_series:%s" % plot_data_line_series

    fig = plotCustomTimeSeries(data = reversed(plot_data_line_series),
        title = '%s' % FILENAME,
        xlabel = 'Size of full capture input (bytes)',
        ylabel = 'Compression ratio',
        step = False,
        xscale = 'log',
        yscale = 'log',
        legend_loc = 'right')

    if NO_LEGEND:
        lgd = plt.legend()
        lgd.set_visible(False)

    if save_plot:
        if not os.path.exists(plot_dir):
            os.makedirs(plot_dir)
        filepath = os.path.join(plot_dir, analysis_name + '.' + plot_ext)
        print "Writing file to %s" % filepath
        fig.savefig(filepath, dpi = DPI)
    if show_plot:
        plt.show()

    plt.close(fig)


def main():
    # Save data to flip the order; need to add one series at a time.
    data = OrderedDict()  # data[size][metric] = ratio
    for size in SIZES:         
        filename = SIZE_TO_NAME(size)
        print "Running analysis on file: %s" % filename
        filepath = os.path.join(FOLDER, filename)
        stats = read_stats_file(filepath + '.' + STATS_DATA_EXT)

        orig_size = stats["known_len_bytes"]
        data[orig_size] = OrderedDict()
        for m in METRICS:
            data[orig_size][m] = float(orig_size) / stats[m]

    plot_dir = os.path.join(FOLDER, 'plots')
    show_plot = False
    save_plot = True
    plot_ext = 'pdf'
    analysis_name = 'compression_ratios_line'
    plot_data_line(data, show_plot, save_plot, plot_dir, plot_ext, analysis_name)


if __name__ == '__main__':
    main()