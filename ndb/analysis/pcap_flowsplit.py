#!/usr/bin/env python
"""
Program to aggregate the individual pkts in a pcap file into flows.

Input: pcap file

Outputs: 
.flowkeys: pcap file, but with truncated flow keys, not full, repeated packets.
.flowkeys_orig: pcap file of original pkts buf, one per flow.
.flows: pickled lookup_table object w/ total for each flow.

Some terminology for stats output below:
- An "unpacked header" describes all the data in the parsed headers of a
packet, as parsed by some parsing tree, such as the OFv1.0 flow forwarding
diagram.
- A "packed header" consists only of the fields relevant to parsing the packet,
so it should be quite a bit smaller.  In NDB terminology, a packed header
only includes bytes in mutable fields.

"""
import os
import sys
import copy
import pickle
from collections import OrderedDict
import time

import dpkt
import dpkt.ethernet
import dpkt.ip

import pox.openflow.libopenflow_01 as libof
import pox.lib.packet.ethernet as ethernet

from ndb.analysis.ofp_match_dpkt import OFPMatch
import ndb.analysis.compress as compress
from flowify_ofp_match import *
from packetlength import packet_len, is_8023
from dpkt_readerpluslen import ReaderPlusLen
from ofp_match_dpkt import ofpm_from_packet


# Store only counters, and not full packet objects, in the big hash table?
# Should speed up processing.
COUNTERS_ONLY = True

def add_pkt(lookup_table, key, buf, flow_dpkt, orig_flowkeys_pcap, ofpmd_file, packed_match):

    if COUNTERS_ONLY:
        if key not in lookup_table:
            lookup_table[key] = [1, buf]
            if WRITE_FLOWKEYS_ORIG:
                orig_flowkeys_pcap.writepkt(buf)
            if DO_PACKED_MATCH:
                ofpmd_file.write(packed_match)
        else:
            lookup_table[key][0] += 1
    else:
        # Add packet to the lookup table & update stats in place.
        if key not in lookup_table:
            lookup_table[key] = [1, buf, flow_dpkt]
            if WRITE_FLOWKEYS_ORIG:
                orig_flowkeys_pcap.writepkt(buf)
            ofpmd_file.write(packed_match)
        else:
            prior_pkts, prior_buf, prior_flow_dpkt = lookup_table[key]
            # Assertion: same flow key -> same zero'ed flow object.
            if str(prior_flow_dpkt) != str(flow_dpkt):
                print "ERROR: prior_flow_dpkt != flow_dpkt"
                print "prior_flow_dpkt, flow_dpkt"
                print "\t %s" % repr(prior_flow_dpkt)
                print "\t %s" % repr(flow_dpkt)
                exit()
            lookup_table[key][0] += 1

def add_pkt_flow(lookup_table, key, buf, flow_dpkt, packed_match, length):
    if key not in lookup_table:
        lookup_table[key] = [ [], flow_dpkt, [] ]
    lookup_table[key][0].append(buf)
    lookup_table[key][2].append(length)
    if str(flow_dpkt) != str(lookup_table[key][1]):
        a = str(flow_dpkt)
        b = str(lookup_table[key][1])
        print "WARN: flow %r different from first packet of this flow %r" % (a, b)

def picklable(lookup_table):
    ret = {}
    for k in lookup_table.keys():
        # Just the list of packets and their lengths
        ret[k] = [ lookup_table[k][0], lookup_table[k][2] ]
    return ret


ARP_LEN = 28

PRINT_INTERVAL = 1e4
VERBOSE = False
SEP = "%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
SHOW_ORIG = True
WRITE_FLOWKEYS = True
# If true, write out a separate file with the original packets corresponding
# to the (truncated, not necessarily valid) flowkeys.
WRITE_FLOWKEYS_ORIG = True
WRITE_FLOWS = True
# These get to be pretty large sometimes 1/3 the size of the original input.
WRITE_FLOW_PACKETS = False
WRITE_STATS = True
SHOW_STATS = True

# IPv6 handling is not in the OpenFlow v1.0 spec, but in the Clemson traces
# these packets do pop up frequently.  To deviate from the v1.0 flow def'n but
# improve the accuracy of tracking data bytes, set this to True to use true
# IPv6 packet lengths (rather than ignoring these).
# TODO: add flowify_ipv6 processing, so that accounted-for bytes better
# correspond to flow key bytes (because right now, all IPv6 flow are accounted
# for as a single flow with the IPv6 Ethertype.
HANDLE_IPV6_LENGTHS = True

# If a packet number is provided, this is the Nth packet parsed, 1-indexed
# to match wireshark.
DEBUG_PACKET = None

UNIMPLEMENTED = '?'


# Stores all data that may be relevant later for printing or graphing
# (but not necessarily directly).  Use a structure here to define expectations
# for what would be in a file, if this stuff is printed out.
# Format is: name: [stat_type, init_val, desc]
#   name: name (string)
#     stat_type: ['pkts',  'bytes', 'flows']
#     init_val: initial value
#     desc: description (string)
# There's no real need to make this an OrderedDict; it just ensures a
# consistent order when pickled and then read back in.
# TODO: define an object to represent a raw or computed stat and remove any
# code instances that depend on the internal ordering of the fields in a
# stats entry.
RAW_STATS = OrderedDict([
    # Dpkt-parsing stats
    ('total_pkts', ['pkts', 0, 'Num packets processed']),
    ('drops', ['pkts', 0, 'Num drops due to parsing errors']),

    # Total bytes used to store Ethernet packet data in pcap (NOT timestamps).
    ('pcap_pkt_bytes', ['bytes', 0, 'Bytes used by PCAP file']),

    # Length-parsing stats
    # Total bytes in packets with known length.
    # If packet is not IPv4 (or other stuff), then we don't
    # know the true length; could be > 64B or less. Track these totals because
    # they are important error bars.
    ('known_len_bytes', ['bytes', 0, 'Known-len pkt bytes']),
    ('known_len_pkts', ['pkts', 0, 'Known-len pkts']),
    ('unknown_len_pkts', ['pkts', 0, 'Unknown-len pkts']),
    ('min_pkt_len', ['pkts', None, 'Min pkt len']),
    ('max_pkt_len', ['pkts', None, 'Max pkt len']),

    # Flow-parsing stats
    # Total bytes to store for flow reconstruction.  Ignores padding bytes for
    # small Ethernet packets and anything past the flow key (e.g., L7 hdrs).
    ('flowkey_size_all', ['bytes', 0, 'Bytes used in flow key storage (inc. dupes)']),
    ('flowkey_size_unique', ['bytes', 0, 'Bytes used in flow key storage (no dupes)']),
    ('flows', ['flows', 0, 'Number of flows']),
    ('flow_totals', ['array', 0, 'Pkts marked for each flow']),

    ('len_mismatch_pkts', ['pkts', 0, 'Unexplained mislabeled-length pkts'])
])


# Map from format string types to values.
STATS_FMT_STRS = {
    'pkts': '%i',
    'bytes': '%i',
    'flows': '%i',
    'ratio': '%0.2f',
    'percent': '%0.2f%%',
    'array': '%s',
}


def init_stats():
    stats = {}
    for name, values in RAW_STATS.iteritems():
        stat_type, init_val, desc = values
        stats[name] = init_val
    return stats

INDENT = "\t"

def raw_stat_str(stats, stat_name, pre = INDENT):
    stat_type, init_val, desc = RAW_STATS[stat_name]
    fmt_str = STATS_FMT_STRS[stat_type]
    stat_val = stats[stat_name] 
    if stats[stat_name] is None:
        return ("%s%s: " + "%s") % (pre, desc, UNIMPLEMENTED)
    else:
        return ("%s%s: " + fmt_str) % (pre, desc, stats[stat_name])

def computed_stat_str(stats, stat_name, pre = INDENT):
    stat_type, desc, fcn = COMPUTED_STATS[stat_name]
    fmt_str = STATS_FMT_STRS[stat_type]
    return ("%s%s: " + fmt_str) % (pre, desc, stats[stat_name])

def print_stats(stats, stat_names):
    """Print stats, regardless of raw or computed."""
    for stat_name in stat_names:
        if stat_name in RAW_STATS:
            print raw_stat_str(stats, stat_name)
        else:
            print computed_stat_str(stats, stat_name)

def update_stat(stats, stat_name):
    """If stat is computed, compute it."""
    if stat_name not in stats:
        if stat_name not in COMPUTED_STATS:
            raise Exception("attempted to update misisng non-computed stat: %s" % stat_name)
        stat_type, desc, fcn = COMPUTED_STATS[stat_name]
        stats[stat_name] = fcn(stats)

def print_ratios(stats, stat_names, pre = INDENT):
    for stat_name in stat_names:
        if stat_name in RAW_STATS:
            stat_type, init_val, desc = RAW_STATS[stat_name]
        else:
            stat_type, desc, fcn = COMPUTED_STATS[stat_name]
        fmt_str = STATS_FMT_STRS['ratio']
        ratio = float(stats['known_len_bytes']) / stats[stat_name]
        print ("%sOriginal / %s: " + fmt_str) % (pre, desc, ratio)

def get_unaccounted_for_B_err(s):
    unaccounted_for_B_err = (1.0 -
                             (float(s['known_len_bytes'] + s['unknown_len_bytes_min']) /
                              (s['known_len_bytes'] + s['unknown_len_bytes_max'])))
    unaccounted_for_B_err_percent = 100.0 * unaccounted_for_B_err
    return unaccounted_for_B_err_percent

# Stores all data that may be relevant later for printing or graphing
# (but not necessarily directly).  Use a structure here to define expectations
# for what would be in a file, if this stuff is printed out.
# Format is: name: [stat_type, desc, fcn]
#   name: name (string)
#     stat_type: ['pkts',  'bytes', 'flows', 'ratio']
#     desc: description (string)
#     fcn: function to compute answer given raw_stats
COMPUTED_STATS = OrderedDict([
    # Dpkt-parsing stats
    ('total_pkts', ['pkts', 0, 'Num packets processed']),
    ('avg_pcap_entry_size_bytes',
        ['ratio',
         'Average PCAP entry size (bytes)',
         lambda s: float(s['pcap_pkt_bytes']) / s['total_pkts']
        ]),
    ('unknown_len_bytes_max',
        ['bytes',
         'Bytes max (unaccounted)',
         lambda s: s['unknown_len_pkts'] * 1514
        ]),
    ('unknown_len_bytes_min',
        ['bytes',
         'Bytes min (unaccounted)',
         lambda s: s['unknown_len_pkts'] * 14
        ]),
    ('unaccounted_for_B_err_percent',
        ['percent',
         'Unaccounted-for bytes worst-case error',
         get_unaccounted_for_B_err]),
    ('avg_pkt_size',
        ['ratio',
         'Average pkt size',
         lambda s: float(s['known_len_bytes']) / s['known_len_pkts']]),
    ('pkts_in_flow_table',
        ['pkts',
         'Number of packets in tracked flows',
         lambda s: sum(s['flow_totals'])]),
    ('pkts_in_largest_flow',
        ['pkts',
         'Number of packets in largest flow (most pkts)',
         lambda s: max(s['flow_totals'])]),
    ('pkts_in_smallest_flow',
        ['pkts',
         'Number of packets in smallest flow (least pkts)',
         lambda s: min(s['flow_totals'])]),
    ('avg_pkts_per_flow',
        ['ratio',
         'Average number of packets per flow',
         lambda s: float(sum(s['flow_totals'])) / s['flows']]),
    ('unpacked_hdrs',
        ['bytes',
         'UnpackedHdrs',
         lambda s: s['flowkey_size_all']]),
    ('packed_hdrs',
        ['bytes',
         'PackedHdrs',
         lambda s: OFP_MATCH_LEN * s['known_len_pkts']]),
    ('pcap_flowkeys',
        ['bytes',
         'PCAPFlowKeys',
         lambda s: float(s['avg_pcap_entry_size_bytes'] * s['flows'])]),
    ('unpacked_hdr_flowkeys',
        ['bytes',
         'UnpackedHdrFlowKeys',
         lambda s: s['flowkey_size_unique']]),
    ('packed_hdr_flowkeys',
        ['bytes',
         'PackedHdrFlowKeys',
         lambda s: s['flows'] * OFP_MATCH_LEN]),
    # If every packet has the same (unused) value for a field, like VLAN,
    # then don't bother storing it.  Also, ignore things that could be
    # compressed away, like bytes.
    # TODO: generalize to arbitrary input sequence.
    ('used_packed_hdr_flowkeys',
        ['bytes',
         'UsedPackedHdrFlowKeys',
         lambda s: s['flows'] * OFP_MATCH_USED_B]),
])

def print_report(stats):
    """Print a report after the completion of a packet analysis run.

    stats: dict containing stuff defined in ANALYSIS_STATS
    """
    print "Raw Parsing Stats:"
    print_stats(stats, ["total_pkts", "drops", "avg_pcap_entry_size_bytes"])

    print "Packet Size Stats:"
    print_stats(stats, ["known_len_pkts", "unknown_len_pkts",
                 "min_pkt_len", "max_pkt_len",
                 "unknown_len_bytes_max", "unknown_len_bytes_min",
                 "unaccounted_for_B_err_percent",
                 "avg_pkt_size",
                 "len_mismatch_pkts"
                 ])

    print "Flow Stats (OFv1.0 flows):"
    print_stats(stats, ["flows", "pkts_in_flow_table",
                        "pkts_in_largest_flow", "pkts_in_smallest_flow",
                        "avg_pkts_per_flow"])

    print "Byte Stats:"
    print_stats(stats, ["known_len_bytes", "pcap_pkt_bytes", "flowkey_size_all"])

# BDH: I commented these out for now, because the comparisons are not quite
# fair.  The PCAPs include timestamp data which should be ignored to start,
# then added back once the other compression methods below include it.
# They're interesting, but misleading.
#
#    print "Byte Compression Stats (general, not flow-def'n-specific):"
#    original_pcap_ratio = float(known_len_bytes) / pcap_pkt_bytes
#    print "\tOriginal / PCAP: %0.2f" % original_pcap_ratio
#    for ext in ['tgz', 'bz2', 'lzma']:
#        filepath = filename_in + '.' + ext
#        if os.path.exists(filepath):
#            compressed_size = os.path.getsize(filepath)
#            ratio = float(known_len_bytes) / float(compressed_size)
#            print "\tOriginal / %s: %0.2f" % (ext, ratio)
#        else:
#            print "\tOriginal / %s: %s" % (ext, UNIMPLEMENTED)

    print "Byte Compression Stats (lossless):"
    print INDENT + "VIMAL, PLEASE ADD HERE"

    print "Byte Compression Stats (lossy, OFv1.0 flows):"
    print_ratios(stats, ["unpacked_hdrs", "packed_hdrs", "pcap_flowkeys",
                         "unpacked_hdr_flowkeys", "packed_hdr_flowkeys",
                         "used_packed_hdr_flowkeys"])

    if DO_PACKED_MATCH:
        for fcn in compress.COMPRESSION_FCNS:
            ratio_print_name = "Original / PackedHdrs+%s" % fcn.__name__
            ratio_size_name = "packedhdrs+%s" % fcn.__name__
            if stats[ratio_size_name] == UNIMPLEMENTED:
                ratio_val_str = UNIMPLEMENTED
            else:
                ratio_val = float(stats['known_len_bytes']) / stats[ratio_size_name]
                ratio_val_str = "%0.2f" % ratio_val
            print "\t%s: %s" % (ratio_print_name, ratio_val_str)


def note_pkt_len(stats, pkt_len):
    """Adjust min & mak pkt lens; note the side effect to stats."""
    if stats["min_pkt_len"] is None or pkt_len < stats["min_pkt_len"]:
        stats["min_pkt_len"] = pkt_len
    if stats["max_pkt_len"] is None or pkt_len > stats["max_pkt_len"]:
        stats["max_pkt_len"] = pkt_len


def pkt_len_mismatch(pkt_len, orig_len):
    """Return True if the protocol-ID'ed (e.g. IP) pkt len doesn't match that
    report by the PCAP capture file (in orig_len), and we don't have a
    reasonable explanation for why they differ.
    """
    if pkt_len == orig_len:
        return False
    # Ignore 64B or below.  If the packet is shorter than
    # the min pkt size for ethernet, then it will have extra padding
    # and hence the protocol-IDed length will be less than this.
    elif orig_len <= 64:
        return False
    # Ignore cases where an OS increases pkt size by one to
    # have an even number of byte (presumably better aligned).
    elif ((pkt_len % 2 == 1) and
          (orig_len == (pkt_len + 1))):
        return False
    else:
        return True


def update_status(f, prev_read, prev_timestamp, pcap_filesize, total_pkts):
    if (total_pkts % PRINT_INTERVAL) == 0:
        #print stats["total_pkts"]
        curr_ptr = f.tell()
        curr_timestamp = time.time()
        percent = curr_ptr * 100.0 / pcap_filesize
        bytes_read = curr_ptr - prev_read
        prev_read = curr_ptr
        dt = curr_timestamp - prev_timestamp
        prev_timestamp = curr_timestamp
        read_rate_mbps = bytes_read * 8.0 / dt / 1e6
        print "%d packets read, %dB/%dB: %.3f%% complete, rate: %.3f Mb/s" % (total_pkts, f.tell(), pcap_filesize, percent, read_rate_mbps)
    return prev_read, prev_timestamp


def packed_match_verify(eth, match_pox, match_dpkt, packed_match_pox, packed_match_dpkt):
    """Verify that two different OFPMatch-from-pkt implementations do, 
    in fact, produce the same result.  They won't; the OF spec is ambiguous or
    POX pkt parsing is broken sometimes.
    
    See specific issue below, in the comments.
    """
    try:
        ETHERTYPE_IPV6 = 0x86dd
        # POX doesn't parse packets with long options correctly.
        # It doesn't put the TCP src and dst into these.
        #import dpkt
        #import dpkt.ip
        #import code
        #code.interact(local=locals())
        has_long_tcp_opts = (isinstance(eth.data, dpkt.ip.IP) and
            isinstance(eth.data.data, dpkt.tcp.TCP) and
            len(eth.data.data.opts) >= 24)
        # POX does not wildcard the match when coming across an IP
        # framgent.
        # See spec v1.0, p9:
        # For IP packets with nonzero fragment offset or 
        # More Fragments bit set, the transport ports are set to zero for the lookup.
        fragment = (isinstance(eth.data, dpkt.ip.IP) and 
            (eth.data.off & (dpkt.ip.IP_MF | dpkt.ip.IP_OFFMASK)) != 0)
        # Avoid comparison; POX-created wildcards don't concern
        # themselves with nw_src and nw_dst wildcard fields for
        # IPv6 packets.
        if (match_dpkt.dl_type != ETHERTYPE_IPV6 and
            not is_8023(buf) and not has_long_tcp_opts and
            not fragment):
            #print "asserting1:"
            assert len(packed_match_dpkt) == OFP_MATCH_LEN
            #print "asserting2:"
            assert packed_match_dpkt == packed_match_pox
            match = match2
    except AssertionError as e:
        print ("WARN: dpkt != pox: %s." % packet_index)
        #print "eth: %s" % repr(eth)
        #print "match1 (pox) : %s" % str(match)
        #print "match2 (dpkt): %s" % repr(match2)
        #print "packed_match1  (pox): %s" % str(packed_match)
        #print "packed_match2 (dpkt): %s" % str(packed_match2)
        #print dpkt.hexdump(packed_match)
        #print dpkt.hexdump(packed_match2)
        #import code
        #code.interact(local=locals())


# If True, store OFP matches, packed, and generate stats from them, using POX.
DO_PACKED_MATCH_POX = False

# If True, store OFP matches, packed, and generate stats from them, using dpkt.
DO_PACKED_MATCH_DPKT = True

DO_PACKED_MATCH = DO_PACKED_MATCH_POX or DO_PACKED_MATCH_DPKT

# Verify that POX and dpkt packed matches are the same?
DO_PACKED_MATCH_VERIFY = False

# If True, verify that length matches what we would expect.  Use the best guess
# of actual packet length from parsing the protocol.
# If False, use the reported PCAP packet length.  Occasionally this is slightly
# larger than the protocol-computed length due to min packet size and packing
# to fit either 128B (seen occasionally) or to round up to an even number of
# bytes. Hence, False here will slightly improve the computed compression
# ratios.
DO_LENGTH_CHECK = False

# Chances are, you want this left in... it does the important flow-tracking
# stuff.  Exists only to understand the performance impact of the "other
# stuff".
DO_LOOKUP_TABLE_UPDATE = True


def process_pcap(filename_in):
    f = open(filename_in)
    pcap_filesize = os.path.getsize(filename_in)
    pcap = ReaderPlusLen(f)

    # Useful progress stats
    prev_read = 0
    prev_timestamp = time.time()

    orig_flowkeys = filename_in + '.flowkeys_orig'
    orig_flowkeys_file = open(orig_flowkeys, 'w')
    orig_flowkeys_pcap = dpkt.pcap.Writer(orig_flowkeys_file)

    # Main data structure:
    # Key: pkt buf, possibly truncated, w/zeroed-out fields
    # Values:
    #   pkts: number of packets
    #   flow_dpkt: dpkt object with zeroed-out fields
    lookup_table = {}  # key is a variable-length string with non-flow-identifying
    lookup_table_flow = {} # key same as above, but check flow_analysis.py for value defn

    stats = init_stats()

    ofpmd_file = None
    if DO_PACKED_MATCH:
        # Generate ofpmd format; ofpmatch dump.
        # Format:
        # [flowkey][flowkey]...
        # each flowkey is OFP_MATCH_LEN bytes.
        # Yep, that's it.
        # Write OFPMatchDump file
        ofpmd_filepath = filename_in + '.' + 'ofpmd'
        ofpmd_file = open(ofpmd_filepath, 'w')


    # 1-indexed to match wireshark
    packet_index = 0

    try:
        for ts, buf, orig_len in pcap:
            packet_index += 1

            if VERBOSE:
                print SEP

            # First, parse the dpkt object for later use. 
            try:
                eth = dpkt.ethernet.Ethernet(buf)
            except IndexError as e:
                print "WARN: parsing failure at packet index %i: [%s]; ignoring." % (packet_index, str(e))
                stats["drops"] += 1
                continue

            if VERBOSE:
                print "\tread pkt: %s" % repr(eth)

            if DEBUG_PACKET and (packet_index != DEBUG_PACKET):
                continue

            if DO_LENGTH_CHECK:
                # Checking the length is no longer necessary, now that it's
                # directly provided by the PCAP file.  Still, there are a number
                # of interesting cases where the actual length differs from our
                # expectation by parsing it, so let's keep this code in, but
                # only use the actual from-the-pcap-file length (orig_len).
                pkt_len = packet_len(eth, buf, packet_index)
                if pkt_len is 0:
                    # Indeterminate length from protocol?  Defer to pcap for true
                    # length.
                    stats["known_len_bytes"] += orig_len # WAS: += pkt_len
                    stats["known_len_pkts"] += 1
                    note_pkt_len(stats, orig_len)
                elif pkt_len is not None:
    
                    if pkt_len_mismatch(pkt_len, orig_len):
                        stats["len_mismatch_pkts"] += 1
                        if VERBOSE:
                            print "WARN: unexpected length at pkt %i: orig %i != %i" % (
                                packet_index, orig_len, pkt_len)
                        #import code
                        #code.interact(local=locals())
                        #print dpkt.hexdump(buf)
                    stats["known_len_bytes"] += orig_len # WAS: += pkt_len
                    stats["known_len_pkts"] += 1
                    note_pkt_len(stats, pkt_len)
                else:
                    stats["unknown_len_pkts"] += 1
                    eth_type = eth.type
                    print "WARN: invalid ethernet type for length parsing at pkt %i: %i (0x%x)" % (
                        packet_index, eth_type, eth_type)
                    print dpkt.hexdump(buf)
            else:
                stats["known_len_bytes"] += orig_len
                stats["known_len_pkts"] += 1
                note_pkt_len(stats, orig_len)

            packed_match = None
            match = None
            if DO_PACKED_MATCH_POX:
                try:
                    pox_pkt = ethernet(buf)
                    match = libof.ofp_match.from_packet(pox_pkt)
                    packed_match = match.pack()
                    assert len(packed_match) == OFP_MATCH_LEN
                except Exception as e:
                    print ("WARN: packet could not be parsed by POX: %s." %
                           packet_index)
                    print e
                    print dpkt.hexdump(buf)
                    stats["drops"] += 1
            else:
                packed_match = ""

            if DO_PACKED_MATCH_DPKT:
                try:
                    match_dpkt = ofpm_from_packet(eth)
                    packed_match_dpkt = match_dpkt.pack()
                    if DO_PACKED_MATCH_VERIFY:
                        packed_match_verify(eth, match_pox, match, packed_match, packed_match_dpkt)
                    else:
                        packed_match = packed_match_dpkt

                except Exception as e:
                    print ("WARN: could not for ofp_match from dpkt: %s." %
                           packet_index)
                    #print "eth: %s" % repr(eth)
                    #print "match1 (pox) : %s" % str(match)
                    #print "match2 (dpkt): %s" % repr(match2)
                    #print "packed_match1  (pox): %s" % str(packed_match)
                    #print "packed_match2 (dpkt): %s" % str(packed_match2)
                    #import code
                    #code.interact(local=locals())
                    print e
                    print dpkt.hexdump(buf)
                    stats["drops"] += 1
            else:
                packed_match = ""


            # Last step: run flowify(), which modies the dpkt object.
            try:
                flow_dpkt, klen = flowify_ofp_match(eth)
            except Exception as e:
                print ("WARN: dropping packet that could not be parsed: %s." %
                       packet_index)
                print e
                print dpkt.hexdump(buf)
                stats["drops"] += 1
                continue

            # Track packet bytes
            stats['pcap_pkt_bytes'] += len(buf)
            stats['flowkey_size_all'] += klen

            if VERBOSE:
                print "\tflow pkt: %s" % repr(flow_dpkt)

            if DO_LOOKUP_TABLE_UPDATE:
                key = str(flow_dpkt)[:klen]
                add_pkt(lookup_table, key, buf, flow_dpkt, orig_flowkeys_pcap, ofpmd_file, packed_match)
                add_pkt_flow(lookup_table_flow, key, buf, flow_dpkt, packed_match, orig_len)

            stats["total_pkts"] += 1

            prev_read, prev_timestamp = update_status(f, prev_read, prev_timestamp, pcap_filesize, stats["total_pkts"])

    except dpkt.dpkt.NeedData as e:
        raise Exception("NeedData error; check validity of pcap input.")
    finally:
        f.close()
        orig_flowkeys_pcap.close()
        print "Wrote flowkeys original packets to %s" % orig_flowkeys

    stats['flows'] = len(lookup_table)
    stats['flow_totals'] = [v[0] for v in lookup_table.values()]
    stats['flowkey_size_unique'] = sum([len(key) for key in lookup_table])

    for stat_name in COMPUTED_STATS:
        update_stat(stats, stat_name)

    if DO_PACKED_MATCH:
        ofpmd_file.close()
        if not os.path.exists(ofpmd_filepath):
            raise Exception("could not find file: %s" % ofpmd_filepath)
        orig_size = os.path.getsize(ofpmd_filepath)
        # Check that the file was properly written:
        assert orig_size == stats['packed_hdr_flowkeys']

        for fcn in compress.COMPRESSION_FCNS:
            # Compress and get the size.
            output, output_filepath = fcn(ofpmd_filepath)
            ratio_print_name = "Original / PackedHdrs+%s" % fcn.__name__
            ratio_size_name = "packedhdrs+%s" % fcn.__name__
            if not os.path.exists(output_filepath):
                stats[ratio_size_name] = UNIMPLEMENTED
            else:
                size = os.path.getsize(output_filepath)
                stats[ratio_size_name] = size

    if SHOW_STATS:
        print_report(stats)

    if WRITE_STATS:
        stats_out = filename_in + '.stats'
        stats_file = open(stats_out, 'w')
        pickle.dump(stats, stats_file)
        print "Wrote stats to %s" % stats_out

    if WRITE_FLOWKEYS:
        flowkeys_out = filename_in + '.flowkeys'
        flowkeys_file = open(flowkeys_out, 'w')
        pcap_out = dpkt.pcap.Writer(flowkeys_file)
        for key in lookup_table:
            pcap_out.writepkt(key)
        pcap_out.close()
        print "Wrote flowkeys to %s" % flowkeys_out

    if WRITE_FLOWS:
        flows_out = filename_in + '.flows'
        flows_file = open(flows_out, 'w')
        pickle.dump(lookup_table, flows_file)
        print "Wrote flows to %s" % flows_out

    if WRITE_FLOW_PACKETS:
        flow_pkt_out = filename_in + ".flows.pickle"
        flow_pkt_file = open(flow_pkt_out, "w")
        pickle.dump(picklable(lookup_table_flow), flow_pkt_file)
        print "Write flows (with packets) to %s" % flow_pkt_out

    if VERBOSE:
        for key, vals in lookup_table.iteritems():
            if not COUNTERS_ONLY:
                pkts, pkt_str, orig_dpkt, flow_dpkt = vals
                print SEP
                print "%i" % pkts
                if SHOW_ORIG:
                    print "\torig: %s" % repr(orig_dpkt)
                print "\tflow: %s" % repr(flow_dpkt)


if __name__ == '__main__':
    # Param is filename to read
    assert len(sys.argv) >= 2
    filename_in = sys.argv[1]
    process_pcap(filename_in)
