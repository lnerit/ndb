#!/usr/bin/python

import sys
import json

from plot_bar_graph import plot_data_bar
from clemson import CLEMSON_QUERY_ORDER

import matplotlib.pyplot as plt

if __name__ == '__main__':
    infile = sys.argv[1]

    # read and close the data file
    f = open(infile, 'r')
    j = json.load(f)
    f.close()

    # parse data
    data = {}

    # our queries are a subset of clemson queries
    # preserve order form CLEMSON_QUERY_ORDER
    group_order = []
    for query in CLEMSON_QUERY_ORDER:
        if query in j.keys():
            group_order.append(query)

    # no subgroups really
    subgroup_order = ['']

    # load data
    for k, v in j.iteritems():
        data[k] = {'': v * 1e6}

    # plot parameters
    plot_dir = '.'
    if(len(sys.argv) > 2):
        plot_dir = sys.argv[2]

    show_plot = False
    save_plot = True
    plot_data = data
    xlabel = '' # 'Packet History Filter Type'
    ylabel_fcn = lambda x: 'Latency (us)'

    # plot bar-graph
    plot_data_bar(plot_dir, plot_data, show_plot, save_plot, group_order,
                  subgroup_order, 'regex_match_clemson', 
                  legend_title = None, 
                  no_legend = True,
                  xlabel = xlabel, 
                  ylabel_fcn = ylabel_fcn,
                  axis_left = 0.05, axis_right = 0.97, axis_bottom = 0.35,
                  size_x = 15,
                  group_name_map = {},
                  subgroup_name_map = {},
                  grid = 'y')

