#!/usr/bin/python

from clemson import ClemsonTopo, CLEMSON_SW_TO_DPID, parse_clemson_trace
from clemson import hostify, locate_ipv4
from analysis_lib import ip_dd, autodict
from postcards import postcards_on_path, NO_INGRESS_INPORT
import networkx as nx
import dpkt
import struct
import sys

#Postcard structure:
#    int len: length of the pakcet buffer 
#    char *pkt: packet buffer
#    int dpid: datapath ID of the switch
#    int inport: input port
#    int version: forwarding state version

def write_postcard(pcard, fp):
    pkt_str = pcard['pkt'].pack() if pcard['pkt'] else None
    pkt_len = len(pkt_str) if pkt_str else 0
    fp.write(struct.pack("@i", pkt_len))
    if pkt_str:
        fp.write(pkt_str)
    fp.write(struct.pack("@3i", 
        pcard['dpid'], pcard['inport'], pcard['version']))

def write_history(history, fp):
    fp.write(struct.pack("@i", len(history)))
    for pcard in history:
        write_postcard(pcard, fp)
    # finally write NULL pcard
    write_postcard({'pkt': None,
        'dpid': 0,
        'inport': 0,
        'version': 0}, fp)

def create_history(eth, t, shortest_paths, sw_to_dpid, fp):
    if eth.type == dpkt.ethernet.ETH_TYPE_IP:
        ip = eth.data
        src = ip.src
        dst = ip.dst
        src_loc = hostify(locate_ipv4(ip_dd(src)))
        dst_loc = hostify(locate_ipv4(ip_dd(dst)))
        node_path = shortest_paths[src_loc][dst_loc]
        path = postcards_on_path(t, node_path, NO_INGRESS_INPORT,
                eth, sw_to_dpid)
        if len(path) <= 0:
            return
        write_history(path, fp)

def gen_clemson_histories(pcap_infile, hist_outfile):
    histories = []
    t = ClemsonTopo()
    filename = pcap_infile
    hist_f = open(hist_outfile, 'wb')
    shortest_paths = nx.all_pairs_shortest_path(t.g)
    parse_clemson_trace(filename, lambda eth: create_history(eth, t,
        shortest_paths, CLEMSON_SW_TO_DPID, hist_f))

    hist_f.close()

if __name__ == '__main__':
    gen_clemson_histories(sys.argv[1], sys.argv[2])
