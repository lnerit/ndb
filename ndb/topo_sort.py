#!/usr/bin/env python
"""
Topo sort implementation(s).

Crucial algorithm in NDB to turn a set of postcards into a list that matches
the topology.
"""
import logging
import random
import unittest
from itertools import permutations
from collections import namedtuple

import dpkt
from openfaucet import ofproto, ofmatch, buffer
from mininet.topo import LinearTopo, Topo
import networkx as nx

# TODO: rename mn-linetopo
#linetopo = __import__('test/mn-linetopo')
from topology import NDBTopo, NDBTopoFastSort
from test.dump_topo import dump_out_band_topo
import analysis.postcards as postcards
from analysis.analysis_lib import allocate_ips, allocate_dpids
from analysis.traffic import IPFlow


lg = logging.getLogger("topo_sort")


def topo_sort(matches, psid_to_dpid, topo, safe, ft_db, logger):
    '''topo_sort postcards into an ordered packetpath in O(n^2) time

    matches: list of postcards (TODO: note format).
    psid_to_dpid: dict from psid to dpid, keys & vals are uints
    topo: Topo or NDBTopo object
    safe: set safe = True when calling check_pp_link?
    ft_db: if safe, MongoHandler object used in check pp_link
    logger: Python Logger instance

    Start with the first switch as the only element of the sorted list.
    Iterate through the remainder of the switches to find the dpid that is
    connected either to the head or the tail of the sorted list.

    NOTE: A naive implementation of the above algorithm could also result in the
    algorithm outputting the reverse path.

    For the correct behavior, we should also check for the 'outport' and
    the corresponding 'inport' from the matched entry.
    '''
    init_m = list(matches) # Safe backup to return if things don't go well
    sorted_m = list() # Sorted list of matches
    pp = list() # Sorted list of pcards to return if things do go well

    #logger.debug('Topo-sorting matched postcards...')

    if len(init_m) <= 1:
        return init_m

    curr_m = init_m[0]
    (curr_version, curr_outport, curr_psid) = curr_m['tag']
    curr_dpid = psid_to_dpid.get(curr_psid)
    sorted_m.append(curr_m)
    pp.append({
        'pkt': curr_m['pkt'],
        'dpid': curr_dpid,
        'inport': None,
        'outport': curr_outport,
        'version':curr_version
        })
    matches.remove(curr_m)

    try_more = True
    while try_more and len(matches) > 0:
        assert len(sorted_m) == len(pp)
        assert len(sorted_m) + len(matches) == len(init_m)

        cleanup_m = [] # the list of postcards that are connected to sorted_m
        for m in matches:
            (m_version, m_outport, m_psid) = m['tag']
            m_dpid = psid_to_dpid.get(m_psid)
            # check with tail of the list
            # Always try at the tail first
            tail_m = sorted_m[-1]
            tail_pcard = pp[-1]
            tail_dpid = tail_pcard['dpid']
            if check_pp_link(tail_m, m, safe, topo, psid_to_dpid, ft_db):
                sorted_m.append(m)
                inport = topo.g[tail_dpid][m_dpid][m_dpid]
                pp.append({
                    'pkt': m['pkt'],
                    'dpid': m_dpid,
                    'inport': inport,
                    'outport': m_outport,
                    'version': m_version
                    })
                cleanup_m.append(m)
                continue

            # check with head of the list
            head_m = sorted_m[0]
            head_pcard = pp[0]
            head_dpid = head_pcard['dpid']
            if check_pp_link(m, head_m, safe, topo, psid_to_dpid, ft_db):
                sorted_m.insert(0, m)
                pp.insert(0, {
                    'pkt': m['pkt'],
                    'dpid': m_dpid,
                    'inport': None,
                    'outport': m_outport,
                    'version': m_version
                    })
                inport = topo.g[head_dpid][m_dpid][head_dpid]
                head_pcard['inport'] = inport
                cleanup_m.append(m)

        for m in cleanup_m:
            matches.remove(m)
        try_more = len(cleanup_m) > 0

    assert len(sorted_m) == len(pp)
    assert len(sorted_m) + len(matches) == len(init_m)

    # Append whatever is left
    if len(sorted_m) != len(init_m):
        logger.error('Incomplete set of postcards - ' +
                'couldn\'t topo sort them')
        for m in matches:
            (m_version, m_outport, m_psid) = m['tag']
            m_dpid = psid_to_dpid.get(m_psid)
            sorted_m.append(m)
            pp.append({
                'pkt': m['pkt'],
                'dpid': m_dpid,
                'inport': None,
                'outport': m_outport,
                'version': m_version
                })
    return pp


def check_pp_link(a_m, b_m, safe, topo, psid_to_dpid, ft_db):
    '''Checks if the two postcards a_m and b_m are directly linked

    a_m: postcard
    b_m: postcard
    safe: set safe = True when calling check_pp_link?
    psid_to_dpid: dict from psid to dpid, keys & vals are uints
    topo: NDBTopo object
    ft_db: if safe, MongoHandler object used in check pp_link
    '''
    (a_version, a_outport, a_psid) = a_m['tag']
    a_dpid = psid_to_dpid.get(a_psid)

    (b_version, b_outport, b_psid) = b_m['tag']
    b_dpid = psid_to_dpid.get(b_psid)

    if a_dpid is None or b_dpid is None:
        return False

    if b_dpid not in topo.g[a_dpid]:
        return False

    outport = topo.g[a_dpid][b_dpid][a_dpid]
    if (a_outport != outport) and (a_outport != ofproto.OFPP_FLOOD):
        return False

    # double check with the flow record that the inport is indeed correct
    if safe:
        b_rec = ft_db.get_records({'dpid': b_dpid, 'version': b_version})
        assert b_rec.count() == 1
        r = b_rec[0]
        match_str = str(r['match'])
        try:
            buf = buffer.ReceiveBuffer()
            buf.append(match_str)
            buf.set_message_boundaries(len(match_str))
            b_match = ofmatch.Match.deserialize(buf)

            inport = topo.g[a_dpid][b_dpid][b_dpid]
            if (b_match.in_port) and (b_match.in_port != inport):
                return False
        except:
            pass

    return True


def topo_sort_fast(matches, psid_to_dpid, topo, safe, ft_db, logger):
    '''topo_sort postcards into an ordered packetpath in O(p) time

    matches: list of postcards (TODO: note format).
    psid_to_dpid: dict from psid to dpid, keys & vals are uints
    topo: NDBTopo object
    safe: set safe = True when calling check_pp_link?
    ft_db: if safe, MongoHandler object used in check pp_link
    logger: Python Logger instance
    '''
    pcard_table = {}  # pcard_table[dpid] -> pcard
    # at any time, sorted_list stores the tail segment of the path.
    sorted_list = []
    curr_list = []

    # populate pcard_table
    for m in matches:
        (m_version, m_outport, m_psid) = m['tag']
        m_dpid = psid_to_dpid.get(m_psid)
        pcard_table[m_dpid] = {
                'pkt': m['pkt'],
                'dpid': m_dpid,
                'inport': None,
                'outport': m_outport,
                'version': m_version
                }

    while len(pcard_table) > 0:
        if len(curr_list) == 0:
            dpid = pcard_table.keys()[0]
            curr_list.append(pcard_table[dpid])
            del pcard_table[dpid]
        # Invariant at this stage: len(curr_list) > 0.
        pcard = curr_list[-1]
        pcard_dpid = pcard['dpid']
        next_dpid = None
        next_inport = None
        for nbr in topo.g[pcard_dpid]:
            outport = topo.g[pcard_dpid][nbr][pcard_dpid]
            if outport == pcard['outport'] or \
                    pcard['outport'] == ofproto.OFPP_FLOOD:
                        if nbr in pcard_table:
                            next_dpid = nbr
                            next_inport = topo.g[pcard_dpid][nbr][nbr]
                            break
        # if path continues:
        if next_dpid:
            pcard_table[next_dpid]['inport'] = next_inport
            curr_list.append(pcard_table[next_dpid])
            del pcard_table[next_dpid]
        # Otherwise, end of the chain, or multiple disconnected groups.
        # If at end of path, add it all
        else:
            sorted_list = curr_list + sorted_list
            curr_list = []
    return curr_list + sorted_list

"""
A topo sort implementation that is optimized for understandability on
unicast traffic.  Does not try to handle broken paths or loops.

TODO:
Unit test handling of random deletions in the path, should return set.
Likely need to consider that chunks may not be contiguous, in which case, 
return them all.

TODO:
Unit test for multiple outports, including OFPP_FLOOD port.  Return tree?

TODO
Clarity:
- s/dpid/sw
- replace paper pseudocode
- consider using http://pypi.python.org/pypi/recordtype to replace namedtuples
Performance:
- build in reverse, then flip at end, to prevent array shifts

Psuedocode:

path = []  # List of postcards in topo-sorted order
while len(pcards) > 0:
    pcard = grab_any_pcard()
    # Follow path from this pcard to create a path segment
    path_segment = [random pcard]
    while True:
        next_sw = topo.opposite(pcard.sw, pcard.outport)
        if next_sw in pcards:
            next_pcard = pcard[next_sw]
            if FILL_IN_INPORTS:
                next_pcard.inport = topo.port(next_pcard.sw, pcard.sw)
            path_segment.append(next_pcard)
            pcard = next_pcard
        else:
            break
    # Append the path segment
    path = path_segment + path
    if FILL_IN_INPORTS and len(path) > 0:
        path[0].inport = topo.port(pcard.dpid, path[0].dpid)
return path
"""
FILL_IN_INPORTS = False


def topo_sort_clean_fasttopo(pcard_list, psid_to_dpid, topo, safe, ft_db, logger):
    '''topo_sort postcards into an ordered packetpath in O(p) time

    pcard_list: list of postcards as dicts.
    psid_to_dpid: dict from psid to dpid, keys & vals are uints
    topo: NDBTopoFastSort object
    safe: set safe = True when calling check_pp_link?
    ft_db: if safe, MongoHandler object used in check pp_link
    logger: Python Logger instance
    '''

    pcard_table = {}  # pcard_table[dpid] -> pcard; enables O(1) lookup by dpid.
    for p in pcard_list:
        version, outport, psid = p['tag']  # Should be removed
        p['dpid'] = psid_to_dpid[psid]  # Should be removed, put in topo obj
        pcard_table[p['dpid']] = p

    path = []  # List of postcards in topo-sorted order
    while len(pcard_table) > 0:
        dpid, pcard = pcard_table.popitem()
        # Follow path from this pcard to create a path segment
        path_segment = [pcard]
        while True:
            next_sw = topo.opposite[pcard['dpid']].get(pcard['outport'])
            if next_sw in pcard_table:
                next_pcard = pcard_table.pop(next_sw)
                if FILL_IN_INPORTS:
                    next_pcard['inport'] = topo.port[next_pcard['dpid']].get(pcard['dpid'])
                path_segment.append(next_pcard)
                pcard = next_pcard
            else:
                break
        # Append the most recent path segment
        path = path_segment + path
        if FILL_IN_INPORTS and len(path) > 0:
            path[0]['inport'] = topo.port[pcard['dpid']].get(path[0]['dpid'])

    return path


def topo_sort_clean(pcard_list, psid_to_dpid, topo, safe, ft_db, logger):
    '''topo_sort postcards into an ordered packetpath in O(p) time

    pcard_list: list of postcards as dicts.
    psid_to_dpid: dict from psid to dpid, keys & vals are uints
    topo: NDBTopo object
    safe: set safe = True when calling check_pp_link?
    ft_db: if safe, MongoHandler object used in check pp_link
    logger: Python Logger instance
    '''
    topo2 = NDBTopoFastSort(topo)
    return topo_sort_clean_fasttopo(pcard_list, psid_to_dpid, topo2, safe, ft_db, logger)


"""
Memory benchmarking to motivate namedtuple

import sys
from collections import namedtuple
Postcard = namedtuple('Postcard', ['dpid', 'inport', 'outport', 'pkt', 'version'])
p = Postcard(1, 2, 3, 4, 5)
p2 = {'dpid': 1, 'inport': 2, 'outport': 3, 'pkt': 4, 'version': 5}
sys.getsizeof(p)
sys.getsizeof(p2)
"""

Postcard = namedtuple('Postcard', ['dpid', 'inport', 'outport', 'pkt', 'version'])


def namedtuple_pcard_list(pcard_list, psid_to_dpid):
    """pcard_list is dict."""
    namedtuple_list = []
    for p in pcard_list:
        version, outport, psid = p['tag']  # Should be removed
        dpid = psid_to_dpid[psid]  # Should be removed, put in topo obj
        inport = None
        nt = Postcard(dpid, inport, outport, p['pkt'], version)
        namedtuple_list.append(nt)
    return namedtuple_list


def dict_pcard_list(pcard_list):
    """pcard_list is namedtuple."""
    dict_pcard_list = []
    for p in pcard_list:
        d = {
             'dpid': p.dpid,
             'inport': p.inport,
             'outport': p.outport,
             'pkt': p.pkt,
             'version': p.version,
        }
        dict_pcard_list.append(d)
    return dict_pcard_list



def topo_sort_clean_namedtuple(pcard_list, psid_to_dpid, topo, safe, ft_db, logger):
    '''topo_sort postcards into an ordered packetpath in O(p) time

    pcard_list: list of postcards as namedtuples, not dicts.
    psid_to_dpid: dict from psid to dpid, keys & vals are uints
    topo: NDBTopoFastSort object
    safe: set safe = True when calling check_pp_link?
    ft_db: if safe, MongoHandler object used in check pp_link
    logger: Python Logger instance
    '''

    pcard_table = {}  # pcard_table[dpid] -> pcard; enables O(1) lookup by dpid.
    for p in pcard_list:
        pcard_table[p.dpid] = p

    path = []  # List of postcards in topo-sorted order
    while len(pcard_table) > 0:
        dpid, pcard = pcard_table.popitem()
        # Follow path from this pcard to create a path segment
        path_segment = [pcard]
        while True:
            next_sw = topo.opposite[pcard.dpid].get(pcard.outport)
            if next_sw in pcard_table:
                next_pcard = pcard_table.pop(next_sw)
                if FILL_IN_INPORTS:
                    inport = topo.port[next_pcard.dpid].get(pcard.dpid) 
                    next_pcard = next_pcard._replace(inport = inport)
                path_segment.append(next_pcard)
                pcard = next_pcard
            else:
                break
        # Append the most recent path segment
        path = path_segment + path
        if FILL_IN_INPORTS and len(path) > 0:
            path[0].inport = topo.port[pcard.dpid].get(path[0].dpid)

    return path


def topo_sort_namedtuple(pcard_list, psid_to_dpid, topo, safe, ft_db, logger):
    '''topo_sort postcards into an ordered packetpath in O(p) time

    pcard_list: list of postcards as dicts.
    psid_to_dpid: dict from psid to dpid, keys & vals are uints
    topo: NDBTopo object
    safe: set safe = True when calling check_pp_link?
    ft_db: if safe, MongoHandler object used in check pp_link
    logger: Python Logger instance
    '''
    topo2 = NDBTopoFastSort(topo)
    pcard_list2 = namedtuple_pcard_list(pcard_list, psid_to_dpid)
    sorted_list = topo_sort_clean_namedtuple(pcard_list2, psid_to_dpid, topo2, safe, ft_db, logger)
    return dict_pcard_list(sorted_list)


# Copied from test/linetopo_match.py
def create_postcards_orig(filename, randomize=False):
    pcards = []
    pcapReader = dpkt.pcap.Reader(file(filename))
    for ts_, data in pcapReader:
        pkt = dpkt.ethernet.Ethernet(data)
        pcards.append({
                'pkt': pkt,
                'tag': get_tag(pkt)
                })
    if randomize:
        random.shuffle(pcards)
    return pcards


# Copied from test/linetopo_match.py
def create_postcards_orig(filename, randomize=False):
    pcards = []
    pcapReader = dpkt.pcap.Reader(file(filename))
    for ts_, data in pcapReader:
        pkt = dpkt.ethernet.Ethernet(data)
        pcards.append({
                'pkt': pkt,
                'tag': get_tag(pkt)
                })
    if randomize:
        random.shuffle(pcards)
    return pcards


def gen_chain_pcards(n):
    #topo_obj = linetopo.LineTopo(n)
    #topo_json = dump_out_band_topo(topo_obj, os.path.devnull)
    #topo = NDBTopo(topo_json = topo_json)
    #XXX psid_json = json.load(open(args.psid))
    #XXX for k, v in psid_json.iteritems():
    #XXX    psid_to_dpid[int(k)] = v

    #ppf_str = '.*{{--dpid %d --inport 1}}' % chain_len
    #ppf = PacketPathFilter(ppf_str, open(os.path.devnull, 'w'))
    # TODO: replace w/simulated stuff

    t = LinearTopo(n)
    host_to_ip = allocate_ips(t)
    sw_to_dpid = allocate_dpids(t)
    traffic = 1
    shortest_paths = nx.all_pairs_shortest_path(t.g)
    src = t.hosts()[0]
    dst = t.hosts()[-1]
    src_ip = host_to_ip[src]
    dst_ip = host_to_ip[dst]
    flow = IPFlow(traffic, src_ip, dst_ip)
    pkt = flow.pkt
    # TODO: generalize to hosts with multiple neighbors
    node_path = shortest_paths[src][dst]

    pcards = postcards.postcards_on_path(t, node_path, postcards.NO_INGRESS_INPORT, pkt,
                                sw_to_dpid)
    postcards.clean_dstmac(pcards)

    psid_to_dpid = {}
    for sw in t.switches():
        dpid = sw_to_dpid[sw]
        psid_to_dpid[dpid] = dpid

    # XXX TODO
    # convert from nx graph of names to nx graph of dpids
    topo = NDBTopo(topo=t, sw_to_dpid=sw_to_dpid)
    return pcards, psid_to_dpid, topo


def get_dpids(pcards):
    return [p['dpid'] for p in pcards]


# Lengths to test w/varying orders
CHAIN_LENS = [4, 8, 16, 32]

# Number of random orderings for each length
RANDOM_ORDERS = 10

# Lengths to test for all permutations.
ALL_PERMUTATIONS_CHAIN_LENS = [4] #[1, 2, 3, 4]



# Recipe from Eli Bendersky
# http://eli.thegreenplace.net/2011/08/02/python-unit-testing-parametrized-test-cases/
class ParametrizedTestCase(unittest.TestCase):
    """ TestCase classes that want to be parametrized should
        inherit from this class.
    """
    def __init__(self, methodName='runTest', param=None):
        super(ParametrizedTestCase, self).__init__(methodName)
        self.param = param

    @staticmethod
    def parametrize(testcase_klass, param=None):
        """ Create a suite containing all tests taken from the given
            subclass, passing them the parameter 'param'.
        """
        testloader = unittest.TestLoader()
        testnames = testloader.getTestCaseNames(testcase_klass)
        suite = unittest.TestSuite()
        for name in testnames:
            suite.addTest(testcase_klass(name, param=param))
        return suite


class TestChainTopo(ParametrizedTestCase):
    """Test for correctness with varying-length chain topologies."""

    def test_ordered(self):
        '''Already-ordered postcards.'''
        for chain_len in CHAIN_LENS:
            pcards, psid_to_dpid, topo = gen_chain_pcards(chain_len)
            sorted_pcards = self.param(pcards, psid_to_dpid, topo,
                                      safe = False, ft_db = None, logger = lg)
            dpids = get_dpids(sorted_pcards)
            self.assertEquals(dpids, sorted(dpids))

    def test_reverse_ordered(self):
        '''Reverse-ordered postcards.'''
        for chain_len in CHAIN_LENS:
            pcards, psid_to_dpid, topo = gen_chain_pcards(chain_len)
            pcards.reverse()
            sorted_pcards = self.param(pcards, psid_to_dpid, topo,
                                      safe = False, ft_db = None, logger = lg)
            dpids = get_dpids(sorted_pcards)
            self.assertEquals(dpids, sorted(dpids))

    def test_random(self):
        '''Randomly ordered postcards.'''
        for chain_len in CHAIN_LENS:
            pcards, psid_to_dpid, topo = gen_chain_pcards(chain_len)
            for i in range(RANDOM_ORDERS):
                random.shuffle(pcards)
                sorted_pcards = self.param(pcards, psid_to_dpid, topo,
                                          safe = False, ft_db = None, logger = lg)
                dpids = get_dpids(sorted_pcards)
                self.assertEquals(dpids, sorted(dpids))

    def test_all_perms(self):
        '''All permutations of postcards.'''
        for chain_len in ALL_PERMUTATIONS_CHAIN_LENS:
            pcards, psid_to_dpid, topo = gen_chain_pcards(chain_len)
            for pcards_perm in permutations(pcards, chain_len):
                pcards_list = list(pcards_perm)
                sorted_pcards = self.param(pcards_list, psid_to_dpid, topo,
                                          safe = False, ft_db = None, logger = lg)
                dpids = get_dpids(sorted_pcards)
                self.assertEquals(dpids, sorted(dpids))


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    suite = unittest.TestSuite()
    #for topo_fcn in [topo_sort, topo_sort_fast, topo_sort_clean]:
    for topo_fcn in [topo_sort_namedtuple]:
        suite.addTest(ParametrizedTestCase.parametrize(TestChainTopo, param=topo_fcn))
    unittest.TextTestRunner(verbosity=2).run(suite)
    #unittest.main()