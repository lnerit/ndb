#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "openflow.h"

#include <stdio.h>
#include <stdlib.h>
#include <glib.h>
#include <epan/emem.h>
#include <epan/packet.h>
#include <epan/dissectors/packet-tcp.h>
#include <epan/prefs.h>
#include <epan/ipproto.h>
#include <epan/etypes.h>
#include <epan/addr_resolv.h>
#include <string.h>
#include <arpa/inet.h>

#define PROTO_TAG_PCARD "PCARD"
#define ETH_TYPE_PCARD 0xABCD
#define PKT_ID_LEN 16

#define NO_STRINGS NULL
#define NO_MASK 0x0

/* Wireshark ID of the PCARD protocol */
static int proto_pcard = -1;
static dissector_handle_t pcard_handle;
static void dissect_pcard(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree);

/* traffic will arrive with ETHERTYPE ETH_TYPE_PCARD*/
#define ETHERTYPE_FILTER "ethertype"
static int global_pcard_proto = ETH_TYPE_PCARD;

/* try to find the ethernet dissector to dissect encapsulated Ethernet data */
static dissector_handle_t data_ethernet;

/* These variables are used to hold the IDs of our fields; they are
 * set when we call proto_register_field_array() in proto_register_pcard()
 */
static gint pcard = -1;
static gint pcard_pkt_id = -1;
static gint pcard_path_id = -1;
static gint pcard_dpid = -1;
static gint pcard_inport = -1;
static gint pcard_outport = -1;
static gint pcard_version = -1;
static gint pcard_length = -1;
static gint pcard_data = -1;

/* These are the ids of the subtrees that we may be creating */
static gint ett_pcard = -1;
static gint ett_pcard_data = -1;

/**
 * Adds "hf" to "tree" starting at "offset" into "tvb" and using "length"
 * bytes.  offset is incremented by length.
 */
static void add_child( proto_item* tree, gint hf, tvbuff_t *tvb, guint32* offset, guint32 len ) {
    proto_tree_add_item( tree, hf, tvb, *offset, len, FALSE );
    *offset += len;
}

/**
 * Adds "hf" to "tree" starting at "offset" into "tvb" and using "length"
 * bytes.  offset is incremented by length.  The specified string is used as the
 * field's display value.
 */
static void add_child_str(proto_item* tree, gint hf, tvbuff_t *tvb, guint32* offset, guint32 len, const char* str) {
    proto_tree_add_string(tree, hf, tvb, *offset, len, str);
    *offset += len;
}

static void dissect_port(proto_tree* tree, gint hf, tvbuff_t *tvb, guint32 *offset) {
    /* get the port number */
    guint16 port = tvb_get_ntohs( tvb, *offset );

    /* check to see if the port is special (e.g. the name of a fake output ports defined by ofp_port) */
    const char* str_port = NULL;
    char str_num[6];
    switch( port ) {

    case OFPP_IN_PORT:
        str_port = "In Port  (send the packet out the input port; This virtual port must be explicitly used  in order to send back out of the input port. )";
        break;

    case OFPP_TABLE:
        str_port = "Table  (perform actions in flow table; only allowed for dst port packet out messages)";
        break;

    case OFPP_NORMAL:
        str_port = "Normal  (process with normal L2/L3 switching)";
        break;

    case OFPP_FLOOD:
        str_port = "Flood  (all physical ports except input port and those disabled by STP)";
        break;

    case OFPP_ALL:
        str_port = "All  (all physical ports except input port)";
        break;

    case OFPP_CONTROLLER:
        str_port = "Controller  (send to controller)";
        break;

    case OFPP_LOCAL:
        str_port = "Local  (local openflow \"port\")";
        break;

    case OFPP_NONE:
        str_port = "None  (not associated with a physical port)";
        break;

    default:
        /* no special name, so just use the number */
        str_port = str_num;
        snprintf(str_num, 6, "%u", port);
    }

    /* put the string-representation in the GUI tree */
    add_child_str( tree, hf, tvb, offset, 2, str_port );
}

static void dissect_ethernet(tvbuff_t *next_tvb, packet_info *pinfo, proto_tree *data_tree) {
    /* add seperators to existing column strings */
    if (check_col(pinfo->cinfo, COL_PROTOCOL))
        col_append_str( pinfo->cinfo, COL_PROTOCOL, "+" );

    if(check_col(pinfo->cinfo,COL_INFO))
        col_append_str( pinfo->cinfo, COL_INFO, " => " );

    /* set up fences so ethernet dissectors only appends to our column info */
    col_set_fence(pinfo->cinfo, COL_PROTOCOL);
    col_set_fence(pinfo->cinfo, COL_INFO);

    /* continue the dissection with the ethernet dissector */
    call_dissector(data_ethernet, next_tvb, pinfo, data_tree);
}

void proto_reg_handoff_pcard(void)
{
    pcard_handle = create_dissector_handle(dissect_pcard, proto_pcard);
    dissector_add(ETHERTYPE_FILTER, global_pcard_proto, pcard_handle);
}

void proto_register_pcard(void)
{
    /* A header field is something you can search/filter on.
    *
    * We create a structure to register our fields. It consists of an
    * array of register_info structures, each of which are of the format
    * {&(field id), {name, abbrev, type, display, strings, bitmask, blurb, HFILL}}.
    */
    static hf_register_info hf[] = {
        /* header fields */

        { &pcard,
            { "Postcard", "pcard", FT_NONE, BASE_NONE, NO_STRINGS, NO_MASK, "Postcard", HFILL }},

        { &pcard_pkt_id,
            { "Packet ID", "pcard.pktid", FT_BYTES, BASE_NONE, NO_STRINGS, NO_MASK, "Packet ID", HFILL }},

        { &pcard_path_id,
            { "Path ID", "pcard.pathid", FT_UINT64, BASE_HEX, NO_STRINGS, NO_MASK, "Packet ID", HFILL }},

        { &pcard_dpid,
            { "Datapath ID", "pcard.dpid", FT_UINT64, BASE_HEX, NO_STRINGS, NO_MASK, "Datapath ID", HFILL }},

        { &pcard_inport,
            { "Input port", "pcard.inport", FT_STRING, BASE_NONE, NO_STRINGS, NO_MASK, "Port on which Packet was Received", HFILL }},

        { &pcard_outport,
            { "Output port", "pcard.outport", FT_STRING, BASE_NONE, NO_STRINGS, NO_MASK, "Port to which Packet was Forwarded", HFILL }},

        { &pcard_version,
            { "Matched version", "pcard.version", FT_UINT16, BASE_DEC, NO_STRINGS, NO_MASK, "Flow table entry version that the packet matched", HFILL }},

        { &pcard_length,
            { "Encapsulated postcard length", "pcard.length", FT_UINT16, BASE_DEC, NO_STRINGS, NO_MASK, "Length of the encapsulated postcard", HFILL }},

        { &pcard_data,
            { "Packet header", "pcard.data", FT_NONE, BASE_NONE, NO_STRINGS, NO_MASK, "Packet header", HFILL }},
    };

    static gint *ett[] = {
        &ett_pcard,
        &ett_pcard_data,
    };

    data_ethernet = find_dissector("eth");

    proto_pcard = proto_register_protocol( "NDB Postcard",
            "PCARD",
            "pcard" ); /* abbreviation for filters */

    proto_register_field_array(proto_pcard, hf, array_length(hf));
    proto_register_subtree_array(ett, array_length(ett));
    register_dissector("pcard", dissect_pcard, proto_pcard);
}

static void dissect_pcard(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree)
{
    guint16 len  = 0;

    /* display our protocol text if the protocol column is visible */
    if (check_col(pinfo->cinfo, COL_PROTOCOL))
        col_set_str(pinfo->cinfo, COL_PROTOCOL, PROTO_TAG_PCARD);

    /* Clear out stuff in the info column */
    if(check_col(pinfo->cinfo,COL_INFO))
        col_clear(pinfo->cinfo,COL_INFO);

    if (tree) { /* we are being asked for details */
        proto_item *item        = NULL;
        proto_tree *pcard_tree    = NULL;
        guint32 offset = 0;
        proto_item *data_item = NULL;
        proto_tree *data_tree = NULL;
        guint total_len = 0;
        tvbuff_t *next_tvb = NULL;

        /* consume the entire tvb for the pcard, and add it to the tree */
        item = proto_tree_add_item(tree, proto_pcard, tvb, 0, -1, FALSE);
        pcard_tree = proto_item_add_subtree(item, ett_pcard);

        /* add the headers field as children of the pcard node */
        add_child(pcard_tree, pcard_pkt_id, tvb, &offset, PKT_ID_LEN);
        add_child(pcard_tree, pcard_path_id, tvb, &offset, 8);
        add_child(pcard_tree, pcard_dpid, tvb, &offset, 8);
        dissect_port(pcard_tree, pcard_inport, tvb, &offset);
        dissect_port(pcard_tree, pcard_outport, tvb, &offset);
        add_child(pcard_tree, pcard_version, tvb, &offset, 2);

        len = tvb_get_ntohs(tvb, offset);
        add_child(pcard_tree, pcard_length, tvb, &offset, 2);

        /* continue the dissection with the Ethernet dissector */
        total_len = len - offset;
        if (data_ethernet) {
            data_item = proto_tree_add_item(pcard_tree, pcard_data, tvb, offset, -1, FALSE);
            data_tree = proto_item_add_subtree(data_item, ett_pcard_data);
            next_tvb = tvb_new_subset(tvb, offset, -1, total_len);
            dissect_ethernet(next_tvb, pinfo, data_tree);
        }
        else {
            /* if we couldn't load the ethernet dissector,
             * just display the bytes */
            add_child(pcard_tree, pcard_data, tvb, &offset, total_len);
        }
    }
}

