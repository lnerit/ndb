#!/usr/bin/python

import sys
sys.path.append('../')

from datetime import datetime
import termcolor as T
from master import Master, print_packetpath

def handle_backtrace(pp, ft_db):
    print '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'
    print T.colored('Got a packet backtrace', 'green')
    print T.colored('Date/time: %s' % str(datetime.now()), 'green')
    print
    print_packetpath(pp, ft_db)
    print '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
    print

if __name__ == "__main__":
    c = Master()
    c.start()

    print 'Started the Master collector.'
    try:
        raw_input('Start all the Slave collectors and hit Enter:')
        while True:
            bkpt = raw_input('Enter breakpoint in the form of a PPF\n>>> ')
            cb = lambda pp: handle_backtrace(pp, c.ft_db)
            c.add_filter(bkpt, cb)
    except KeyboardInterrupt:
        c.stop()
        print 'Exiting. Bye!'
