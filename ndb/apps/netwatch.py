#!/usr/bin/python

import sys
sys.path.append('../')

from datetime import datetime
import termcolor as T
from master import Master, print_packetpath

def handle_backtrace(pp, ft_db):
    print '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'
    print T.colored('Got a packet backtrace', 'green')
    print T.colored('Date/time: %s' % str(datetime.now()), 'green')
    print
    print_packetpath(pp, ft_db)
    print '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
    print

def edge_ports(topo):
    '''find all the edge ports given a Mininet Topo object'''
    ep = {}
    for s in topo.switches():
        for nbr in topo.ports[s]:
            if not topo.is_switch(nbr):
                p = topo.ports[s][nbr]
                if s not in ep:
                    ep[s] = []
                ep[s].append(p)
    return ep

def core_ports(topo):
    '''find all the non-edge ports given a Mininet Topo object'''
    cp = {}
    for s in topo.switches():
        for nbr in topo.ports[s]:
            if topo.is_switch(nbr):
                p = topo.ports[s][nbr]
                if s not in cp:
                    cp[s] = []
                cp[s].append(p)
    return cp

def get_ports(host_set, topo):
    '''find the edge ports connected to a host_set given a Mininet Topo object'''
    ports = {}
    for h in host_set:
        for s in topo.ports[h]:
            p = topo.ports[s][h]
            if s not in ports:
                ports[s] = []
            ports[s].append(p)
    return ports

def get_port_ppf(port_dict, port_type='inport'):
    '''generate PPF to match a postcard at a given set of dpid-ports'''
    ppf_str = '['
    for dpid, ports in port_dict.iteritems():
        if len(ports) == 0:
            continue
        port_str = '"(' + '|'.join(map(str, ports)) + ')"'
        ppf_str += '{{--dpid "%s" --%s %s}}' % (dpid, port_type, port_str)
    ppf_str += ']'
    return ppf_str

###### Invariant PPF generation functions ######

def loop():
    '''generate PPF to detect loops in packet paths'''
    return '.*(.).*(\1).*'

def isolation(a_host_set, b_host_set, topo):
    '''generate PPF to detect violation in isolation of two host groups'''
    
    a_port_dict = get_ports(a_host_set, topo)
    b_port_dict = get_ports(b_host_set, topo)

    a_inport_ppf = get_port_ppf(a_port_dict, 'inport')
    a_outport_ppf = get_port_ppf(a_port_dict, 'outport')
    b_inport_ppf = get_port_ppf(b_port_dict, 'inport')
    b_outport_ppf = get_port_ppf(b_port_dict, 'outport')

    a_b_ppf = '^%s.*%s$' % (a_inport_ppf, b_outport_ppf)
    b_a_ppf = '^%s.*%s$' % (b_inport_ppf, a_outport_ppf)

    return [a_b_ppf, b_a_ppf]

def blackhole(topo):
    '''generate PPF to detect blackholes in packet paths'''
    cports = core_ports(topo)
    cport_ppf = get_port_ppf(cports, 'outport')
    return '.*%s$' % cport_ppf

def waypoint_routing(traffic_class, dpid):
    '''generate PPF to detect violation in waypoint routing policy'''
    traffic_non_dpid_ppf = '{{--bpf "%s" --dpid not "%s"}}' % (traffic_class,
            dpid)
    non_dpid_ppf = '{{--dpid not "%s"}}' % dpid
    return '%s%s*' % (traffic_non_dpid_ppf, non_dpid_ppf)

def max_path_length(n):
    '''generate PPF to detect paths exceeding max length'''
    return '.{%d}.*' % (n+1)

