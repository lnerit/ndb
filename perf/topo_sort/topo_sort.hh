#ifndef TOPO_SORT_HH
#define TOPO_SORT_HH

#include <string>
#include <vector>
#include <set>
#include <cstdlib>
#include <ctime>
#include <crafter.h>

#define nelem(x) (sizeof(x)/sizeof((x)[0]))

using namespace std;
using namespace Crafter;

/* This is the basic unit we match against. */
struct Postcard
{
	int len;
	char pkt[128];
	int dpid;
	int inport;
	int version;
	Postcard *prev;
	Postcard *next;
};


void make_chain_topo(int chain_len);
Postcard *gen_chain_pcards(int chain_len);
void shuffle(vector<Postcard*> &hist);
void topo_sort(const vector<Postcard*> &hist);
void print_path(const vector<Postcard*> &path);

#endif
