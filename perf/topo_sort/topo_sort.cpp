#include "topo_sort.hh"

using namespace std;
using namespace Crafter;
#include <algorithm>
#include <jansson.h>
#include <getopt.h>

//command line options
char *outfile = NULL;
int ITERS = 1000000;
int chain_lens[] = {2, 4, 8, 16, 32, 64};

static struct option long_options[] =
{
    {"outfile", required_argument, NULL, 'o'},
    {"iters", required_argument, NULL, 'n'},
    {NULL, 0, NULL, 0}
};

#define MAX_DPIDS 100
#define MAX_PORTS 100
int topo[MAX_DPIDS+1][MAX_DPIDS+1] = { { 0 } };

int get_neighbor(int dpid, int port)
{
        return topo[dpid][port];
}

void set_neighbor(int dpid, int port, int nbr)
{
        topo[dpid][port] = nbr;
}

void
make_chain_topo(int chain_len)
{
    for(int i = 0; i < chain_len - 1; i++) {
        int src = i+1;
        int dst = i+2;
        int outport = 2;
        int inport = 1;
        set_neighbor(src, outport, dst);
        set_neighbor(dst, inport, src);
    }
}

inline void set_null_postcard(Postcard *p)
{
    p->len = 0;
}

Postcard*
gen_chain_pcards(int chain_len)
{
    Postcard *postcards = (Postcard *) malloc(sizeof(Postcard)*(chain_len + 1));
    int i = 0;
    for(i = 0; i < chain_len; i++) {
        Postcard *pcard = &postcards[i];
        pcard->dpid = i+1;
        pcard->inport = 1;
        pcard->version = 1;

        /* Create an ethernet header */
        Ethernet eth_header;
        eth_header.SetDestinationMAC("00:00:00:00:00:00:02");
        eth_header.SetSourceMAC("00:00:00:00:00:00:01");
        eth_header.SetType(0x800);

        /* Create an IP header */
        IP ip_header;
        /* Set the Source and Destination IP address */
        ip_header.SetSourceIP("10.0.0.1");
        ip_header.SetDestinationIP("10.0.0.2");

        /* Create an TCP - SYN header */
        TCP tcp_header;
        tcp_header.SetSrcPort(11);
        tcp_header.SetDstPort(80);
        tcp_header.SetSeqNumber(RNG32());
        tcp_header.SetFlags(TCP::SYN);

        /* A raw layer, this could be any array of bytes or chars */
        RawLayer payload("ArbitraryPayload");

        /* Create a packets */
        Packet tcp_packet = eth_header / ip_header / tcp_header / payload;
        tcp_packet.PreCraft();

        memcpy(pcard->pkt, tcp_packet.GetBuffer(), tcp_packet.GetSize());
        pcard->len = tcp_packet.GetSize();
    }

    // set the last postcard to null
    set_null_postcard(&postcards[i]);

    return postcards;
}

void
shuffle(vector<Postcard*> &hist)
{
    srand(time(NULL));
    for(unsigned int i = 0; i < hist.size(); i++) {
        int k = hist.size() - i;
        int j = i + (rand() % k);

        Postcard *tmp = hist[j];
        hist[j] = hist[i];
        hist[i] = tmp;
    }
}

Postcard *
topo_sort_fast(Postcard *hist, int len)
{
	Postcard *locs[MAX_DPIDS];
	bzero(locs, sizeof(locs));

	Postcard *curr = hist;
	for (int i = 0; i < len; ++i) {
		curr->prev = NULL;
		curr->next = NULL;
		if (locs[curr->dpid]) {
			fprintf(stderr, "Loop detected.  Can't do toposort with cycles.\n");
			assert(false);
		}
		locs[curr->dpid] = hist + i;
		curr++;
	}

	int i = 0;
	curr = hist;
	while (i < len) {
		int nbr = get_neighbor(curr->dpid, curr->inport);
		Postcard *prev = locs[nbr];
		curr->prev = prev;
		if (prev)
			prev->next = curr;
		i++;
		curr++;
	}

	i = 0;
	curr = hist;
	while (i < len) {
		if (curr->prev == NULL)
			return curr;
		curr++;
		i++;
	}

/* Should not come here... or there is a loop? */
	return NULL;
}

void
topo_sort(const vector<Postcard*> &hist, vector<Postcard*> &path)
{
    //vector<Postcard*> path;
    path.reserve(hist.size());

    Postcard* pcard_table[MAX_DPIDS+1];
    // set<int> dpid_lst;
    int dpid_lst[MAX_DPIDS+1];
    int dpid_lst_size = 0;

    bzero(dpid_lst, sizeof(dpid_lst));

    for(unsigned int i = 0; i < hist.size(); i++) {
        int dpid = hist[i]->dpid;
        pcard_table[dpid] = hist[i];
        //dpid_lst.insert(dpid);
	dpid_lst[dpid] = 1;
	dpid_lst_size++;
    }

    int dpid = 0;
    while(dpid_lst_size > 0) {
	    vector<Postcard*> path_segment;

	// Vimal: Nikhil, why do you start with the smallest dpid?
/*
        set<int>::iterator sit = dpid_lst.begin();
        int dpid = *sit;
        dpid_lst.erase(sit);
        dpid_lst_size--;
*/
	while (dpid_lst[dpid] == 0) dpid++;
	dpid_lst_size--;
        Postcard *pcard = pcard_table[dpid];
        path_segment.push_back(pcard);

        while(true) {
            dpid = pcard->dpid;
            int inport = pcard->inport;
            int nbr = get_neighbor(dpid, inport);

	    if (dpid_lst[nbr]) {
		    Postcard *prev_pcard = pcard_table[nbr];
		    // path_segment.push_back(prev_pcard);
		    path_segment.push_back(prev_pcard);
		    dpid_lst[nbr] = 0;
	    } else {
		    break;
	    }
/*
            set<int>::iterator nit = dpid_lst.find(nbr);
            if(nit != dpid_lst.end()) {
                Postcard *prev_pcard = pcard_table[nbr];
                path_segment.insert(path_segment.begin(), prev_pcard);
                dpid_lst.erase(nit);
                dpid_lst_size--;
                pcard = prev_pcard;
            }
            else
                break;
*/
        }
        path.insert(path.end(), path_segment.begin(), path_segment.end());
    }
}

void
print_path(const vector<Postcard*> &path)
{
    for(unsigned int i = 0; i < path.size(); i++) {
        Postcard *p = path[i];
        printf("-> (%d) %d ", p->inport, p->dpid);
    }
    printf("\n");
}

void
print_path_arr(Postcard *path, int len)
{
	Postcard *p = path;
	for(int i = 0; i < len; i++) {
		printf("-> (%d) %d ", p->inport, p->dpid);
		p++;
	}
	printf("\n");
}

void
print_path_from(Postcard *p)
{
	printf("Sorted path: ");
	while (p) {
		printf("-> (%d) %d ", p->inport, p->dpid);
		p = p->next;
	}
	printf("\n");
}

int 
main(int argc, char **argv)
{

    // loop over all of the options
    int option_index = 0;
    int ch;
    printf("Parsing arguments...\n");
    while ((ch = getopt_long(argc, argv, "o:n:", 
                    long_options, &option_index)) != -1) {
        switch (ch) {
            case 'o':
                outfile = optarg;
                break;
            case 'n':
                ITERS = atoi(optarg);
                break;
            default:
                abort ();
        }
    }
    printf("Done parsing arguments.\n");

    json_t *result = json_object();
    json_t *sort_val = json_object();
    json_object_set_new(result, "topo_sort_fast", sort_val);
    
    for(int j = 0; j < (int)nelem(chain_lens); j++) {
        int clen = chain_lens[j];

        char clen_str[10];
        sprintf(clen_str, "%d", clen);
        json_t *chain_len_val = json_object();
        json_object_set_new(sort_val, clen_str, chain_len_val);

        printf("Sorting randomly shuffled histories of length %d, %d times...\n", clen, ITERS);
        struct timeval start, end;
        Postcard *p = gen_chain_pcards(clen);
        make_chain_topo(clen);

        Postcard pcards[clen];
        for(int i = 0; i < clen ; i++) {
            pcards[i] = p[i];
        }

        double total_t = 0;

        for(int i = 0; i < ITERS; i++) {
            std::random_shuffle(pcards, pcards + clen);
            //print_path_arr(pcards, clen);
            gettimeofday(&start, NULL);
            // topo_sort(pcards, sorted);
            topo_sort_fast(pcards, clen);
            //Postcard *head = topo_sort_fast(pcards, clen);
            gettimeofday(&end, NULL);
            //print_path_from(head);
            total_t += ((end.tv_sec - start.tv_sec)*1e6 + (end.tv_usec - start.tv_usec));
        }

        printf("\n");
        printf("Time per sort: %.6fns\n", total_t * 1e3 /ITERS);
        json_object_set_new(chain_len_val, "median", json_real(total_t/(ITERS*1e6)));
        free(p);
    }

    char *result_str; 
    result_str = json_dumps(result, JSON_INDENT(4));
    printf("%s\n", result_str);
    free(result_str);
    if(outfile)
        json_dump_file(result, outfile, JSON_INDENT(4));

    json_decref(result);
    return 0;
}
