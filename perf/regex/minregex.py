#import pcapy
import shlex
import minbpf

'''
A simple regex-engine built from scratch to match packet histories (PH) against
packet history filters (PHF).

The engine currently supports only simple non-regex-based matches for each field in a
postcard.

The engine uses a simple non-backtracking algorithm.
Complexity: O(m*n), where 
m is the number of atomic elements in the regex
n is the length of the input string
Source Code Credits: https://bitbucket.org/cfbolz/minregex
'''

class Regex(object):
    def __init__(self, empty):
        # empty denotes whether the regular expression
        # can match the empty string
        self.empty = empty
        # mark that is shifted through the regex
        self.marked = False

    def reset(self):
        """ reset all marks in the regular expression """
        self.marked = False

    def shift(self, c, mark):
        """ shift the mark from left to right, matching character c."""
        # _shift is implemented in the concrete classes
        marked = self._shift(c, mark)
        self.marked = marked
        return marked

    def match(self, s):
        if not s or len(s) == 0:
            return self.empty
        # shift a mark in from the left
        result = self.shift(s[0], True)
        for c in s[1:]:
            # shift the internal marks around
            result = self.shift(c, False)
        self.reset()
        return result

class Char(Regex):
    def __init__(self, c):
        Regex.__init__(self, False)
        self.c = c

    def _shift(self, c, mark):
        # handle '.' which matches any char
        return mark and (self.c == '.' or c == self.c)

class Postcard(Regex):
    def __init__(self, pf_str='.'):
        Regex.__init__(self, False)
        self.pf_str = pf_str
        self.pf_lst = shlex.split(self.pf_str)

        self.parse_pf_list()
        self.compile()

    def _shift(self, pcard, mark):
        # handle '.' which matches any pcard
        if self.pf_str == '.':
            return mark
        else:
            # Match bpf
            self.bpf_m = False
            if not self.bpf:
                self.bpf_m = not self.bpf_negate
            else:
                if('pkt_str' in pcard):
#                    self.bpf_m = self.bpf.filter(pcard['pkt_str']) != 0
                    self.bpf_m = self.bpf.filter(pcard['pkt_str']) > 0
                    if self.bpf_negate:
                        self.bpf_m = not self.bpf_m
                elif('pkt' in pcard):
                    pkt_str = pcard['pkt'].pack()
#                    self.bpf_m = self.bpf.filter(pkt_str) != 0
                    self.bpf_m = self.bpf.filter(pkt_str) > 0
                    if self.bpf_negate:
                        self.bpf_m = not self.bpf_m

            # Match dpid
            self.dpid_m = False
            if not self.dpid_filter:
                self.dpid_m = not self.dpid_negate
            else:
                if('dpid' in pcard):
                    self.dpid_m = (self.dpid_filter == repr(pcard['dpid']))
                    if self.dpid_negate:
                        self.dpid_m = not self.dpid_m

            # Match inport
            self.inport_m = False
            if not self.inport_filter:
                self.inport_m = not self.inport_negate
            else:
                if('inport' in pcard):
                    self.inport_m = (self.inport_filter == repr(pcard['inport']))
                    if self.inport_negate:
                        self.inport_m = not self.inport_m

            # Match outport
            self.outport_m = False
            if not self.outport_filter:
                self.outport_m = not self.outport_negate
            else:
                if('outport' in pcard):
                    self.outport_m = (self.outport_filter ==
                            repr(pcard['outport']))
                    if self.outport_negate:
                        self.outport_m = not self.outport_m
                        
            # Match version
            self.version_m = False
            if not self.version_filter:
                self.version_m = not self.version_negate
            else:
                if('version' in pcard):
                    self.version_m = (self.version_filter ==
                            repr(pcard['version']))
                    if self.version_negate:
                        self.version_m = not self.version_m

            pcard_match = (self.bpf_m and self.dpid_m and self.inport_m and self.outport_m and self.version_m)
            return mark and pcard_match

    def get_filter(self, filter_type):
        '''get the filter string, or raise an exception if something fails'''
        try:
            i = self.pf_lst.index('%s' % filter_type)
            negate = False
            filter_str = self.pf_lst[i + 1]
            if filter_str == 'not':
                filter_str = self.pf_lst[i + 2]
                negate = True
            return (filter_str, negate)
        except Exception as e:
            raise e

    def parse_pf_list(self):
        # bpf
        try:
            (self.pkt_filter, self.bpf_negate) = self.get_filter('--bpf')
            if 'ether dst' in self.pkt_filter:
                raise Exception('BPF in PacketFilter cannot contain \'ether dst\'')
        except ValueError:
            self.pkt_filter = None
            self.bpf_negate = False


        # dpid
        try:
            self.dpid_filter, self.dpid_negate = self.get_filter('--dpid')
        except ValueError:
            self.dpid_filter = None
            self.dpid_negate = False

        # inport
        try:
            self.inport_filter, self.inport_negate = self.get_filter('--inport')
        except ValueError:
            self.inport_filter = None
            self.inport_negate = False

        # outport
        try:
            self.outport_filter, self.outport_negate = self.get_filter('--outport')
        except ValueError:
            self.outport_filter = None
            self.outport_negate = False

        # version
        try:
            self.version_filter, self.version_negate = self.get_filter('--version')
        except ValueError:
            self.version_filter = None
            self.version_negate = False

    def compile(self):
        self.bpf = None
        if self.pkt_filter:
#            self.bpf = pcapy.compile(pcapy.DLT_EN10MB, 1500, self.pkt_filter, 0, 1)
            self.bpf = minbpf.bpf()
            self.bpf.compile(self.pkt_filter)

class Epsilon(Regex):
    def __init__(self):
        Regex.__init__(self, empty=True)

    def _shift(self, c, mark):
        return False

class Binary(Regex):
    def __init__(self, left, right, empty):
        Regex.__init__(self, empty)
        self.left = left
        self.right = right

    def reset(self):
        self.left.reset()
        self.right.reset()
        Regex.reset(self)

class Alternative(Binary):
    def __init__(self, left, right):
        empty = left.empty or right.empty
        Binary.__init__(self, left, right, empty)

    def _shift(self, c, mark):
        marked_left  = self.left.shift(c, mark)
        marked_right = self.right.shift(c, mark)
        return marked_left or marked_right


class Repetition(Regex):
    def __init__(self, re):
        Regex.__init__(self, True)
        self.re = re

    def _shift(self, c, mark):
        return self.re.shift(c, mark or self.marked)

    def reset(self):
        self.re.reset()
        Regex.reset(self)

class Sequence(Binary):
    def __init__(self, left, right):
        empty = left.empty and right.empty
        Binary.__init__(self, left, right, empty)

    def _shift(self, c, mark):
        old_marked_left = self.left.marked
        marked_left = self.left.shift(c, mark)
        marked_right = self.right.shift(
            c, old_marked_left or (mark and self.left.empty))
        return (marked_left and self.right.empty) or marked_right

def packet_path_match(ppf, pcard_lst):
    '''Wrapper function useful for analysis'''
    return ppf.match(pcard_lst)

if __name__ == '__main__':
    import unittest
    import ndb.topo_sort as topo_sort

    def create_pp(chain_len):
        pcards, psid_to_dpid, topo = topo_sort.gen_chain_pcards(chain_len)
        return pcards

    class PacketFilterTestCase(unittest.TestCase):

        def setUp(self):
            chain_len = 3
            self.pp = create_pp(chain_len)
            self.pcard = self.pp[0]

        def tearDown(self):
            pass

        def test_pcap_parse(self):
            self.assertIs(type(self.pcard), dict, "pcard isn't of type dict")

        def test_null_pf(self):
            self.assertTrue(Postcard('').match([self.pcard]), 
                    "Pcard doesn't match NULL filter")

        def test_ip_pf(self):
            self.assertTrue(Postcard('--bpf ip').match([self.pcard]), 
                    "Pcard doesn't match IP filter")

    class PacketPathFilterTestCase(unittest.TestCase):

        def setUp(self):
            self.chain_len = 3
            self.pp = create_pp(self.chain_len)

        def tearDown(self):
            pass

        def test_pp_parse(self):
            self.assertIs(type(self.pp), list, "PacketPath isn't of type list")

        def test_null_ppf(self):
            self.assertFalse(Regex(empty=False).match([]), 
                    "PacketPath matches empty regex")

        def test_dot_ppf(self):
            self.assertFalse(Postcard('.').match(self.pp), 
                    "PacketPath matches DOT regex")

        def test_ip_ppf(self):
            n = self.chain_len
            ppf = Sequence(Sequence(Postcard('--dpid 1 --inport 1'), 
                Repetition(Postcard('.'))),
                Postcard('--dpid %d --inport 2' % n))
            self.assertTrue(ppf.match(self.pp), 
                    "PacketPath doesn't match ICMP filter")
    unittest.main()
