#!/usr/bin/env python
"""Sweep across different sizes for the chain topology."""

import os
import sys
from collections import OrderedDict
import time

import ndb.topo_sort as topo_sort
from common_analysis import Analysis
from stat_fcns import STAT_FCNS

import regex.minregex as minre
from regex_match_chain import us_str
from regex_match_chain import gen_ppf_str, gen_minre
from regex_match_chain import DPI
import regex_match_chain

from multiprocessing import Process, JoinableQueue, Event


DEF_DATA_EXT = 'latparallel'  # dict of sizes to latencies

DEF_CHAIN_LENS = [2 ** i for i in range(1, 6)] + [60]
DEF_ITERS = 100
IGN_ITERS = 10 #no. of initial iterations to ignore (to get pypy to "warm up")
DEF_JOBS_PER_ITER = 64
DEF_BATCH_SIZE = 10000
DEF_STATS_LST = STAT_FCNS.keys()

# Data analysis fcns: each benchmarks a scenario.
DATA_ANALYSIS_FCNS = {
    'thread_sweep': (lambda options: chain_sweep_parallel(options)),
}
ALL_DATA_ANALYSIS_NAMES = DATA_ANALYSIS_FCNS.keys()

NO_LEGEND = False

prog = None
is_pypy = regex_match_chain.is_pypy
if is_pypy:
    prog = 'pypy'
else:
    import ndb.filter as filter
    prog = 'python'

MATCH_FCNS = OrderedDict([
    ('minre_match', minre.packet_path_match),
    ])
PPF_TYPES = regex_match_chain.PPF_TYPES
#PPF_TYPES = ['.*X$',]

DEF_THREADS = [1, 2, 4, 8]

class RegexMatchChainSweepAnalysis(Analysis):

    def plot_data(self, plot_dir, data, show_plot, save_plot, normalize):
        from ndb.analysis.plot_defaults import apply_time_series_plot_defaults
        from ndb.analysis.plot_bar_graph import plot_data_bar
        import matplotlib.pyplot as plt
        from ndb.plot.plot_helper import plotTimeSeries
        from ndb.plot.plot_helper import linestyleGenerator, colorGenerator

        apply_time_series_plot_defaults()
        lsgen = linestyleGenerator()

        # data[analysis_name][match_fcn_name][ppf_type][chain_len][num_threads][stat] ...
        analysis_name = 'thread_sweep'
        analysis_data = data[analysis_name]
        match_fcn_name = 'minre_match'
        match_fcn_data = analysis_data[match_fcn_name]
        metric = 'median'

        plot_ext = self.plot_ext
        group_order = match_fcn_data.keys()
        legend_title = 'Threads'
        ylabel_fcn = lambda x: 'PHF Match Throughput (million matches/sec)'

        data_series = {}
        # swap the data
        for ppf_type, ppf_data in match_fcn_data.iteritems():
            for chain_len, chain_len_data in ppf_data.iteritems():
                chain_len_int = int(chain_len)
                if chain_len_int not in data_series:
                    data_series[chain_len_int] = {}
                data_series[chain_len_int][ppf_type] = \
                        match_fcn_data[ppf_type][chain_len]

        for chain_len in sorted(data_series.keys()):
            chain_len_data = data_series[chain_len]

            print '-'*50
            print 'Plotting Match Throughput for chain length %d' % chain_len

            # plot_data[num_threads][ppf_type]
            plot_data = OrderedDict()

            sorted_threads = sorted(map(int, chain_len_data.itervalues().next().keys()))
            subgroup_order = sorted_threads
            for ppf_type in group_order:
                if ppf_type in chain_len_data:
                    ppf_data  = chain_len_data[ppf_type]
                else:
                    ppf_data = {}
                plot_data[ppf_type] = OrderedDict()
                for threads in sorted_threads:
                    if str(threads) in ppf_data:
                        plot_data[ppf_type][threads] = \
                                1./(1e6 * ppf_data[str(threads)][metric])
                    else:
                        plot_data[ppf_type][threads] = 0

            plot_data_bar(plot_dir, plot_data, show_plot, save_plot, group_order,
                          subgroup_order, 'chain_len_%d' % chain_len, 
                          legend_title = legend_title, 
                          xlabel = 'Packet History Filter Type', 
                          ylabel_fcn = ylabel_fcn,
                          axis_left = 0.05, axis_right = 0.95, axis_bottom = 0.25,
                          group_name_map = {},
                          subgroup_name_map = {})


    def process_options(self, options):
        options = super(RegexMatchChainSweepAnalysis, self).process_options(options)
        # Convert from comma-sep string into list
        if options.chain_lens:
            options.chain_lens = options.chain_lens.split(',')
            options.chain_lens = [int(i) for i in options.chain_lens]
        else:
            options.chain_lens = DEF_CHAIN_LENS

        if options.threads:
            options.threads = options.threads.split(',')
            options.threads = map(int, options.threads)
        else:
            options.threads = DEF_THREADS

        return options

    def parse_args(self):
        """Parse options"""
        opts = self.get_options()
        opts.add_option("--chain_lens", type = 'str',
                        default = None,
                        help = "comma-sep list of chain lens")
        opts.add_option("--iters", type = 'int',
                        default = DEF_ITERS,
                        help = "number of iterations per test")
        opts.add_option("--threads", type = 'str',
                        default = None,
                        help = "comma-sep list of no. of threads")
        opts.add_option("--stats", type = 'str',
                        default = DEF_STATS_LST,
                        action = 'callback',
                        callback = self.str_list_callback,
                        help = "Stats to produce results for")
        opts.add_option("--match_fcns", type = 'str',
                        default = MATCH_FCNS,
                        action = 'callback',
                        callback = self.str_list_callback,
                        help = "Stats to produce results for")
        options, arguments = opts.parse_args()
        options = self.process_options(options)
        return options


def chain_sweep_parallel(options):

    class Worker(Process):

        def __init__(self, match_fcn, ppf, pcards, queue):
            super(Worker, self).__init__()
            self.match_fcn = match_fcn
            self.ppf = ppf
            self.pcards = pcards
            self.queue = queue
            self.run_job = Event()
            self.exit = Event()

        def shutdown(self):
            self.exit.set()

        def run(self):
            while True:
                try:
                    batch_size = self.queue.get_nowait()
                    self.run_job.wait()
                    for i in xrange(batch_size):
                        self.match_fcn(self.ppf, self.pcards)
                    self.queue.task_done()
                except:
                    if self.exit.is_set():
                        return
                    time.sleep(0.005)


    data = {}

    for match_fcn_name in options.match_fcns:
        match_fcn = MATCH_FCNS[match_fcn_name]
        match_fcn_key = match_fcn_name

        data[match_fcn_key] = {}
        
        for ppf_type in PPF_TYPES:
            data[match_fcn_key][ppf_type] = OrderedDict({})

            for chain_len in options.chain_lens:
                if ppf_type == 'X.*X.*X$' and chain_len < 3:
                    continue

                data[match_fcn_key][ppf_type][chain_len] = OrderedDict({})

                ppf_str = gen_ppf_str(ppf_type, chain_len)
                pcards, psid_to_dpid, topo = topo_sort.gen_chain_pcards(chain_len)
                ppf = None
                if 'minre_match' in match_fcn_name:
                    ppf = gen_minre(ppf_type, chain_len)
                else:
                    if not is_pypy:
                        if 'old_match' in match_fcn_name:
                            ppf = filter.PacketPathFilter(ppf_str, open(os.path.devnull, 'w'),
                                    syntax_check=False)

                print "Running %s w/len = %s for %s iters" % (match_fcn_name,
                        chain_len, options.iters)
                print "PPF TYPE: %s" % ppf_type
                print "PPF: %s" % ppf_str

                for num_threads in options.threads:
                    print '-'*5, num_threads, 'Threads', '-'*5

                    # queue of jobs
                    queue = JoinableQueue()

                    # Create the worker processes
                    data[match_fcn_key][ppf_type][chain_len][num_threads] = {}
                    vals = []
                    workers = []

                    for i in range(num_threads):
                        w = Worker(match_fcn, ppf, pcards, queue)
                        workers.append(w)

                    for w in workers:
                        w.start()

                    for i in range(options.iters):

                        # populate the queue
                        jobs_per_iter = num_threads
                        for j in range(jobs_per_iter):
                            queue.put(DEF_BATCH_SIZE)

                        time.sleep(0.05)

                        start_t = time.time()
                        # signal the workers to start
                        for w in workers:
                            w.run_job.set()
                        queue.join()
                        delta = (time.time() - start_t)/(jobs_per_iter * DEF_BATCH_SIZE)
                        vals.append(delta)

                        # signal the workers to stop
                        for w in workers:
                            w.run_job.clear()

                    for w in workers:
                        w.shutdown()
                        w.join()

                    vals = vals[IGN_ITERS:]
                    for stat in options.stats:
                        fcn = STAT_FCNS[stat]
                        stat_val = fcn(vals)
                        print "%s: %s" % (stat, us_str(stat_val))
                        data[match_fcn_key][ppf_type][chain_len][num_threads][stat] = stat_val
                    #print 'Median match time: %s' % us_str(sorted(vals)[len(vals)/2])

    return data

if __name__ == '__main__':
    RegexMatchChainSweepAnalysis(def_analyses = ALL_DATA_ANALYSIS_NAMES,
                              data_analysis_fcns = DATA_ANALYSIS_FCNS,
                              def_data_ext = DEF_DATA_EXT)
