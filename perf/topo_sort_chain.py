#!/usr/bin/env python
"""Simple performance-testing script for chain topology."""

import copy
import time
import random
import logging

import os

import ndb
from ndb.topology import NDBTopoFastSort
import ndb.topo_sort as topo_sort
from common_analysis import Analysis
from stat_fcns import STAT_FCNS

lg = logging.getLogger("topo_sort_chain")


DEF_DATA_EXT = 'lats'  # latencies

DEF_CHAIN_LEN = 255
DEF_ITERS = 100


# Data analysis fcns: each benchmarks a scenario.
DATA_ANALYSIS_FCNS = {
    'chain': (lambda options: chain_analysis_all(options)),
}
ALL_DATA_ANALYSIS_NAMES = DATA_ANALYSIS_FCNS.keys()


SORT_FCNS = {
    'fast': topo_sort.topo_sort_fast,
    #'original': topo_sort.topo_sort,   # too slow.  commented out.
    'clean': topo_sort.topo_sort_clean_fasttopo,
    'namedtuple': topo_sort.topo_sort_clean_namedtuple,
}

DPI = 300


class TopoSortChainAnalysis(Analysis):

    def plot_data(self, plot_dir, data, show_plot, save_plot, normalize):
        from ndb.analysis.plot_defaults import apply_cdf_plot_defaults
        import matplotlib.pyplot as plt
        from ndb.plot.plot_helper import plotCDF

        apply_cdf_plot_defaults()

        for analysis_name, analysis_data in data.iteritems():

            plot_data_series = []
            for sort_name, sort_data in analysis_data.iteritems():
                yvals = [x * 1e6 for x in sort_data]
                plot_data_series.append({
                    'label': sort_name,
                    'y': yvals
                })

            fig = plotCDF(data = plot_data_series,
                    title = '%s latency' % analysis_name,
                    xlabel = 'latency (us)',
                    ylabel = 'fraction',
                    step = False,
                    xscale = 'log')
        
            if save_plot:
                if not os.path.exists(plot_dir):
                    os.makedirs(plot_dir)
                filepath = os.path.join(plot_dir, analysis_name + '.' + self.plot_ext)
                print "Writing file to %s" % filepath
                fig.savefig(filepath, dpi = DPI)
            if show_plot:
                plt.show()
    
            plt.close(fig)


    def parse_args(self):
        """Parse options"""
        opts = self.get_options()
        opts.add_option("--chain_len", type = 'int',
                        default = DEF_CHAIN_LEN,
                        help = "chain len")
        opts.add_option("--iters", type = 'int',
                        default = DEF_ITERS,
                        help = "number of iterations per test")
        options, arguments = opts.parse_args()
        options = self.process_options(options)
        return options


def run_test_random(chain_len, sort_fcn, iters):
    '''Randomly ordered postcards.'''
    pcards, psid_to_dpid, topo = topo_sort.gen_chain_pcards(chain_len)
    deltas = []
    pcards_list = []  # list of pcard lists for sorting, each a new order.
    for i in range(iters):
        random.shuffle(pcards)
        pcards_list.append(copy.copy(pcards))
    
    for i in range(iters):
        #print topo_sort.get_dpids(pcards)
        if 'fasttopo' in sort_fcn.__name__:
            topo2 = NDBTopoFastSort(topo)
            start = time.time()
            sorted_pcards = sort_fcn(pcards_list[i], psid_to_dpid, topo2,
                                     safe = False, ft_db = None, logger = lg)
            delta = time.time() - start
        elif 'namedtuple' in sort_fcn.__name__:
            topo2 = NDBTopoFastSort(topo)
            pcards_list2 = topo_sort.namedtuple_pcard_list(pcards_list[i], psid_to_dpid)
            start = time.time()
            sorted_pcards = sort_fcn(pcards_list2, psid_to_dpid, topo2,
                                     safe = False, ft_db = None, logger = lg)
            delta = time.time() - start
            sorted_pcards = topo_sort.dict_pcard_list(sorted_pcards)
        else:
            start = time.time()
            sorted_pcards = sort_fcn(pcards_list[i], psid_to_dpid, topo,
                                     safe = False, ft_db = None, logger = lg)
            delta = time.time() - start

        dpids = topo_sort.get_dpids(sorted_pcards)
        assert dpids == sorted(dpids)
        # If using a destructive sort with shallow copies, screws up numbers.
        assert len(sorted_pcards) > 0
        deltas.append(delta)
    return deltas


def us_str(fraction, precision = 3):
    format_str = "%" + "0.%sf us" % str(precision)
    return format_str % (fraction * 1e6)


def chain_analysis_single(name, sort_fcn, chain_len, iters):
    print "Running %s w/len = %s for %s iters" % (name, chain_len, iters)
    times = run_test_random(chain_len, sort_fcn, iters)
    for name, fcn in STAT_FCNS.iteritems():
        print "%s: %s" % (name, us_str(fcn(times)))
    return times


def chain_analysis_all(options):
    # master dict w/all data.
    data = {}
    for name, sort_fcn in SORT_FCNS.iteritems():
        data[name] = chain_analysis_single(name, sort_fcn, options.chain_len,
                                           options.iters)
    return data


if __name__ == '__main__':
    TopoSortChainAnalysis(def_analyses = ALL_DATA_ANALYSIS_NAMES,
                          data_analysis_fcns = DATA_ANALYSIS_FCNS,
                          def_data_ext = DEF_DATA_EXT)
