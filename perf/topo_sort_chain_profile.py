#!/usr/bin/env python
"""Simple performance-testing script for chain topology."""

import copy
import time
import random
import logging

from line_profiler import LineProfiler

import ndb.topo_sort as topo_sort
from topo_sort_chain import SORT_FCNS
from ndb.topology import NDBTopoFastSort

lg = logging.getLogger("topo_sort_chain")

DEF_CHAIN_LEN = 16
DEF_ITERS = 1000



def critical_region(sort_fcn, pcards_list, psid_to_dpid, topo, iters):
    '''topo: NDBTopoFastSort'''
    for i in range(iters):
        # If using a destructive sort with shallow copies, screws up numbers.
        assert len(pcards_list[i]) > 0
        #print topo_sort.get_dpids(pcards_copy)
        sorted_pcards = sort_fcn(pcards_list[i], psid_to_dpid, topo,
                                 safe = False, ft_db = None, logger = lg)
        #print topo_sort.get_dpids(sorted_pcards)


def run_test_random(chain_len, sort_fcn, iters):
    '''Randomly ordered postcards.'''
    pcards, psid_to_dpid, topo = topo_sort.gen_chain_pcards(chain_len)
    pcards_list = []  # list of pcard lists for sorting, each a new order.
    for i in range(iters):
        random.shuffle(pcards)
        pcards_list.append(copy.copy(pcards))
    if 'fasttopo' in sort_fcn.__name__:
        topo2 = NDBTopoFastSort(topo)
        critical_region(sort_fcn, pcards_list, psid_to_dpid, topo2, iters)
    elif 'namedtuple' in sort_fcn.__name__:
        topo2 = NDBTopoFastSort(topo)
        pcards_list2 = [topo_sort.namedtuple_pcard_list(pcards, psid_to_dpid) for pcards in pcards_list]
        critical_region(sort_fcn, pcards_list2, psid_to_dpid, topo2, iters)
    else:
        critical_region(sort_fcn, pcards_list, psid_to_dpid, topo, iters)

def do_stuff():
    run_test_random(DEF_CHAIN_LEN, SORT_FCNS['namedtuple'], DEF_ITERS)


if __name__ == '__main__':
    profile = LineProfiler(topo_sort.topo_sort, topo_sort.topo_sort_fast,
                           topo_sort.topo_sort_clean_fasttopo,
                           topo_sort.topo_sort_clean_namedtuple,
                           topo_sort.check_pp_link, critical_region)
    profile.run('do_stuff()')
    #profile.dump_stats('lp_output')
    profile.print_stats()