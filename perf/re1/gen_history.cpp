#include <crafter.h>
extern "C"
{
      #include "regexp.h"
}

/* Collapse namespaces */
using namespace std;
using namespace Crafter;

void set_null_postcard(Postcard *p)
{
    p->len = 0;
}

Postcard *gen_chain_pcards(int chain_len)
{
    Postcard *postcards = (Postcard *) malloc(sizeof(Postcard)*(chain_len + 1));
    int i = 0;
    for(i = 0; i < chain_len; i++) {
        Postcard *pcard = &postcards[i];
        pcard->dpid = i+1;
        pcard->inport = 1;
        pcard->version = 1;

        /* Create an ethernet header */
        Ethernet eth_header;
        eth_header.SetDestinationMAC("00:00:00:00:00:00:02");
        eth_header.SetSourceMAC("00:00:00:00:00:00:01");
        eth_header.SetType(0x800);

        /* Create an IP header */
        IP ip_header;
        /* Set the Source and Destination IP address */
        ip_header.SetSourceIP("10.0.0.1");
        ip_header.SetDestinationIP("10.0.0.2");

        /* Create an TCP - SYN header */
        TCP tcp_header;
        tcp_header.SetSrcPort(11);
        tcp_header.SetDstPort(80);
        tcp_header.SetSeqNumber(RNG32());
        tcp_header.SetFlags(TCP::SYN);

        /* A raw layer, this could be any array of bytes or chars */
        RawLayer payload("ArbitraryPayload");

        /* Create a packets */
        Packet tcp_packet = eth_header / ip_header / tcp_header / payload;
        tcp_packet.PreCraft();

        memcpy(pcard->pkt, tcp_packet.GetBuffer(), tcp_packet.GetSize());
        pcard->len = tcp_packet.GetSize();
    }

    // set the last postcard to null
    set_null_postcard(&postcards[i]);

    return postcards;
}

void
print_history(Postcard *p)
{
    if(p == NULL)
        return;
    while(!eo_postcard(p)) {
        printf("---------------------------------------------\n");
        printf("len: %d\n", p->len);
        char *hex = (char *)malloc(sizeof(char) * (2 * p->len + 1));
        hexify_packet(p->pkt, hex, p->len);
        printf("hex: %s\n", hex);
        free(hex);
        printf("dpid: %d\n", p->dpid);
        printf("inport: %d\n", p->inport);
        printf("version: %d\n", p->version);
        printf("---------------------------------------------\n");
        p++;
    }
}

/*
int 
main(int argc, char **argv)
{
    Postcard *sub[MAXSUB];

    char *ppf_str = argv[1];
    Regexp *re;
    Prog *prog;
    int chain_len = 10;
    int ret;
    Postcard *inp;
    re = parse(ppf_str);
    parse_postcard_filters(re);
    printf("Parse tree for regex\n");
    printre(re);
    printf("\n");
    prog = compile(re);

    inp = gen_chain_pcards(chain_len);
    memset(sub, 0, sizeof sub);
    print_history(inp);
    ret = recursiveprog(prog, inp, sub, nelem(sub));

    if (!ret) {
        printf("-no match-\n");
    }
    else {
        printf("match\n");
    }
    return 0;
}*/
