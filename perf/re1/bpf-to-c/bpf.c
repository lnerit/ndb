
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>
#include <pcap.h>
#include <stdarg.h>

int compile_to_bpf(char *filter, struct bpf_program *prog) {
	int ret;
	ret = pcap_compile_nopcap(1500, DLT_EN10MB, prog,
				  filter, 1, 0);

	return ret;
}

static int ipointer = 0;
int BIP() {
	return ipointer;
}
int BIPNext() {
	return ipointer++;
}

int EmitCode(char *fmt, ...) {
	int ret;
	va_list args;
	va_start(args, fmt);
	ret = vprintf(fmt, args);
	va_end(args);
	printf("\n");
	return ret;
}

void output_load(struct bpf_insn *ins) {
	switch(ins->code) {
		case BPF_LD|BPF_W|BPF_ABS:
		EmitCode("A = get_u32(pkt + %d);", ins->k);
		break;
		
		case BPF_LD|BPF_H|BPF_ABS:
		EmitCode("A = get_u16(pkt + %d);", ins->k);
		break;
		
		case BPF_LD|BPF_B|BPF_ABS:
		EmitCode("A = pkt[%d];", ins->k);
		break;
		
		case BPF_LD|BPF_W|BPF_LEN:
		EmitCode("A = wirelen;");
		break;
		
		case BPF_LDX|BPF_W|BPF_LEN:
		EmitCode("X = wirelen;");
		break;
		
		case BPF_LD|BPF_W|BPF_IND:
		EmitCode("A = get_u32(pkt[X + %d]);", ins->k);
		break;
		
		case BPF_LD|BPF_H|BPF_IND:
		EmitCode("A = get_u16(pkt[X + %d]);", ins->k);
		break;
		
		case BPF_LD|BPF_B|BPF_IND:
		EmitCode("A = pkt[X + %d]", ins->k);
		break;
		
		
		case BPF_LDX|BPF_MSH|BPF_B:
		EmitCode("X = (pkt[%d] & 0xf) << 2;");
		break;
		
		case  BPF_LD|BPF_IMM:
		EmitCode("A = %d;", ins->k);
		break;
		
		case BPF_LDX|BPF_IMM:
		EmitCode("X = %d;", ins->k);
		break;
		
		case BPF_LD|BPF_MEM:
		EmitCode("A = mem[%d];", ins->k);
		break;
		
		case BPF_LDX|BPF_MEM:
		EmitCode("X = mem[%d];", ins->k);
		break;
	}
}

void output_store(struct bpf_insn *ins) {
	switch(ins->code) {
		case BPF_ST:
		EmitCode("mem[%d] = A;", ins->k);
		break;
		
		case BPF_STX:
		EmitCode("mem[%d] = X;", ins->k);
		break;
		
		
	}
}

void output_alu(struct bpf_insn *ins) {
	switch(ins->code) {
		case BPF_ALU|BPF_ADD|BPF_X:
		EmitCode("A += X;");
		break;
		
		case BPF_ALU|BPF_SUB|BPF_X:
		EmitCode("A -= X;");
		break;
		
		case BPF_ALU|BPF_MUL|BPF_X:
			EmitCode("A *= X;");
			break;

		case BPF_ALU|BPF_DIV|BPF_X:
			EmitCode("if (X == 0) return 0; A /= X;");
			break;
		
		case BPF_ALU|BPF_AND|BPF_X:
			EmitCode("A &= X;");
			break;

		case BPF_ALU|BPF_OR|BPF_X:
			EmitCode("A |= X;");
			break;

		case BPF_ALU|BPF_LSH|BPF_X:
			EmitCode("A <<= X");
			break;;

		case BPF_ALU|BPF_RSH|BPF_X:
			EmitCode("A >>= X");
			break;

		case BPF_ALU|BPF_ADD|BPF_K:
			EmitCode("A += %d;", ins->k);
			break;

		case BPF_ALU|BPF_SUB|BPF_K:
			EmitCode("A -= %d;", ins->k);
			break;

		case BPF_ALU|BPF_MUL|BPF_K:
			EmitCode("A *= %d;", ins->k);
			break;

		case BPF_ALU|BPF_DIV|BPF_K:
			EmitCode("A /= %d;", ins->k);
			break;

		case BPF_ALU|BPF_AND|BPF_K:
			EmitCode("A &= %d;", ins->k);
			break;

		case BPF_ALU|BPF_OR|BPF_K:
			EmitCode("A |= %d;", ins->k);
			break;

		case BPF_ALU|BPF_LSH|BPF_K:
			EmitCode("A <<= %d;", ins->k);
			break;

		case BPF_ALU|BPF_RSH|BPF_K:
			EmitCode("A >>= %d;", ins->k);
			break;

		case BPF_ALU|BPF_NEG:
			EmitCode("A = -A;");
			break;		
	}
}

void output_jmp(struct bpf_insn *ins) {
	#define JTJF 		EmitCode("JT = %d;", 1+ins->jt); \
					    EmitCode("JF = %d;", 1+ins->jf);

	switch(ins->code) {
		case BPF_JMP|BPF_JA:
		EmitCode("goto *ins[%d];", BIP() + ins->k);
		break;
		
		case BPF_JMP|BPF_JGT|BPF_K:
		JTJF;
		EmitCode("goto *ins[%d + ((A > %d) ? JT : JF)];", BIP(), ins->k);
		break;
		
		case BPF_JMP|BPF_JGE|BPF_K:
		JTJF;
		EmitCode("goto *ins[%d + ((A >= %d) ? JT : JF)];", BIP(), ins->k);
		break;
		
		case BPF_JMP|BPF_JEQ|BPF_K:
		JTJF;
		EmitCode("goto *ins[%d + ((A == %d) ? JT : JF)];", BIP() , ins->k);
		break;
		
		case BPF_JMP|BPF_JSET|BPF_K:
		JTJF;
		EmitCode("goto *ins[%d + ((A & %d) ? JT : JF)];", BIP() , ins->k);
		break;

		/* Repeat same for X */
		case BPF_JMP|BPF_JGT|BPF_X:
		JTJF;
		EmitCode("goto *ins[%d + ((A > X) ? JT : JF)];", BIP());
		break;
		
		case BPF_JMP|BPF_JGE|BPF_X:
		JTJF;
		EmitCode("goto *ins[%d + ((A >= X) ? JT : JF)];", BIP());
		break;
		
		case BPF_JMP|BPF_JEQ|BPF_X:
		JTJF;
		EmitCode("goto *ins[%d + ((A == X) ? JT : JF)];", BIP());
		break;
		
		case BPF_JMP|BPF_JSET|BPF_X:
		JTJF;
		EmitCode("goto *ins[%d + ((A & X) ? JT : JF)];", BIP());
		break;
		
	}
}
void output_ret(struct bpf_insn *ins) {
	switch(ins->code) {
		case BPF_RET|BPF_K:
		EmitCode("return %d;\n", ins->k);
			break;
		
		case BPF_RET|BPF_A:
		EmitCode("return A;\n");
			break;
	}
}
void output_misc(struct bpf_insn *ins) {
	switch(ins->code) {
		case BPF_MISC|BPF_TAX:
		EmitCode("X = A;");
		break;
		
		case BPF_MISC|BPF_TXA:
		EmitCode("A = X;");
		break;
	}
}

void EmitPreamble() {
	printf("#include <stdio.h>\n\
#include <sys/time.h>\n\
#include <pcap.h>\n\
typedef unsigned char u8;\n\
typedef unsigned short u16;\n\
typedef unsigned int u32;\n\n");
}


void EmitBenchmark(char *filter) {
	printf("\n\nvoid x86() {\n\
        u8 byte[100]; \n\
        byte[12] = 0x08;\n\
        byte[13] = 0x00;\n\
        byte[20] = 0x6;\n\
        int repeat = 10000000, N = repeat;\n\
		unsigned long long count=0;\n\
        struct timeval start, end;\n\
        gettimeofday(&start, NULL);\n\
        while(repeat--) {\n\
                count += match(byte);\n\
        }\n\
        gettimeofday(&end, NULL);\n\
        double dt = end.tv_sec - start.tv_sec;\n\
        dt = (dt * 1e6) + (end.tv_usec - start.tv_usec);\n\
        dt *= 1e3; \n\
        printf(\"count %%llu, %%.3lfns\\n\", count, dt/N);\n\
}\n\n");
	
	printf("void bpf() {\n\
        u8 byte[100]; \n\
        byte[12] = 0x08;\n\
        byte[13] = 0x00;\n\
        byte[20] = 0x6;\n\
        int repeat = 10000000, N = repeat;\n\
			unsigned long long count=0;\n\
        struct timeval start, end;\n\
			struct bpf_program program;\n\
			pcap_compile_nopcap(1500, DLT_EN10MB, &program, \n\
						  \"%s\", 1, 0);\n\
        gettimeofday(&start, NULL);\n\
        while(repeat--) {\n\
                count += bpf_filter(program.bf_insns, byte,\n\
                                 100, 100);\n\
        }\n\
        gettimeofday(&end, NULL);\n\
        double dt = end.tv_sec - start.tv_sec;\n\
        dt = (dt * 1e6) + (end.tv_usec - start.tv_usec);\n\
        dt *= 1e3; \n\
        printf(\"count %%llu, %%.3lfns\\n\", count, dt/N);\n\
}", filter);

	printf("\n\nint main() {\n\
		x86();\n\
		bpf();\n\
		return 0;\n\
}\n\
");
}


void compile_to_c(char *filter) {
	struct bpf_program program;
	compile_to_bpf(filter, &program);
	struct bpf_program *prog = &program;

	EmitPreamble();
	EmitCode("static inline u16 get_u16(u8 *ptr) {");
	EmitCode("\treturn ntohs(*(u16 *)ptr);");
	EmitCode("}\n\n");
	
	EmitCode("static inline u32 get_u32(u8 *ptr) {");
	EmitCode("\treturn ntohl(*(u32 *)ptr);");
	EmitCode("}\n\n");
	
	EmitCode("int match(u8 *pkt) {");
	EmitCode("register u32 A = 0, X = 0, JT, JF;");
	EmitCode("static void *ins[]={");
	int i;
	
	for (i = 0; i < prog->bf_len; i++) {
		EmitCode("\t&&label_%d,", i);
	}
	EmitCode("};\n");

	struct bpf_insn *ins = prog->bf_insns;

	for (i = 0; i < prog->bf_len; i++) {
		/*
		printf("filter %s (%d) jt %i jf %i K%i\n",
			    bpf_mnemonics[ins->code],
				ins->code, ins->jt, ins->jf, ins->k);
		ins++;
		continue;
		*/
		unsigned int K = ins->k;
		EmitCode("\nlabel_%d:", BIP());
		switch(BPF_CLASS(ins->code)) {
			case BPF_LD:
			case BPF_LDX:
				output_load(ins);
				break;
				
			case BPF_ST:
			case BPF_STX:
				output_store(ins);
				break;
				
			case BPF_ALU:
				output_alu(ins);
				break;
			
				case BPF_JMP:
				output_jmp(ins);
				break;
				
				case BPF_RET:
				output_ret(ins);
				break;
				
				case BPF_MISC:
				output_misc(ins);
				break;
				
				default:
				printf("Unknown instruction class %d (0x%x)", ins->code, ins->code);
				return;
		}
		ins++;
		BIPNext();
	}
	EmitCode("label_%d: printf(\"Shouldn't reach this point.\\n\");", BIPNext());
	EmitCode("}");
	EmitBenchmark(filter);
	return;
}

int main() {
	char *hex = "9e6b6e3116e397c781871aee08004500002800000000400657a901010101b84868dd13887530deadbeef000000005002ffff66650000";
	char buff[128];
	int i = 0;
	int count = 0;
	int integer = 0;
	char *ptr;

	//compile_to_bpf("ip or tcp or icmp or udp", &program);
	//printf("/* %d BPF instructions */\n", program.bf_len);
	compile_to_c("(net 130.127.8.0/24 or  net 130.127.9.0/24 or  net 130.127.10.0/24 or  net 130.127.11.0/24 or  net 130.127.28.0/24 or  net 130.127.69.0/24 or  net 130.127.60.0/24 or  net 130.127.235.0/24 or  net 130.127.236.0/24 or  net 130.127.237.0/24 or  net 130.127.238.0/24 or  net 130.127.239.0/24 or  net 130.127.240.0/24 or  net 130.127.241.0/24 or  net 130.127.239.0/24 or  net 130.127.201.0/24 or  net 130.127.255.250/32 or  net 130.127.255.251/32 or  net 130.127.255.246/32 or  net 130.127.255.247/32 or  net 130.127.255.248/32 or  net 172.19.3.0/24)");

	//printf("**********************\n\n");
	//bpf_dump(&program, 0);
}
