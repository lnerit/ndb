// Copyright 2007-2009 Russ Cox.  All Rights Reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

#ifndef REGEXP_H
#define REGEXP_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>
#include <pcap.h>
#include "bpf-linux/bpf-jit-linux.h"

#define nil ((void*)0)
#define nelem(x) (sizeof(x)/sizeof((x)[0]))
#define MAX_PF_SIZE 25500
#define MAX_BPF_SIZE 12800

#define BPF_JIT 1

typedef struct Regexp Regexp;
typedef struct Prog Prog;
typedef struct Inst Inst;

struct PostcardFilter;
typedef struct PostcardFilter PostcardFilter;

struct Regexp
{
	int type;
	int n;
	char pf[MAX_PF_SIZE];
	PostcardFilter *pfexp;
	Regexp *left;
	Regexp *right;
};

enum	/* Regexp.type */
{
	Alt = 1,
	Cat,
	Lit,
	Dot,
	Paren,
	Quest,
	Star,
	Plus,
};

Regexp *parse(char*);
Regexp *reg(int type, Regexp *left, Regexp *right);
void printre(Regexp*);
void fatal(char*, ...);
void *mal(int);
void parse_postcard_filters(Regexp *r);

struct Prog
{
	Inst *start;
	int len;
};

struct Inst
{
	int opcode;
	char *pf;
	PostcardFilter *pfexp;
	int n;
	Inst *x;
	Inst *y;
	int gen;	// global state, oooh!
};

enum	/* Inst.opcode */
{
	Char = 1,
	Match,
	Jmp,
	Split,
	Any,
	Save,
};

Prog *compile(Regexp*);
void printprog(Prog*);

extern int gen;

enum {
	MAXSUB = 20
};

typedef struct Sub Sub;

/* This is the basic unit we match against. */
struct Postcard
{
	int len;
	char pkt[128];
	int dpid;
	int inport;
	int version;
};

typedef struct Postcard Postcard;

struct Sub
{
	int ref;
	int nsub;
	Postcard *sub[MAXSUB];
};

struct PostcardFilter
{
	int bpf;
	struct bpf_program program;
	struct sk_filter filter;
	int dpid;
	int inport;
	int version;
};

Sub *newsub(int n);
Sub *incref(Sub*);
Sub *copy(Sub*);
Sub *update(Sub*, int, Postcard*);
void decref(Sub*);

// int backtrack(Prog*, char*, char**, int);
int recursiveloop(Inst *pc, Postcard *sp, Postcard **subp, int nsubp);
int recursiveprog(Prog *prog, Postcard *input, Postcard **subp, int nsubp);
int recursiveloopprog(Prog *prog, Postcard *input, Postcard **subp, int nsubp);
int thompsonvm(Prog *prog, Postcard *input, Postcard **subp, int nsubp);
int pikevm(Prog*, Postcard*, Postcard**, int);
int match(Postcard *sp, PostcardFilter *pf);

void byteify_packet(char *hex, char *bytes);
void hexify_packet(const char *buf, char *hex, size_t buflen);

// defined in gen_history.cpp
Postcard *gen_chain_pcards(int chain_len);
void set_null_postcard(Postcard *p);
void print_history(Postcard *p);

// defined in stream_history.cpp
Postcard *read_packet_history(FILE *fp);
void write_packet_history(FILE *fp, Postcard *p, int len_history);

static inline int eo_postcard(Postcard *p) {
	return p == NULL || p->len == 0;
}

#endif //REGEXP_H
