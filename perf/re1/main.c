// Copyright 2007-2009 Russ Cox.  All Rights Reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
#include <time.h>
#include <sys/time.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include <jansson.h>
#include "regexp.h"
#include "clemson_queries.h"

typedef int (*RegexpMatcher)(Prog*, Postcard *, Postcard**, int);
struct {
	char *name;
	RegexpMatcher fn;
} tab[] = {
	{ "recursive", recursiveprog },
	{ "recursiveloop", recursiveloopprog },
	// "backtrack", backtrack,
	{ "thompson", thompsonvm },
	{ "pike", pikevm },
};

// command line options
int write_hist = 0;
int read_hist = 0;
int microbenchmark = 0;
int macrobenchmark = 0;
int generate = 0;
char *infile = NULL;
char *outfile = NULL;
int num_hist = -1;
int chain_len = 8;
char *regex = "";
RegexpMatcher match_fn = recursiveloopprog;

static struct option long_options[] =
{
    {"write-hist", no_argument, NULL, 'w'},
    {"read-hist", no_argument, NULL, 'r'},
    {"microbenchmark", no_argument, NULL, 'b'},
    {"macrobenchmark", no_argument, NULL, 'B'},
    {"generate", no_argument, NULL, 'g'},
    {"infile", required_argument, NULL, 'i'},
    {"outfile", required_argument, NULL, 'o'},
    {"num-hist", required_argument, NULL, 'n'},
    {"chain-len", required_argument, NULL, 'c'},
    {"regex", required_argument, NULL, 'e'},
    {"match-fn", required_argument, NULL, 'm'},
    {NULL, 0, NULL, 0}
};

Postcard *sub[MAXSUB];

int num_chain_lens = 6;
int chain_lens[] = {2, 4, 8, 16, 32, 64};

int num_ppf_types = 4;
char *ppf_types[] = {".*X$", "X.*X$", ".*X.*", "X.*X.*X$"};

char *get_ppf_str(char *ppf_str, char *ppf_type, int chain_len)
{
    if(strcmp(ppf_type, ".*X$") == 0) {
        sprintf(ppf_str,
                ".*{{ --bpf ip --dpid %d --inport 1 }}",
                chain_len);
    }
    else if(strcmp(ppf_type, "X.*X$") == 0) {
        sprintf(ppf_str,
                "{{ --bpf ip --dpid 1 --inport 1 }}.*{{ --bpf ip --dpid %d --inport 1 }}",
                chain_len/2 + 1);
    }
    else if(strcmp(ppf_type, ".*X.*") == 0) {
        sprintf(ppf_str,
                ".*{{ --bpf ip --dpid %d --inport 1 }}.*",
                chain_len);
    }
    else if(strcmp(ppf_type, "X.*X.*X$") == 0) {
        sprintf(ppf_str,
                "{{ --bpf ip --dpid 1 --inport 1 }}.*{{ --bpf ip --dpid %d --inport 1 }}.*{{ --bpf ip --dpid %d --inport 1 }}",
                chain_len/2 + 1, chain_len);
    }
    return ppf_str;
}

int re_bench(RegexpMatcher fn, Prog *prog, Postcard *inp, int iters, double *time_per_match)
{
	struct timeval start, end;
	int REPEAT = 100000, k = 0;
	int ret = 0;

        if(iters > 0)
            REPEAT = iters;

	gettimeofday(&start, NULL);
	for(k = 0; k < REPEAT; k++) {
		memset(sub, 0, sizeof sub);
		ret = fn(prog, inp, sub, nelem(sub));
	}
	gettimeofday(&end, NULL);

	*time_per_match = ((end.tv_sec - start.tv_sec) + (end.tv_usec - start.tv_usec) / 1000000.0)/ REPEAT;
	printf("time_per_match: %.3fus\n", *time_per_match * 1e6);

	return ret;
}

void re_bench_string(char *regexp)
{
	Regexp *re = parse(regexp);
	Prog *prog;
	parse_postcard_filters(re);
	printre(re);
	printf("\n");

	prog = compile(re);
	printprog(prog);
}

char get_hex_char(char hexval)
{
    if(hexval <= 9) {
        return hexval + '0';
    }
    else {
        return (hexval - 10) + 'a';
    }
}

void hexify_packet(const char *buf, char *hex, size_t buflen)
{
    unsigned int i;
    for(i = 0; i < buflen; i++) {
        *hex++ = get_hex_char((buf[i] >> 4) & 0x0f);
        *hex++ = get_hex_char(buf[i] & 0x0f);
    }
    *hex = '\0';
}

void byteify_packet(char *hex, char *bytes) {
    int count = 0;
    if (hex == NULL)
        return;

    while (*hex) {
        int c = 0;
        count++;
        if (*hex <= '9')
            c = c * 16 + (*hex - '0');
        else
            c = c * 16 + (*hex - 'a' + 10);

        if (count == 2) {
            count = 0;
            *bytes++ = c;
        }
        hex++;
    }
}


void print_matches(Postcard *inp) {
	int k, l;
	for(k=MAXSUB; k>0; k--)
		if(sub[k-1])
			break;
	for(l=0; l<k; l+=2) {
		printf(" (");
		if(sub[l] == nil)
			printf("?");
		else
			printf("%d", (int)(sub[l] - inp));
		printf(",");
		if(sub[l+1] == nil)
			printf("?");
		else
			printf("%d", (int)(sub[l+1] - inp));
		printf(")");
	}
	printf("\n");
}

json_t *load_results(char *filename)
{
    json_t *root;
    json_error_t error;
    root = json_load_file(filename, 0, &error);
    return root;
}

int dump_results(json_t *root, char *filename)
{
    return json_dump_file(root, filename, JSON_INDENT(4));
}

void history_write(char *filename, int chain_len, int num_histories)
{
    int i;
    Postcard *p;
    FILE *fp;
    printf("Writing %d histories, each of length %d, to %s\n", num_histories, chain_len, filename);

    fp = fopen(filename, "w");

    for(i = 0; i < num_histories; i++) {
        p = gen_chain_pcards(chain_len);
        write_packet_history(fp, p, chain_len);
        free(p);
    }
    fclose(fp);
    printf("Wrote %d histories, each of length %d, to %s\n", num_histories, chain_len, filename);

}

void history_read(char *filename, int num_histories)
{
    Postcard *p;
    FILE *fp;
    int i = 0;
    struct timeval start, end;
    double time_per_read;

    printf("Reading histories from %s\n", filename);
    fp = fopen(filename, "r");
    gettimeofday(&start, NULL);
    while(num_histories < 0 || i < num_histories) {
        p = read_packet_history(fp);
        if(p == NULL) {
            break;
        }
        //printf("---------------- history no. %d ------------\n", i+1);
        //print_history(p);
        free(p);
        i++;
    }
    gettimeofday(&end, NULL);
    time_per_read = ((end.tv_sec - start.tv_sec) + (end.tv_usec - start.tv_usec) / 1000000.0)/ i;

    fclose(fp);
    printf("Read %d histories from %s\n", i, filename);
    printf("time_per_read: %.3fus\n", time_per_read * 1e6);
}

int
main(int argc, char **argv)
{
    // loop over all of the options
    int option_index = 0;
    int ch;
    int j;
    printf("Parsing arguments...\n");
    while ((ch = getopt_long(argc, argv, "wrbBgi:o:n:c:e:m:", 
                    long_options, &option_index)) != -1) {
        switch (ch) {
            case 'w':
                write_hist = 1;
                break;
            case 'r':
                read_hist = 1;
                break;
            case 'b':
                microbenchmark = 1;
                break;
            case 'B':
                macrobenchmark = 1;
                break;
            case 'g':
                generate = 1;
                break;
            case 'i':
                infile = optarg;
                break;
            case 'o':
                outfile = optarg;
                break;
            case 'n':
                num_hist = atoi(optarg);
                break;
            case 'c':
                chain_len = atoi(optarg);
                break;
            case 'e':
                regex = optarg;
                break;
            case 'm':
                for(j = 0; j < nelem(tab); j++) {
                    if(strcmp(optarg, tab[j].name) == 0) {
                        printf("Using match function: %s\n", optarg);
                        match_fn = tab[j].fn;
                    }
                }
                break;
            default:
                abort ();
        }
    }
    printf("Done parsing arguments.\n");

    if(write_hist) {
        history_write(outfile, chain_len, num_hist);
        return 0;
    }
    if(read_hist && strcmp(regex, "") == 0) {
        history_read(infile, num_hist);
        return 0;
    }
    if(strcmp(regex, "") != 0) {
	Regexp *re;
	Prog *prog;
        Postcard *sub[MAXSUB];
	int ret = 0;

        Postcard *p;
        FILE *fp;
        int i = 0;
	struct timeval start, end;
        double time_per_read_match = 0;
        double time_per_match = 0;

        re = parse(regex);
        parse_postcard_filters(re);
        prog = compile(re);
        memset(sub, 0, sizeof sub);

        printf("Input file: %s\n", infile);
        printf("Matching against PHF: \"%s\"\n", regex);
        fp = fopen(infile, "r");

	gettimeofday(&start, NULL);
        while(num_hist < 0 || i < num_hist) {
            struct timeval match_start, match_end;
            p = read_packet_history(fp);
            if(p == NULL) {
                break;
            }
            gettimeofday(&match_start, NULL);
            ret = match_fn(prog, p, sub, nelem(sub));
            gettimeofday(&match_end, NULL);
            time_per_match += ((match_end.tv_sec - match_start.tv_sec) + (match_end.tv_usec - match_start.tv_usec) / 1000000.0);
            //printf("---------------- history no. %d ------------\n", i+1);
            //print_history(p);
            free(p);
            i++;
        }
	gettimeofday(&end, NULL);
        time_per_match /= i;
	time_per_read_match = ((end.tv_sec - start.tv_sec) + (end.tv_usec - start.tv_usec) / 1000000.0)/ i;

        if (!ret) {
            printf("-no match-\n");
        }
        else {
            printf("match\n");
        }

        fclose(fp);
        printf("Processed %d histories from %s\n", i, infile);
	printf("time_per_read_match: %.3fus\n", time_per_read_match * 1e6);
	printf("time_per_match: %.3fus\n", time_per_match * 1e6);
        return 0;
    }
    if(generate) {
	Regexp *re;
	Prog *prog;
        Postcard *sub[MAXSUB];
	int ret = 0;

        char *ppf_type;
        char ppf_str[250];
        int chain_len;

        Postcard *p;
        FILE *fp;
        int i = 0, j = 0, k = 0, np = 0;
	struct timeval start, end;

        json_t *result = json_object();
        printf("Generating paper-ready results\n");


        int num_fcn_types = 2;
        char *fcn_type[] = {"match", "no-match"};


        for(j = 0; j < num_fcn_types; j++) {
            json_t *match_val = json_object();
            json_object_set_new(result, fcn_type[j], match_val);
            for(np = 0; np < num_ppf_types; np++) {
                ppf_type = ppf_types[np];
                json_t *ppf_val = json_object();
                json_object_set_new(match_val, ppf_type, ppf_val);
                for(k = 0; k < num_chain_lens; k++) {
                    char chain_len_str[10];
                    char hist_file[30];
                    //double time_per_read_match = 0;
                    double time_per_match = 0;

                    chain_len = chain_lens[k];
                    sprintf(chain_len_str, "%d", chain_len);
                    sprintf(hist_file, "%s-c%d.hist", infile, chain_len);

                    json_t *chain_len_val = json_object();
                    json_object_set_new(ppf_val, chain_len_str, chain_len_val);

                    printf("Input file: %s\n", hist_file);
                    fp = fopen(hist_file, "r");

                    if(strcmp(fcn_type[j], "match") == 0) {
                        // get a matching PPF
                        get_ppf_str(ppf_str, ppf_type, chain_len);
                    }
                    else if(strcmp(fcn_type[j], "no-match") == 0) {
                        // get a non-matching PPF
                        get_ppf_str(ppf_str, ppf_type, 2*chain_len);
                    }
                    printf("Matching against PHF: \"%s\"\n", ppf_str);

                    re = parse(ppf_str);
                    parse_postcard_filters(re);
                    prog = compile(re);
                    memset(sub, 0, sizeof sub);

                    gettimeofday(&start, NULL);
                    i = 0;
                    while(num_hist < 0 || i < num_hist) {
                        struct timeval match_start, match_end;
                        p = read_packet_history(fp);
                        if(p == NULL) {
                            break;
                        }
                        gettimeofday(&match_start, NULL);
                        ret = match_fn(prog, p, sub, nelem(sub));
                        gettimeofday(&match_end, NULL);
                        time_per_match += ((match_end.tv_sec - match_start.tv_sec) + (match_end.tv_usec - match_start.tv_usec) / 1000000.0);
                        free(p);
                        i++;
                    }
                    gettimeofday(&end, NULL);
                    time_per_match /= i;
                    //time_per_read_match = ((end.tv_sec - start.tv_sec) + (end.tv_usec - start.tv_usec) / 1000000.0)/ i;
                    json_object_set_new(chain_len_val, "median", json_real(time_per_match));

                    if (!ret) {
                        printf("-no match-\n");
                    }
                    else {
                        printf("match\n");
                    }

                    fclose(fp);
                }
            }
        }

        char *result_str; 
        result_str = json_dumps(result, JSON_INDENT(4));
        printf("%s\n", result_str);
        free(result_str);

        if(outfile)
            dump_results(result, outfile);
        json_decref(result);
        return 0;
    }
    if(microbenchmark) {
        int i, j, p;
	Regexp *re;
	Prog *prog;
	int ret;
        char *ppf_type;
        char ppf_str[250];
        int chain_len;

	Postcard *inp;
        json_t *old_result;

        printf("Running microbenchmark...\n");
        if(infile != NULL) {
            printf("Loading old results from %s\n", infile);
            old_result = load_results(infile);
        }
        else {
            old_result = json_object();
        }

        json_t *result = json_object();

        for(j = 0; j < nelem(tab); j++) {
            printf("================================================\n");
            printf("%s\n", tab[j].name);
            memset(sub, 0, sizeof sub);

            json_t *match_val = json_object();
            json_object_set_new(result, tab[j].name, match_val);

            for(p = 0; p < num_ppf_types; p++) {
                ppf_type = ppf_types[p];
                json_t *ppf_val = json_object();
                json_object_set_new(match_val, ppf_type, ppf_val);
                for(i = 0; i < num_chain_lens; i++) {
                    chain_len = chain_lens[i];
                    char chain_len_str[10];
                    sprintf(chain_len_str, "%d", chain_len);

                    json_t *chain_len_val = json_object();
                    json_object_set_new(ppf_val, chain_len_str, chain_len_val);

                    inp = gen_chain_pcards(chain_len);
                    get_ppf_str(ppf_str, ppf_type, chain_len);

                    printf("---------------------------------------\n");
                    printf("Chain length: %d\n", chain_len);
                    printf("PPF TYPE: %s\n", ppf_type);
                    printf("PPF String: %s\n", ppf_str);

                    re = parse(ppf_str);
                    parse_postcard_filters(re);
                    //printf("Parse tree for regex\n");
                    //printre(re);
                    //printf("\n");
                    prog = compile(re);

                    //printf("Bytecode for regex\n");
                    //printprog(prog);

                    double time_per_match;
                    ret = re_bench(tab[j].fn, prog, inp, num_hist, &time_per_match);
                    json_object_set_new(chain_len_val, "median", json_real(time_per_match));
                    if (!ret) {
                        printf("-no match-\n");
                        continue;
                    }
                    printf("match\n");
                    //print_matches(inp);
                }
            }
        }



        json_object_update(old_result, result);

        char *result_str; 
        result_str = json_dumps(result, JSON_INDENT(4));
        printf("%s\n", result_str);
        free(result_str);

        if(outfile)
            dump_results(old_result, outfile);
        json_decref(old_result);
        return 0;
    }
    if(macrobenchmark) {
        int num_queries = nelem(clemson_queries);
        int i = 0, j = 0;
	struct timeval start, end;
        struct timeval match_start, match_end;
        double time_per_read_match, time_per_match;

	Postcard *p;
        FILE *fp;
        json_t *result = json_object();

        printf("Running Clemson macrobenchmark...\n");
        for(i = 0; i < num_queries; i++) {
            Regexp *re;
            Prog *prog;
            int ret;
            char *query = clemson_queries[i].query;
            char *query_name = clemson_queries[i].name;
            printf("======================================\n");
            printf("Query no.: %d\n", i+1);
            printf("Query name: %s\n", query_name);
            //printf("\"%s\"\n", query);
            fp = fopen(infile, "r");
            memset(sub, 0, sizeof sub);

            //printf("Compling query...\n");
            re = parse(query);
            parse_postcard_filters(re);
            prog = compile(re);
            //printf("Parse tree for regex\n");
            //printre(re);
            //printf("\n");
            //printf("Bytecode for regex\n");
            //printprog(prog);

            //printf("Reading packet histories from %s\n", infile);
            int num_matches = 0;
            j = 0;
            time_per_read_match = 0;
            time_per_match = 0;

            gettimeofday(&start, NULL);
            while(1) {
                p = read_packet_history(fp);
                if(p == NULL) {
                    break;
                }
                gettimeofday(&match_start, NULL);
                ret = match_fn(prog, p, sub, nelem(sub));
                gettimeofday(&match_end, NULL);
                time_per_match += ((match_end.tv_sec - match_start.tv_sec) + (match_end.tv_usec - match_start.tv_usec) / 1000000.0);
                if(ret)
                    num_matches++;
                free(p);
                j++;
            }

            gettimeofday(&end, NULL);
            time_per_match /= j;
            time_per_read_match = ((end.tv_sec - start.tv_sec) + (end.tv_usec - start.tv_usec) / 1000000.0)/ j;

            json_object_set_new(result, query_name, json_real(time_per_match));

            printf("matches: %d\n", num_matches);
            printf("non-matches: %d\n", (j - num_matches));
            printf("time_per_read_match: %.3fus\n", time_per_read_match * 1e6);
            printf("time_per_match: %.3fus\n", time_per_match * 1e6);
            fclose(fp);
        }
        // print result
        char *result_str; 
        result_str = json_dumps(result, JSON_INDENT(4));
        printf("%s\n", result_str);
        free(result_str);

        // dump result to output file
        if(outfile)
            dump_results(result, outfile);
        json_decref(result);
        return 0;
    }
    return 0;

}
