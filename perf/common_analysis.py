"""Analysis: a class to help with a common data-processing pattern."""

from optparse import OptionParser
import os
import json

DEF_DATA_DIR = 'data'
DEF_PLOT_DIR = 'plot'
DEF_PLOT_EXT = 'pdf'


class Analysis(object):

    def __init__(self,
                 def_analyses,
                 data_analysis_fcns,
                 def_data_ext,
                 def_data_dir = DEF_DATA_DIR,
                 def_plot_dir = DEF_PLOT_DIR,
                 def_plot_ext = DEF_PLOT_EXT):

        self.def_analyses = def_analyses
        self.data_analysis_fcns = data_analysis_fcns
        self.def_data_dir = def_data_dir
        self.def_plot_dir = def_plot_dir
        self.data_ext = def_data_ext
        self.plot_ext = def_plot_ext
        self.run()

    def str_list_callback(self, option, opt, value, parser):
        setattr(parser.values, option.dest, value.split(','))

    def get_options(self):
        """Return common options as an OptionParser object"""
        opts = OptionParser()
        opts.add_option("-g", "--generate",  action = "store_true",
                        default = False,
                        help = "generate data?")
        opts.add_option("-f", "--force", action = "store_true",
                        default = False,
                        help = "re-compute analysis even if data is there?")
        opts.add_option("-s", "--save_data", action = "store_true",
                        default = False,
                        help = "save generated data to files?")
        opts.add_option("--save_plot",  action = "store_true",
                        default = False,
                        help = "save plot?")
        opts.add_option("-p", "--show_plot",  action = "store_true",
                        default = False,
                        help = "show plot?")
        opts.add_option("-n", "--normalize", action = "store_true",
                        default = False,
                        help = "normalize plot data?")
        opts.add_option("-a", "--analyses", type = 'string',
                        default = None,
                        help = "comma-sep list of analyses in %s" % self.def_analyses)
        opts.add_option("--data_dir", type = 'string',
                        default = self.def_data_dir,
                        help = "directory for data files")
        opts.add_option("--plot_dir", type = 'string',
                        default = self.def_plot_dir,
                        help = "directory for data files")
        return opts

    def parse_args(self):
        """Parse options"""
        opts = self.get_options()
        options, arguments = opts.parse_args()
        options = self.process_options(options)
        return options


    def process_options(self, options):
        """Convert any options with special formatting, like comma-sep strs."""
        all_names = [name for name in self.data_analysis_fcns]
        # Convert from comma-sep string into list
        if options.analyses:
            options.analyses = options.analyses.split(',')
        else:
            options.analyses = self.def_analyses
        for analysis in options.analyses:
            assert analysis in all_names
        return options


    def load_data(self, data_dir):
        if os.path.exists(data_dir):
            data = {}  # data[analysis filename] = PostcardAnalysis results dict
            print "Reading data from %s" % data_dir
            found_data = False
            for filename in os.listdir(data_dir):
                analysis, ext = os.path.splitext(filename)
                print "Loading %s" % filename
                if ext[1:] == self.data_ext:
                    found_data = True
                    with open(os.path.join(data_dir, filename), 'r') as f:
                        data[analysis] = json.load(f)
            if not found_data:
                print "Could not find data in %s" % data_dir
                return None
            print "len(data): %s" % len(data)
            return data
        else:
            return None
    
    
    def generate_data(self, data_dir, analyses, options):
        data = {}
        for analysis_name in analyses:
            fcn = self.data_analysis_fcns[analysis_name]
            print "Generating data: %s()" % analysis_name
            data[analysis_name] = fcn(options)
        return data
    
    
    def save_data(self, data_dir, data):
        print "Writing data to %s" % data_dir
        if not data:
            raise Exception("no data to write")
        if not os.path.exists(data_dir):
            os.makedirs(data_dir)
        for filename, vals in data.iteritems():
            full_filename = os.path.join(data_dir, filename + '.' + self.data_ext)
            with open(full_filename, 'w') as f:
                json.dump(vals, f, indent = 4)
                print "Wrote %s" % filename


    def plot_data(self, plot_dir, data, show_plot, save_plot, normalize):
        pass


    def run(self):
        options = self.parse_args()
        data = self.load_data(options.data_dir)
        # Ensure that we have some data first
        if options.generate:
            if (data and not options.force):
                new_data = self.generate_data(options.data_dir,
                        options.analyses, options)
                for analysis in new_data:
                    if analysis not in data:
                        data[analysis] = {}
                    data[analysis].update(new_data[analysis])
            else:
                data = self.generate_data(options.data_dir, 
                        options.analyses, options)
        elif data is None:
            print "No data input provided; exiting."
            exit()
    
        assert data is not None
        if options.save_data:
            self.save_data(options.data_dir, data)
    
        if options.show_plot or options.save_plot:
            self.plot_data(options.plot_dir, data, options.show_plot, options.save_plot,
                      options.normalize)
