#!/usr/bin/env python
"""Sweep across different sizes for the chain topology."""

import logging
import os
from collections import OrderedDict

import ndb.topo_sort as topo_sort
from common_analysis import Analysis
from stat_fcns import STAT_FCNS

from regex_match_chain import chain_analysis_single, us_str
from regex_match_chain import gen_ppf_str, gen_minre
from regex_match_chain import DPI
import regex_match_chain

lg = logging.getLogger("regex_match_sweep")


DEF_DATA_EXT = 'latsweep'  # dict of sizes to latencies

DEF_CHAIN_LENS = [2 ** i for i in range(1, 6)] + [60]
DEF_ITERS = 100
DEF_STATS_LST = STAT_FCNS.keys()


# Data analysis fcns: each benchmarks a scenario.
DATA_ANALYSIS_FCNS = {
    'chain_sweep': (lambda options: chain_sweep_all(options)),
}
ALL_DATA_ANALYSIS_NAMES = DATA_ANALYSIS_FCNS.keys()

NO_LEGEND = False

prog = None
is_pypy = regex_match_chain.is_pypy
if is_pypy:
    prog = 'pypy'
else:
    import ndb.filter as filter
    prog = 'python'

MATCH_FCNS = regex_match_chain.MATCH_FCNS
PPF_TYPES = regex_match_chain.PPF_TYPES

class RegexMatchChainSweepAnalysis(Analysis):

    def plot_data(self, plot_dir, data, show_plot, save_plot, normalize):
        from ndb.analysis.plot_defaults import apply_time_series_plot_defaults
        import matplotlib.pyplot as plt
        from ndb.plot.plot_helper import plotTimeSeries
        from ndb.plot.plot_helper import linestyleGenerator, colorGenerator

        apply_time_series_plot_defaults()
        lsgen = linestyleGenerator()

        # data[analysis_name][match_fcn_name][ppf_type][chain_len][stat] ...
        plot_data_series = []
        for analysis_name, analysis_data in data.iteritems():
            for match_fcn_name, match_fcn_data in analysis_data.iteritems():
                linestyle = lsgen.next()
                cgen = colorGenerator()
                for ppf_type, ppf_data in match_fcn_data.iteritems():

                    chain_lens = ppf_data.keys()
                    data_series = {}
                    metrics = ppf_data.itervalues().next().keys()

                    for chain_len, chain_len_data in ppf_data.iteritems():
                        for m in metrics:
                            data_series[m] = []

                    # flipping the key-values: chain_len, stat
                    for chain_len, chain_data in ppf_data.iteritems():
                        for metric_name, metric_val in chain_data.iteritems(): 
                            data_series[metric_name].append(metric_val)

                    for m in metrics:
                        # stupid json stores int keys as strings... 
                        # this is a way to fix it
                        xyvals = zip(map(int, chain_lens), [d * 1e6 for d in
                            data_series[m]])
                        xyvals.sort(key=lambda t: t[0])
                        xvals, yvals = zip(*xyvals)

                        plot_data_series.append({
                            'label': '%s-%s' % (ppf_type, match_fcn_name),
                            'x': xvals,
                            'y': yvals,
                            'opts': {'marker': 'o', 'color': cgen.next(), 'linestyle': linestyle},
                        })

        fig = plotTimeSeries(data = plot_data_series,
            title = '%s latency' % analysis_name,
            xlabel = 'Packet History Length',
            ylabel = 'Latency (us)',
            step = False,
            xscale = 'log',
            yscale = 'log')

        if NO_LEGEND:
            lgd = plt.legend()
            lgd.set_visible(False)
    
        if save_plot:
            if not os.path.exists(plot_dir):
                os.makedirs(plot_dir)
            filepath = os.path.join(plot_dir, analysis_name + '.' + self.plot_ext)
            print "Writing file to %s" % filepath
            fig.savefig(filepath, dpi = DPI)
        if show_plot:
            plt.show()
        
        plt.close(fig)


    def process_options(self, options):
        options = super(RegexMatchChainSweepAnalysis, self).process_options(options)
        # Convert from comma-sep string into list
        if options.chain_lens:
            options.chain_lens = options.chain_lens.split(',')
            options.chain_lens = [int(i) for i in options.chain_lens]
        else:
            options.chain_lens = DEF_CHAIN_LENS
        return options

    def parse_args(self):
        """Parse options"""
        opts = self.get_options()
        opts.add_option("--chain_lens", type = 'str',
                        default = None,
                        help = "comma-sep list of chain lens")
        opts.add_option("--iters", type = 'int',
                        default = DEF_ITERS,
                        help = "number of iterations per test")
        opts.add_option("--stats", type = 'str',
                        default = DEF_STATS_LST,
                        action = 'callback',
                        callback = self.str_list_callback,
                        help = "Stats to produce results for")
        opts.add_option("--match_fcns", type = 'str',
                        default = MATCH_FCNS,
                        action = 'callback',
                        callback = self.str_list_callback,
                        help = "Stats to produce results for")
        options, arguments = opts.parse_args()
        options = self.process_options(options)
        return options


def chain_sweep_all(options):
    # master dict w/all data.
    data = {}

    for match_fcn_name in options.match_fcns:
        match_fcn = MATCH_FCNS[match_fcn_name]

        match_fcn_key = match_fcn_name
        if prog:
            match_fcn_key = '%s-%s' % (match_fcn_name, prog)

        print '='*50
        print '='*20, match_fcn_key, '='*20
        data[match_fcn_key] = {}
        for ppf_type in PPF_TYPES:
            data[match_fcn_key][ppf_type] = OrderedDict({})
            for chain_len in options.chain_lens:
                if ppf_type == 'X.*X.*X$' and chain_len < 3:
                    continue
                ppf_str = gen_ppf_str(ppf_type, chain_len)
                ppf = None
                if 'minre_match' in match_fcn_name:
                    ppf = gen_minre(ppf_type, chain_len)
                else:
                    if not is_pypy:
                        if 'old_match' in match_fcn_name:
                            ppf = filter.PacketPathFilter(ppf_str, open(os.path.devnull, 'w'),
                                    syntax_check=False)
                data[match_fcn_key][ppf_type][chain_len] = {}
                vals = chain_analysis_single(match_fcn_name, match_fcn,
                        ppf_type, ppf_str, ppf,
                        chain_len, options.iters)
                                                   
                for stat in options.stats:
                    fcn = STAT_FCNS[stat]
                    stat_val = fcn(vals)
                    data[match_fcn_key][ppf_type][chain_len][stat] = stat_val
            print '-'*50
            
    return data


if __name__ == '__main__':
    RegexMatchChainSweepAnalysis(def_analyses = ALL_DATA_ANALYSIS_NAMES,
                              data_analysis_fcns = DATA_ANALYSIS_FCNS,
                              def_data_ext = DEF_DATA_EXT)
