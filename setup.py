#!/usr/bin/env python
'''Setuptools params'''

from setuptools import setup, find_packages

setup(
    name='ndb',
    version='0.0.0',
    description='Network Debugger',
    author='NDB team',
    packages=find_packages(exclude='test'),
    long_description="""\
See the HotSDN paper "Where's the debugger for my software-defined network?" for more details.
      """,
    install_requires=['setuptools']
)
