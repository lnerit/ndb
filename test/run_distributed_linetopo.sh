#!/bin/bash

OUTDIR=/tmp/ndb
MN_LOG=$OUTDIR/mininet.log
PROXY_LOG=$OUTDIR/proxy.log
MASTER_LOG=$OUTDIR/master_collector.log
SLAVE_LOG_PREF=$OUTDIR/slave_collector

N=3
DEBUG=""
#DEBUG="--debug"

screenname="ndb"
wnum=1

cleanup()
{
    killall screen
    mn -c
}

# run if user hits control-c
control_c()
{
    cleanup
    exit
}

# wait for mininet to start
wait_mininet()
{
    echo "Waiting for the mininet network to startup"
    while [ "`grep \"mininet>\" $MN_LOG`" == "" ]
    do
        echo -n .
        sleep 1
    done
    echo 
}

# wait for the switches to connect to the proxy
wait_proxy_connect()
{
    echo "Waiting for the switches to connect to the proxy"
    while [ "`grep \"ALL SWITCHES CONNECTED\" $PROXY_LOG`" == "" ]
    do
        echo -n .
        sleep 1
    done
    echo 
}

# wait for the master to start
wait_collector()
{
    echo "Waiting for the collector logged at $1 to start"
    while [ "`grep \">>>\" $1`" == "" ]
    do
        echo -n .
        sleep 1
    done
    echo 
}

# wait for the slave to connect to the master
wait_slave_connect()
{
    echo "Waiting for all the slaves to connect to the master"
    prev_connect=0
    while [ "`grep -c -w \"HELLO\" $MASTER_LOG`" -lt $N ]
    do
        curr_connect=`grep -c -w \"HELLO\" $MASTER_LOG`
        if [ $curr_connect -gt $prev_connect ]
        then
            echo
            echo "$curr_connect slaves connected"
            prev_connect=$curr_connect
        fi

        echo -n .
        sleep 1
    done
    echo 
}

cleanup

# cleanup old files
rm -rf $OUTDIR
mkdir -p $OUTDIR

# trap keyboard interrupt (control-c)
trap control_c SIGINT

# start new screen session
screen -dm -S $screenname 

# add a new window and start the mininet network
echo "Adding a new window and starting the mininet network"
screen -dr -S $screenname -X screen 
screen -dr -S $screenname -p $wnum -X logfile $OUTDIR/mininet.log
screen -dr -S $screenname -p $wnum -X logfile flush .1
screen -dr -S $screenname -p $wnum -X log on
screen -dr -S $screenname -p $wnum -X stuff "python \
    test/distributed_linetopo.py -n $N $(printf \\r)"
mnwin=$wnum
wnum=$(expr $wnum + 1)

# wait for the mininet network to startup
wait_mininet

# add a new window and start the ndb proxy
echo "Adding a new window and starting the ndb proxy"
screen -dr -S $screenname -X screen 
screen -dr -S $screenname -p $wnum -X logfile $OUTDIR/proxy.log
screen -dr -S $screenname -p $wnum -X logfile flush .1
screen -dr -S $screenname -p $wnum -X log on
screen -dr -S $screenname -p $wnum -X stuff "python ndb.py --topo /tmp/topo.json $(printf \\r)"
wnum=$(expr $wnum + 1)

# wait for the switches to connect to the proxy
wait_proxy_connect

# add a new window and start the master collector
echo "Adding a new window and starting the master collector"
screen -dr -S $screenname -X screen 
screen -dr -S $screenname -p $wnum -X logfile $OUTDIR/master_collector.log
screen -dr -S $screenname -p $wnum -X logfile flush .1
screen -dr -S $screenname -p $wnum -X log on
screen -dr -S $screenname -p $wnum -X stuff "python master.py $DEBUG $(printf \\r)"
masterwin=$wnum
wnum=$(expr $wnum + 1)

# wait for the master to start
wait_collector $MASTER_LOG

# add new windows and start slave collectors
for S in `seq 1 $N`
do
    echo "Adding a new window and starting the slave collector no. $S"
    SLAVE_LOG="${SLAVE_LOG_PREF}.${S}.log"
    SLAVE_NAME=`echo $N + $S | bc`
    INTF="h${SLAVE_NAME}-eth0"
    screen -dr -S $screenname -X screen 
    screen -dr -S $screenname -p $wnum -X logfile $SLAVE_LOG
    screen -dr -S $screenname -p $wnum -X logfile flush .1
    screen -dr -S $screenname -p $wnum -X log on
    screen -dr -S $screenname -p $wnum -X stuff "python slave.py --intf \
        $INTF $DEBUG $(printf \\r)"
    wnum=$(expr $wnum + 1)

    # wait for the slave to start
    wait_collector $SLAVE_LOG
done

# wait for the slave to connect to the master
wait_slave_connect

# add PPF to the master
echo "Adding PPF to the master"
screen -dr -S $screenname -p $masterwin -X stuff "c.add_filter('.*{{--bpf \
\"icmp\" --dpid 3 --inport 1}}')$(printf \\r)"

# run pings in mininet
echo "Running pings in mininet"
screen -dr -S $screenname -p $mnwin -X stuff "h1 ping -c 1 10.0.0.2 $(printf \\r)"

# wait for the ping to complete and the backtrace to be produced
# TODO: a more reasonable logic to figure out how long to wait
echo "Waiting for the ping to complete and the backtrace to be produced"
sleep 5

# terminate the screen session including all the windows
echo "Terminating the screen session including all the windows"
screen -r $screenname -X quit
cleanup

echo "All output is logged in $OUTDIR"
