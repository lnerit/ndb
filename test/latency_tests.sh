#!/bin/bash

RESULTS_DIR="test/results"
PINGS=100
PARSE=
MATCHTEST=

while getopts pmo: opt
do
    case "$opt" in
        p) PARSE=1;;
        m) MATCHTEST=1;;
	o) RESULTS_DIR=$OPTARG;;
    esac
done

if [ "$PARSE" == "1" ]; then
    RESULTS_DIR="results"
    cd test
    for N in 2 4 8 16 32
    do
        sudo python parse_timestamp.py -i $RESULTS_DIR/n$N -o \
            $RESULTS_DIR/n$N/$N.json
    done
    cd -
elif [ "$MATCHTEST" == "1" ]; then
    for N in 2 4 8 16 32
    do
        echo -e "\n--------------------------------"
        echo -e "Running linetopo matchtest for N=$N\n"
        sudo ./test/run_linetopo.sh -c -i -n $N -p 1 -o $RESULTS_DIR/n$N
    done
else
    for N in 2 4 8 16 32
    do
        echo -e "\n--------------------------------"
        echo -e "Running linetopo test for N=$N\n"
        sudo ./test/run_linetopo.sh -t -n $N -p $PINGS -o $RESULTS_DIR/n$N
    done
fi

