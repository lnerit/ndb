#!/bin/bash
# Fail on ony error.
set -e

RAND=
while getopts r opt
do
    case "$opt" in
        r) RAND="--randomize";;
    esac
done

RUNS=100
cd test

for n in 2 4 8 16 32
do
    OUTDIR=results2/n$n
    pcap=$OUTDIR/pp.pcap
    psid=$OUTDIR/psid.json
    python linetopo_match.py -n $n --pcap $pcap --psid $psid $RAND
done
cd -
