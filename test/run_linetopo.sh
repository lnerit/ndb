#!/bin/bash

OUTDIR=/tmp/ndb
TIMESTAMP=
N=3
DEBUG=
PINGS=1
PCAP=
PSID=

#Process the arguments
usage()
{
    echo "
    Usage: $0 options

    OPTIONS:
    -h      Show this message
    -t      Run timestamping experiment
    -n      Length of the linetopo
    -d      Run in debug mode
    -p      No. of pings to run
    -c      Capture postcards in pcap file
    -i      Dump psid->dpid in json file
    "
}

while getopts tn:dp:o:cih opt
do
    case "$opt" in
        t) TIMESTAMP=1;;
        n) N=$OPTARG;;
        d) DEBUG="--debug";;
        p) PINGS=$OPTARG;;
	o) OUTDIR=$OPTARG;;
	c) PCAP=1;;
	i) PSID=1;;
        h) usage; exit;;
        ?) usage; exit;;
    esac
done

MN_LOG=$OUTDIR/mininet.log
PROXY_LOG=$OUTDIR/proxy.log
MASTER_LOG=$OUTDIR/master_collector.log
SLAVE_LOG=$OUTDIR/slave_collector.log

if [ "$PCAP" == "1" ]
then
    PCAP="--pcap-out $OUTDIR/pp.pcap"
fi

if [ "$PSID" == "1" ]
then
    PSID="--psid-out $OUTDIR/psid.json"
fi

screenname="ndb"
wnum=1

cleanup()
{
    screen -wipe $screenname
    killall screen
    killall tcpdump
    mn -c
}

# run if user hits control-c
control_c()
{
    cleanup
    exit
}

# wait for mininet to start
wait_mininet()
{
    echo "Waiting for the mininet network to startup"
    while [ "`grep \"mininet>\" $MN_LOG`" == "" ]
    do
        echo -n .
        sleep 1
    done
    echo 
}

# wait for the switches to connect to the proxy
wait_proxy_connect()
{
    echo "Waiting for the switches to connect to the proxy"
    while [ "`grep \"ALL SWITCHES CONNECTED\" $PROXY_LOG`" == "" ]
    do
        echo -n .
        sleep 1
    done
    echo 
}

# wait for the master to start
wait_master()
{
    echo "Waiting for the master to start"
    while [ "`grep \">>>\" $MASTER_LOG`" == "" ]
    do
        echo -n .
        sleep 1
    done
    echo 
}

# wait for the slave to connect to the master
wait_slave_connect()
{
    echo "Waiting for the slave to connect to the master"
    while [ "`grep \"HELLO\" $MASTER_LOG`" == "" ]
    do
        echo -n .
        sleep 1
    done
    echo 
}

cleanup

# cleanup old files
rm -rf $OUTDIR
mkdir -p $OUTDIR

# trap keyboard interrupt (control-c)
trap control_c SIGINT

# start new screen session
screen -dm -S $screenname 

# add a new window and start the mininet network
echo "Adding a new window and starting the mininet network"
screen -dr -S $screenname -X screen 
screen -dr -S $screenname -p $wnum -X logfile $MN_LOG
screen -dr -S $screenname -p $wnum -X logfile flush .1
screen -dr -S $screenname -p $wnum -X log on
screen -dr -S $screenname -p $wnum -X stuff "python test/mn-linetopo.py -n $N $(printf \\r)"
mnwin=$wnum
wnum=$(expr $wnum + 1)

# wait for the mininet network to startup
wait_mininet

# add a new window and start the ndb proxy
echo "Adding a new window and starting the ndb proxy"
screen -dr -S $screenname -X screen 
screen -dr -S $screenname -p $wnum -X logfile $PROXY_LOG
screen -dr -S $screenname -p $wnum -X logfile flush .1
screen -dr -S $screenname -p $wnum -X log on
screen -dr -S $screenname -p $wnum -X stuff "python ndb/ndb.py --topo /tmp/topo.json $(printf \\r)"
wnum=$(expr $wnum + 1)

# wait for the switches to connect to the proxy
wait_proxy_connect

# add a new window and start the master collector
echo "Adding a new window and starting the master collector"
screen -dr -S $screenname -X screen 
screen -dr -S $screenname -p $wnum -X logfile $MASTER_LOG
screen -dr -S $screenname -p $wnum -X logfile flush .1
screen -dr -S $screenname -p $wnum -X log on
screen -dr -S $screenname -p $wnum -X stuff "python ndb/master.py $DEBUG \
    $PCAP $PSID $(printf \\r)"
masterwin=$wnum
wnum=$(expr $wnum + 1)

# wait for the master to start
wait_master

# add a new window and start the slave collector
echo "Adding a new window and starting the slave collector"
screen -dr -S $screenname -X screen 
screen -dr -S $screenname -p $wnum -X logfile $SLAVE_LOG
screen -dr -S $screenname -p $wnum -X logfile flush .1
screen -dr -S $screenname -p $wnum -X log on
screen -dr -S $screenname -p $wnum -X stuff "python ndb/slave.py --intf h0-eth0 \
    $DEBUG $(printf \\r)"
wnum=$(expr $wnum + 1)

# wait for the slave to connect to the master
wait_slave_connect

# add PPF to the master
echo "Adding PPF to the master"
screen -dr -S $screenname -p $masterwin -X stuff "c.add_filter('.*{{--bpf \
\"icmp\" --dpid $N --inport 1}}')$(printf \\r)"

if [ "$TIMESTAMP" == "1" ]; then
    echo "Starting tcpdump"
    INTF="s1-eth2"
    tcpdump -tttt -ni $INTF -s0 -w $OUTDIR/capture.pcap "icmp" &
    # sleep for a bit while tcpdump starts
    sleep 1
fi

# run pings in mininet
echo "Running pings in mininet"
screen -dr -S $screenname -p $mnwin -X stuff "h1 ping -c $PINGS 10.0.0.3 $(printf \\r)"

# wait for the ping to complete and the backtrace to be produced
# TODO: a more reasonable logic to figure out how long to wait
echo "Waiting for the ping to complete and the backtrace to be produced"
for i in `seq 1 $(expr $PINGS + 5)`
do
    echo -n '.'
    sleep 1
done
echo ""

# terminate the screen session including all the windows
echo "Terminating the screen session including all the windows"
screen -r $screenname -X quit
cleanup

echo "All output is logged in $OUTDIR"
