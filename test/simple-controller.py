import argparse
from twisted.internet import reactor, defer, protocol, interfaces
from openfaucet import ofcontroller, ofproto, ofaction, buffer, ofmatch, ofstats
from zope import interface

parser = argparse.ArgumentParser(description="Simple Openflow controller")
parser.add_argument('--port', '-p',
                    dest="port",
                    type=int,
                    help="Port to listen for connections",
                    default=10000)

args = parser.parse_args()

class SimpleController(ofproto.OpenflowProtocol):
    def __init__(self):
        super(SimpleController, self).__init__()

    def connectionMade(self):
        super(SimpleController, self).connectionMade()
        addr = self.transport.getPeer()
        print "Connection made... to %s:%s" % (addr.host, addr.port)

    def handle_echo_request(self, xid, data):
        print "Echo request"
        self.send_echo_reply(xid, data)

    def handle_packet_in(self, buffer_id, total_len, in_port, reason, data):
        print "Packet in"
        pass

factory = ofproto.OpenflowProtocolFactory(protocol=SimpleController)
print "Listening on port %s" % args.port
reactor.listenTCP(args.port, factory)
reactor.run()
