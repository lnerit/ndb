import sys
import os
import re
import matplotlib.pyplot as plt
import argparse
import json

from ndb.plot_helper import plotCDF
import ndb.plot_defaults


def parse_args():
    parser = argparse.ArgumentParser(description="Plot latency CDF")

    parser.add_argument('--infiles', '-i',
                        dest="infiles",
                        action="store",
                        nargs='+',
                        help="Input json files",
                        default=['timestamps.json'])

    parser.add_argument('--keys', '-k',
                        dest="keys",
                        action="store",
                        nargs='+',
                        help="Keys to plot",
                        default=['get', 'sort', 'match', 'total'])


    args = parser.parse_args()
    return args

if __name__ == '__main__':
    args = parse_args()
    for k in args.keys:
        data = []

        # try to sort files
        def baseint(f):
            base=os.path.basename(f)
            label = os.path.splitext(base)[0]
            r = re.compile('(\d+)')
            m = r.search(label)
            if m:
                return int(m.group(1))
            return 0

        sorted_f = sorted(args.infiles, key=baseint)
        for f in sorted_f:
            label = str(baseint(f)) if baseint(f) > 0 else f
            j = json.load(open(f, 'r'))
            yvals = map(lambda x: x*1000, j[k])
            data.append({'label': 'N = %s' % label, 'y': yvals})
        plotCDF(data, '%s latency' % k, 'Latency (ms)', 'Fraction', False)
    plt.show()


