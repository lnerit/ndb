#!/usr/bin/python

import sys
import os

# append to path if being run locally
sys.path.append('../')

import argparse
import dpkt
import time
import datetime
import glob
import json
from collector import pkt_hash
from timestamp import parse_tstamp

def parse_args():
    parser = argparse.ArgumentParser(description="Parse timestamp data")

    parser.add_argument('--indir', '-i',
                        dest="indir",
                        help="Input directory",
                        default='/tmp/ndb')

    parser.add_argument('--outfile', '-o',
                        dest="outfile",
                        help="Output file",
                        default='timestamps.json')

    args = parser.parse_args()
    return args

def parse_pcap(filename):
    pcap_dict = {}
    pcapReader = dpkt.pcap.Reader(file(filename))
    for ts_, data in pcapReader:
        pkt = dpkt.ethernet.Ethernet(data)
        h = pkt_hash(pkt)
        ts = datetime.datetime.fromtimestamp(ts_).strftime('%Y%m%d%H%M%S%f')
        pcap_dict[h] = ts
    return pcap_dict

def parse_master(filename):
    if not os.path.isfile(filename):
        print 'Error! Master logfile %s doesn\'t exist'
        return
    master_recvd_dict = {}
    master_matched_dict = {}
    master_get_dict = {}
    master_sorted_dict = {}
    f = open(filename, 'r')
    key = 'TIMESTAMP:'
    for line in f:
        try:
            i = line.index(key)
            tokens = line[i + len(key):].split()
            ts = tokens[0]
            h = tokens[3]
            if tokens[2] == 'RECEIVED':
                master_recvd_dict[h] = ts
            elif tokens[2] == 'MATCHED':
                master_matched_dict[h] = ts
            elif tokens[2] == 'GET':
                master_get_dict[h] = ts
            elif tokens[2] == 'SORTED':
                master_sorted_dict[h] = ts
        except:
            pass
    f.close()
    return master_sorted_dict, master_get_dict, master_recvd_dict, master_matched_dict

def parse_slave(filename):
    if not os.path.isfile(filename):
        print 'Error! slave logfile %s doesn\'t exist'
        return
    slave_recvd_dict = {}
    slave_notify_dict = {}
    f = open(filename, 'r')
    key = 'TIMESTAMP:'
    for line in f:
        try:
            i = line.index(key)
            tokens = line[i + len(key):].split()
            ts = tokens[0]
            h = tokens[3]
            if tokens[2] == 'RECEIVED':
                if h not in slave_recvd_dict:
                    slave_recvd_dict[h] = []
                slave_recvd_dict[h].append(ts)
            elif tokens[2] == 'NOTIFY':
                if h not in slave_notify_dict:
                    slave_notify_dict[h] = []
                slave_notify_dict[h].append(ts)
        except:
            pass
    f.close()
    return slave_recvd_dict, slave_notify_dict

if __name__ == '__main__':
    args = parse_args()

    tcpdump_pcap = '%s/capture.pcap' % args.indir
    master_logfile = '%s/master_collector.log' % args.indir
    slave_logfiles = '%s/slave_collector*.log' % args.indir

    print '---------------- parsing %s ----------------' % tcpdump_pcap 
    pcap_dict = parse_pcap(tcpdump_pcap)

    print '---------------- parsing %s ----------------' % master_logfile
    master_sorted_dict, master_get_dict, master_recvd_dict, master_matched_dict = parse_master(master_logfile)

    slave_dicts = []
    for filename in glob.glob(slave_logfiles):
        print '---------------- parsing %s ----------------' % filename
        slave_dicts.append(parse_slave(filename))

    print 

    total_t = []
    sort_t = []
    match_t = []
    get_t = []
    j = {}

    for h in master_matched_dict:
        ts = []
        # find h in pcap_dict
        if h not in pcap_dict:
            print 'pkt %s could not be found in pcap_dict' % h
            continue
        pcap_ts = pcap_dict[h]

        # find h in slave_recvd_dict and slave_notify_dict
        slave_recvd_ts = []
        slave_notify_ts = []
        for s in slave_dicts:
            if h in s[0]:
                slave_recvd_ts += s[0][h]
            if h in s[1]:
                slave_notify_ts += s[1][h]

        master_get_ts = master_get_dict[h]
        master_recvd_ts = master_recvd_dict[h]
        master_sorted_ts = master_sorted_dict[h]
        master_matched_ts = master_matched_dict[h]

        get_t.append((parse_tstamp(master_recvd_ts) -
            parse_tstamp(master_get_ts)).total_seconds())
        sort_t.append((parse_tstamp(master_sorted_ts) -
	    parse_tstamp(master_recvd_ts)).total_seconds())
	match_t.append((parse_tstamp(master_matched_ts) -
	    parse_tstamp(master_sorted_ts)).total_seconds())
        total_t.append((parse_tstamp(master_matched_ts) - 
            parse_tstamp(pcap_ts)).total_seconds())

        print '--------------------------------------------'
        print 'pcap_ts:', pcap_ts
        print 'slave_recvd_ts:', slave_recvd_ts
        print 'slave_notify_ts:', slave_notify_ts
        print 'master_recvd_ts:', master_recvd_ts
        print 'master_matched_ts:', master_matched_ts
        print 'Master get time:', get_t[-1], 'sec' 
        print 'Master sort time:', sort_t[-1], 'sec' 
        print 'Master matching time:', match_t[-1], 'sec' 
        print 'Total time:', total_t[-1], 'sec'  

    j['get'] = get_t
    j['sort'] = sort_t
    j['match'] = match_t
    j['total'] = total_t
    out_f = open(args.outfile, 'w')
    print >>out_f, json.dumps(j)
    out_f.close()

